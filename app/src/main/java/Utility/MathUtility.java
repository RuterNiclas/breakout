package Utility;

import com.example.nismrg.breakoutsample.Math.Vector2;

/**
 * Created by nismrg on 2016-06-02.
 */
public class MathUtility
{

    //region Static

    //region Methods

    //region Public

    public static int roundUP(double d)
    {
        double dAbs = Math.abs(d);
        int i = (int) dAbs;
        double result = dAbs - (double) i;

        if(result==0.0)
        {
            return (int) d;
        }
        else
        {
            return (int) d<0 ? -(i+1) : i+1;
        }
    }

    public static double euclideanDistanceBetweenPoints(double x1,
                                                        double y1,
                                                        double x2,
                                                        double y2)
    {

        double dX = x1- x2;
        double dY = y1 - y2;

        double dx2 = Math.pow(dX, 2);
        double dy2 = Math.pow(dY, 2);

        return  Math.sqrt(dx2 + dy2);

    }

    public static double euclideanDistanceBetweenPoints(float x1,
                                                        float y1,
                                                        float x2,
                                                        float y2)
    {
        return MathUtility.euclideanDistanceBetweenPoints((double) x1,
                                                          (double) y1,
                                                          (double) x2,
                                                          (double) y2);
    }

    public static double euclideanDistanceBetweenPoints(Vector2 v1, Vector2 v2)
    {
        return MathUtility.euclideanDistanceBetweenPoints(v1.getM_x(),
                                                          v1.getM_Y(),
                                                          v2.getM_x(),
                                                          v2.getM_Y());
    }

    public static float linearInterpolation (float a, float b, float floatingPoint)
    {
        return a + floatingPoint * (b - a);
    }

    //endregion

    //endregion

    //endregion

}
