package Utility.AntiConcurrentCollections;

import java.util.ArrayList;

/**
 * Created by nismrg on 2016-07-30.
 */
public class AntiConcurrentCollection<T>
{

    //region Variables

    //region Private

    private ArrayList<T> m_Objects;
    private ArrayList<T> m_NewObjects;
    private ArrayList<T> m_ObjectsToDelete;

    //endregion

    //endregion

    //region Encapsulation

    public ArrayList<T> getM_Objects() {
        return m_Objects;
    }

    public void setM_Objects(ArrayList<T> m_Objects) {
        this.m_Objects = m_Objects;
    }

    public ArrayList<T> getM_NewObjects() {
        return m_NewObjects;
    }

    public void setM_NewObjects(ArrayList<T> m_NewObjects) {
        this.m_NewObjects = m_NewObjects;
    }

    public ArrayList<T> getM_ObjectsToDelete() {
        return m_ObjectsToDelete;
    }

    public void setM_ObjectsToDelete(ArrayList<T> m_ObjectsToDelete) {
        this.m_ObjectsToDelete = m_ObjectsToDelete;
    }

    //endregion

    //region Constructors

    public AntiConcurrentCollection()
    {
        this.setM_NewObjects(new ArrayList<T>());
        this.setM_Objects(new ArrayList<T>());
        this.setM_ObjectsToDelete(new ArrayList<T>());
    }

    //endregion

    //region Methods

    //region Public

    public void commit()
    {
        this.getM_Objects().addAll(this.getM_NewObjects());
        this.setM_NewObjects(new ArrayList<T>());

        for(T t : this.getM_ObjectsToDelete())
        {
            this.getM_Objects().remove(t);
        }

        //warning Check this functionality.
        //this.setM_ObjectsToDelete(new ArrayList<T>());
    }

    //endregion

    //endregion

}
