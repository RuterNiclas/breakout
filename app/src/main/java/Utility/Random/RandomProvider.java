package Utility.Random;

import java.util.Random;
import java.util.Stack;

/**
 * Created by nismrg on 2016-07-26.
 */
public class RandomProvider
{

    //region Instance

    //region Constants

    //region Private

    // Next int.
    private final int NEXT_INT_INITIAL_SIZE = 100000;
    private final int NEXT_INT_CONTINUOUS_VALUE = 500;

    // Next float.
    private final int NEXT_FLOAT_INITIAL_SIZE = 100000;
    private final int NEXT_FLOAT_CONTINUOUS_VALUE = 500;

    // Next float.
    private final int NEXT_GAUSSIAN_INITIAL_SIZE = 100000;
    private final int NEXT_GAUSSIAN_CONTINUOUS_VALUE = 500;

    //endregion

    //endregion

    //region Variables

    //region Private

    private Random m_Random;

    private Stack<Integer> m_IntStack;
    private Stack<Float> m_FloatStack;
    private Stack<Double> m_GaussianStack;

    //endregion

    //endregion

    //region Encapsulation

    //region Private

    private void setM_Random(Random m_Random) {
        this.m_Random = m_Random;
    }

    //endregion

    //endregion

    //region Constructors

    private RandomProvider()
    {
        this.setM_Random(new Random());
    }

    //endregion

    //region Methods

    //region Public

    public void initializeSingleTon()
    {
        // Int
        this.m_IntStack = new Stack<>();
        this.replenishNexInts(this.NEXT_INT_INITIAL_SIZE);

        // Float
        this.m_FloatStack = new Stack<>();
        this.replenishNextFloats(NEXT_FLOAT_INITIAL_SIZE);

        // Gaussian
        this.m_GaussianStack = new Stack<>();
        this.replenishGaussian(this.NEXT_GAUSSIAN_INITIAL_SIZE);

    }

    public int nextInt()
    {
        if(this.m_IntStack.empty())
        {
            this.replenishNexInts(this.NEXT_INT_CONTINUOUS_VALUE);
        }

        int value = this.m_IntStack.pop().intValue();

        return value;
    }

    public int nextInt(int span)
    {
        return this.m_Random.nextInt(span);
    }

    public float nextFloat()
    {
        if(this.m_FloatStack.empty())
        {
            this.replenishNextFloats(this.NEXT_FLOAT_CONTINUOUS_VALUE);
        }

        float value = this.m_FloatStack.pop().floatValue();

        return value;
    }

    public double nextGaussian()
    {
        if(this.m_GaussianStack.empty())
        {
            this.replenishGaussian(this.NEXT_GAUSSIAN_CONTINUOUS_VALUE);
        }

        double value = this.m_GaussianStack.pop().doubleValue();

        return  value;
    }

    //endregion

    //region Private

    private void replenishNexInts(int numberOfNumbers)
    {
        for(int i = 0; i < numberOfNumbers; i++)
        {
            this.m_IntStack.add(this.m_Random.nextInt());
        }
    }

    private void replenishNextFloats(int numberOfNumbers)
    {
        for(int i = 0; i < numberOfNumbers; i++)
        {
            this.m_FloatStack.add(this.m_Random.nextFloat());
        }
    }

    private void replenishGaussian(int numberOfNumbers)
    {
        for(int i = 0; i < numberOfNumbers; i++)
        {
            this.m_GaussianStack.add(this.m_Random.nextGaussian());
        }
    }

    //endregion

    //endregion

    //endregion

    //region Static

    //region Variables

    //region Private

    private static RandomProvider ourInstance = new RandomProvider();

    //endregion

    //endregion

    //region Encapsulation

    public static RandomProvider getInstance() {
        return ourInstance;
    }

    //endregion

    //endregion

}
