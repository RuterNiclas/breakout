package Utility;

/**
 * Created by nismrg on 2016-06-09.
 */
public interface IDeepClone<T>
{

    //region Methods

    T deepClone();

    //endregion

}
