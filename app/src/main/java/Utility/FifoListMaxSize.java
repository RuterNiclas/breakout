package Utility;

import java.util.ArrayList;

/**
 * Created by nismrg on 2016-11-23.
 */
public class FifoListMaxSize<K> extends ArrayList<K>
{

    //region Variables

    //region Private

    private int maxSize;

    //endregion

    //endregion

    //region Constructors

    public FifoListMaxSize(int size)
    {
        this.maxSize = size;
    }

    //endregion

    //region Methods

    //region Public

    @Override
    public boolean add(K k)
    {
        boolean r = super.add(k);
        if (size() > maxSize)
        {
            removeRange(0, size() - maxSize - 1);
        }
        return r;
    }

    public K getLast()
    {
        return this.get(this.size() - 1);
    }

    //endregion

    //endregion

}