package com.example.nismrg.breakoutsample.GameObjects.Bricks;

import android.graphics.RectF;

import com.example.nismrg.breakoutsample.DeviceFunctions.DeviceFunctions;
import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.GameObject;
import com.example.nismrg.breakoutsample.GameObjects.HitFrom;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

/**
 * Created by nismrg on 2016-05-24.
 */
public abstract class BrickBase extends GameObject
{

    //region Constants

    //region Private

    private final int DEFAULT_PADDING = 3;
    private final int DAMAGE_RESPITE_IN_NUMBER_OF_FRAMES = 3;

    //endregion

    //endregion

    //region Variables

    //region Private

    private RectF m_Rectangle;
    private RectF m_DrawingRectangle;
    private int m_HitPointsLeft;
    private HitFrom m_FirstHitFrom;
    private World mParentWorld;
    private boolean mBroken;
    private float mDepth;

    private boolean mVibratesOnHit;
    private long[] mVibrationPatternOnHit;
    private int mVibrationRepeatsOnHit;

    //endregion

    //region Protected

    protected int m_FrameCounter;

    //endregion

    //endregion

    //region Encapsulation

    public RectF getM_Rectangle()
    {
        return m_Rectangle;
    }

    public void setM_Rectangle(RectF m_Rectangle)
    {
        this.m_Rectangle = m_Rectangle;
    }

    public int getM_HitPointsLeft()
    {
        return m_HitPointsLeft;
    }

    public void setM_HitPointsLeft(int m_HitPointsLeft)
    {
        this.m_HitPointsLeft = m_HitPointsLeft;
    }

    public RectF getM_DrawingRectangle() {
        return m_DrawingRectangle;
    }

    public void setM_DrawingRectangle(RectF m_DrawingRectangle) {
        this.m_DrawingRectangle = m_DrawingRectangle;
    }

    public HitFrom getM_FirstHitFrom() {
        return m_FirstHitFrom;
    }

    public void setM_FirstHitFrom(HitFrom m_FirstHitFrom) {
        this.m_FirstHitFrom = m_FirstHitFrom;
    }

    public World getmParentWorld() {
        return mParentWorld;
    }

    public void setmParentWorld(World mParentWorld) {
        this.mParentWorld = mParentWorld;
    }

    public boolean ismBroken() {
        return mBroken;
    }

    public void setmBroken(boolean mBroken) {
        this.mBroken = mBroken;
    }

    public float getmDepth() {
        return mDepth;
    }

    public void setmDepth(float mDepth) {
        this.mDepth = mDepth;
    }


    //endregion

    //region Constructors

    public BrickBase(World world, BrickLocationAndSize brickLocationAndSize)
    {
        super();

        this.myInitialize(world,
                          brickLocationAndSize,
                          false,
                          null,
                          0);

    }

    public BrickBase(World world,
                     BrickLocationAndSize brickLocationAndSize,
                     boolean vibratesOnHit,
                     long[] vibrationPatternOnHit,
                     int vibrationRepeatsOnHit)
    {
        super();

        this.myInitialize(world,
                          brickLocationAndSize,
                          vibratesOnHit,
                          vibrationPatternOnHit,
                          vibrationRepeatsOnHit);
    }

    //endregion

    //region Methods

    //region Public

    public void takeDamage(UpdateInformation updateInformation)
    {
        this.m_HitPointsLeft--;
        this.m_FrameCounter = DAMAGE_RESPITE_IN_NUMBER_OF_FRAMES;
        if(this.m_HitPointsLeft < 1)
        {
            this.setmBroken(true);
        }
    }

    public HitFrom locationStrucksMeFrom(UpdateInformation updateInformation, BallBase ballBase)
    {

        HitFrom hitFrom = HitFrom.NONE;

        if(RectF.intersects(this.getM_Rectangle(), ballBase.getTopDerivativeRect()))
        {
            hitFrom = HitFrom.BELOW;
        }
        if(RectF.intersects(this.m_Rectangle, ballBase.getBottomDerivativeRect()))
        {
            hitFrom = HitFrom.ABOVE;
        }
        if(RectF.intersects(this.m_Rectangle, ballBase.getRightDerivativeRect()))
        {
            hitFrom = HitFrom.LEFT;
        }
        if(RectF.intersects(this.getM_Rectangle(), ballBase.getLeftDerivativeRect()))
        {
            hitFrom = HitFrom.RIGHT;
        }

        if(hitFrom != HitFrom.NONE &&
           this.m_FrameCounter <= 0)
        {
            if(this.getM_FirstHitFrom() == HitFrom.NONE)
            {
                this.setM_FirstHitFrom(hitFrom);
            }

            this.takeDamage(updateInformation);

            if(this.mVibratesOnHit)
            {
                this.vibrate();
            }
        }

        return hitFrom;

    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation) {

        super.drawMe(drawInformation);

    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        super.updateMe(updateInformation);

        this.m_FrameCounter--;

    }

    @Override
    public int getMaxAreaUnitsASecond()
    {
        return  0;
    }

    //endregion

    //region toString

    @Override
    public String toString()
    {
        String value = String.format("Actual: (%1.4f, %2.4f) DrawRect: (%3.4f, 4.4f) Loaction: (%5.4f, %6.4f)",
                this.getM_Rectangle().right - this.m_Rectangle.left,
                this.getM_Rectangle().bottom - this.m_Rectangle.top,
                this.getM_DrawingRectangle().right - this.getM_DrawingRectangle().left,
                this.getM_DrawingRectangle().bottom - this.getM_DrawingRectangle().top,
                this.getM_Rectangle().left, this.getM_Rectangle().top);
        return value;
    }

    //endregion

    //endregion

    //region Protected

    /*
     * invertDimensions tells if there will be a rotation later.
     */
    protected BitMapAndName scaleBitmapToBrickSize(BitMapAndName bitMapAndName /*, boolean invertDimensions*/)
    {

        int width = (int)this.getM_DrawingRectangle().width();
        int height = (int)this.getM_DrawingRectangle().height();

        BitMapAndName bitmapToReturn;

        /*
        if(invertDimensions)
        {
            bitmapToReturn =
                    ResourceHolderImages.getInstance().getResizedBitmap(bitmap,
                            height,
                            width);
        }
        else
        {
        */
            bitmapToReturn =
                    ResourceHolderImages.getInstance().getResizedBitmap(bitMapAndName,
                            width,
                            height);
        /*
        }
*/




        return bitmapToReturn;

    }

    //endregion

    //region Private

    private void myInitialize(World world,
                              BrickLocationAndSize brickLocationAndSize,
                              boolean vibratesOnHit,
                              long[] vibrationPatternOnHit,
                              int vibrationRepeatsOnHit)
    {

        this.setmParentWorld(world);

        Vector2 location = brickLocationAndSize.getmLocation();
        int width = brickLocationAndSize.getmSize().getWidth();
        int height = brickLocationAndSize.getmSize().getHeight();

        //                                             Performance
        float left = location.getM_x() - (width * 0.5f /* / 2 */);
        float top = location.getM_Y() - (height * 0.5f /* / 2 */);
        float right = location.getM_x() + (width * 0.5f /* / 2 */);
        float bottom = location.getM_Y() + (height * 0.5f /* / 2 */);

        this.setM_Rectangle(new RectF(left,
                top,
                right,
                bottom));
        this.setM_DrawingRectangle(new RectF(new RectF(left + DEFAULT_PADDING,
                top + DEFAULT_PADDING,
                right - DEFAULT_PADDING,
                bottom - DEFAULT_PADDING)));

        // Depth is usually the heiht.
        // These bricks looks like this from the side:
        //
        //      /xxxxx/
        //      /xxxxx/
        //      /xxxxx/
        //
        this.setmDepth(this.getM_DrawingRectangle().height());

        this.m_FrameCounter = 0;

        this.setM_FirstHitFrom(HitFrom.NONE);
        this.setmBroken(false);

        //region Vibration patterns

        this.mVibratesOnHit = vibratesOnHit;
        this.mVibrationPatternOnHit = vibrationPatternOnHit;
        this.mVibrationRepeatsOnHit = vibrationRepeatsOnHit;

        //endregion

    }

    public void vibrate()
    {
        //DeviceFunctions.getInstance().getmVibrator().vibrate(75);


        DeviceFunctions.getInstance().cancelVibrations();
        DeviceFunctions.getInstance().vibrate(this.mVibrationPatternOnHit, this.mVibrationRepeatsOnHit);

    }

    //endregion

    //endregion

}
