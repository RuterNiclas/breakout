package com.example.nismrg.breakoutsample.Math;

/**
 * Created by nismrg on 2016-06-09.
 */
public interface IVector<T>
{
    void rotateAmongZAxis(float angle);
    void rotateAmongZAxis(double angle);
    T rotateAmongZAxisNewVector(float angle);
    T rotateAmongZAxisNewVector(double angle);
    T scale(float scale);
    T clampToLength(float length);
    void scaleToLength(float length);
    float getLength();
    void addVector(T v);
    T addAndNewVector(T v);
    void normalize();
}
