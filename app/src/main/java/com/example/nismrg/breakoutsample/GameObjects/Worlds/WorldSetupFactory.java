package com.example.nismrg.breakoutsample.GameObjects.Worlds;

import android.util.Size;

import com.example.nismrg.breakoutsample.GameObjects.Balls.BasicBall;
import com.example.nismrg.breakoutsample.GameObjects.Balls.ExplodeBall.ExplodeBall;
import com.example.nismrg.breakoutsample.GameObjects.Balls.FastBall.FastBall;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickBase;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickLocationAndSize;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.CrateBricks.CrateBrick;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.IndestructibleBrick;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks.SteelBrick1;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks.SteelBrick2;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by nismrg on 2016-05-24.
 */
public enum WorldSetupFactory
{

    //region enums

    GreenGrass, Factory, Hard;

    //endregion

    //region Methods

    //region Public

    public WorldDescriptor getWorldDescriptor(World world) throws Exception {

        WorldDescriptor worldDescriptor;

        switch (this) {
            case GreenGrass:
                worldDescriptor = this.buildGreenGrass(world);
                break;
            case Factory:
                worldDescriptor = this.buildFactory(world);
                break;
            case Hard:
                worldDescriptor = this.buildHard(world);
                break;
            default:
                throw new Exception("Unreachable world");

        }

        return  worldDescriptor;

    }

    //endregion

    //region Private

    private WorldDescriptor buildGreenGrass(World world)
    {

        WorldDescriptor worldDescriptor = new WorldDescriptor();

        //region Make bricks

        // Crate:          C
        // Steel:          S
        // Indestructible: I

        String[][] mapStencil = new String[][]{ {"C", "C", "C", "C", "C", "C", "C", "C"},
                                                {"C", "C", "C", "C", "C", "C", "C", "C"},
                                                {"C", "C", "C", "C", "C", "C", "C", "C"},
                                                {"C", "C", "C", "C", "C", "C", "C", "C"},
                                                {"C", "C", "C", "C", "C", "C", "C", "C"} };

        int BLOCK_HEIGHT = 50;

        int COLUMNS = 8;

        Size atomSize = new Size(world.getM_Size().getWidth() / COLUMNS,
                                 BLOCK_HEIGHT);

        Vector2 offset = new Vector2(atomSize.getWidth() / 2,
                                     atomSize.getHeight() / 2);

        worldDescriptor.setmBricks(this.brickMatrixToArrayList(world,
                                                               mapStencil,
                                                               atomSize,
                                                               offset));

        //endregion

        //region Make balls

        //worldDescriptor.getmBallBaseSetUp().add(new FollowBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new ExplodeBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new FastBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new BasicBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new BasicBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new FastBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new BasicBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new BasicBall(world));

        //endregion

        return worldDescriptor;

    }

    private WorldDescriptor buildFactory(World world)
    {

        WorldDescriptor worldDescriptor = new WorldDescriptor();

        //region Make bricks

        // Crate:          C
        // Steel:          S
        // Indestructible: I

        String[][] mapStencil = new String[][]{ { " ", " ", " ", " ", " ", " ", " ", " " },
                                               // { "C", "C", "C", "C", "C", "C", "C", "C" },
                                               // { "S", "S", "I", "I", "C", "I", "S", "S" },
                                                { " ", " ", " ", " ", " ", " ", " ", " " },
                                                { "I", "I", "I", "I", "I", "I", "I", "I" } };

        // };

/*
        String[][] mapStencil = new String[][]{ { "C", "C", "C", "C", "C", "C", "C", "C" },
                                                { "C", "C", "C", "C", "C", "C", "C", "C" },
                                                { "C", "C", "C", "C", "C", "C", "C", "C" },
                                                { " ", " ", " ", " ", " ", " ", " ", " " },
                                                { "S", "S", "S", "S", "S", "S", "S", "S" } };

*/

        int BLOCK_HEIGHT = 60;

        int COLUMNS = 8;

        Size atomSize = new Size(world.getM_Size().getWidth() / COLUMNS,
                                 BLOCK_HEIGHT);

        Vector2 offset = new Vector2(atomSize.getWidth() / 2,
                                     atomSize.getHeight() / 2);

        worldDescriptor.setmBricks(this.brickMatrixToArrayList(world,
                                                               mapStencil,
                                                               atomSize,
                                                               offset));

        //endregion

        //region Make balls

        worldDescriptor.getmBallBaseSetUp().add(new ExplodeBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new ExplodeBall(world));
        //worldDescriptor.getmBallBaseSetUp().add(new LaserBall(world));
        //worldDescriptor.getmBallBaseSetUp().add(new FollowBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new FastBall(world));
        //worldDescriptor.getmBallBaseSetUp().add(new FollowBall(world));
        //worldDescriptor.getmBallBaseSetUp().add(new BasicBall(world));
        //worldDescriptor.getmBallBaseSetUp().add(new FastBall(world));
        //worldDescriptor.getmBallBaseSetUp().add(new ExplodeBall(world));





       /*
        worldDescriptor.getmBallBaseSetUp().add(new FastBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new LaserBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new BasicBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new FastBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new LaserBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new BasicBall(world));
*/
        //endregion


        BitMapAndName bitMapAndName =
            ResourceHolderImages.getInstance().getResizedBitmap(ResourceHolderImages.getInstance().getmSpace1(),
                                                                world.getM_Size().getWidth(),
                                                                world.getM_Size().getHeight());

        // Need to mae new instance here not to alter original image.
        bitMapAndName = new BitMapAndName(ResourceHolderImages.getInstance().makeGammaBitmapCopy(bitMapAndName.getmBitmap(), 1.5f), ResourceHolderImages.getInstance().feedUniqueImageId());

        worldDescriptor.setmBackground(bitMapAndName);

        return worldDescriptor;

    }

    private WorldDescriptor buildHard(World world)
    {

        WorldDescriptor worldDescriptor = new WorldDescriptor();

        //region Make bricks

        // Crate:          C
        // Steel:          S
        // Indestructible: I

        String[][] mapStencil = new String[][]{ { "C", "C", "C", "C", "C", "C", "C" },
                                                { "C", "C", " ", " ", " ", "C", "C" },
                                                { "C", "C", " ", " ", " ", "C", "C" },
                                                { " ", " ", " ", " ", " ", " ", " " },
                                                { "I", "I", "I", " ", "I", "I", "I" } };

        int BLOCK_HEIGHT = 50;

        int COLUMNS = 7;

        Size atomSize = new Size(world.getM_Size().getWidth() / COLUMNS,
                                 BLOCK_HEIGHT);

        Vector2 offset = new Vector2(atomSize.getWidth() / 2,
                                     atomSize.getHeight() / 2);

        worldDescriptor.setmBricks(this.brickMatrixToArrayList(world,
                                                               mapStencil,
                                                               atomSize,
                                                               offset));

        //endregion

        //region Make balls

        //worldDescriptor.getmBallBaseSetUp().add(new FollowBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new ExplodeBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new FastBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new ExplodeBall(world));
        worldDescriptor.getmBallBaseSetUp().add(new ExplodeBall(world));

        //endregion

        return worldDescriptor;

    }

    private ArrayList<BrickBase> brickMatrixToArrayList(World world,
                                                        String[][] stencil,
                                                        Size atomSize,
                                                        Vector2 offset)
    {

        Random r = new Random();
        BrickLocationAndSize brickLocationAndSize = null;
        ArrayList<BrickBase> brickList = new ArrayList<>();

        for (int row = 0; row < stencil.length; row++)
        {
            for (int col = 0; col < stencil[row].length; col++)
            {

                brickLocationAndSize =
                    new BrickLocationAndSize(new Vector2((col) * atomSize.getWidth() + offset.getM_x() + 1,
                                                         (row) * atomSize.getHeight() + offset.getM_Y() + 1),
                                             atomSize);

                switch (stencil[row][col])
                {
                    case "C" : brickList.add(new CrateBrick(world, brickLocationAndSize));
                               break;

                    case "S" : int randomInt = r.nextInt(2);
                               switch (randomInt)
                               {
                                   case 1:
                                       brickList.add(new SteelBrick1(world, brickLocationAndSize));
                                       break;
                                   default:
                                       brickList.add(new SteelBrick2(world, brickLocationAndSize));
                                       break;
                               }

                               break;

                    case "I" : brickList.add(new IndestructibleBrick(world, brickLocationAndSize));
                               break;

                    default: break;

                }
            }
        }

        return brickList;

    }

    //endregion

    //endregion

}
