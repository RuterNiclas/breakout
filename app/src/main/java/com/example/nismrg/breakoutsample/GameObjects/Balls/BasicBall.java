package com.example.nismrg.breakoutsample.GameObjects.Balls;

import android.graphics.Canvas;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

/**
 * Created by nismrg on 2016-07-10.
 */
public class BasicBall extends BallBase {

    //region Constants

    //region Private

    private final int LAUNCH_SPEED_UNITS_SECOND = 400;
    private final int DEFAULT_RADIUS = 30;

    //endregion

    //endregion

    //region Variables

    //region Private

    BitMapAndName mMyImage;

    //endregion

    //endregion

    //region Constructors

    public BasicBall(World world){
        super(world);

        this.setM_Radius(DEFAULT_RADIUS);

        this.myInitialize();
    }

    public BasicBall(World world, float radius){
        super(world, radius);

        this.myInitialize();

    }

    //endregion

    //region Methods

    //region Public

    public float getStartingSpeed()
    {
        return LAUNCH_SPEED_UNITS_SECOND;
    }

    public void drawMeAt(DrawInformation drawInformation, Vector2 location)
    {

        Canvas canvas = drawInformation.getmCanvas();

        canvas.drawBitmap(this.mMyImage.getmBitmap(),
                          location.getM_x() - super.getM_Radius(),
                          location.getM_Y()- super.getM_Radius(),
                          null);

    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation) {

        //super.drawMe(canvas, gameTimeRunMillis);

        this.drawMeAt(drawInformation, this.getM_Location());
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize()
    {
        this.mMyImage = ResourceHolderImages.getInstance().getResizedBitmap(ResourceHolderImages.getInstance().getmDefaultBall(),
                                                                            Math.round(this.getM_Radius()) * 2,
                                                                            Math.round(this.getM_Radius()) * 2);
    }

    //endregion

    //endregion

}
