package com.example.nismrg.breakoutsample.GameObjects.Bricks;

import android.graphics.Canvas;
import android.graphics.RectF;

import com.example.nismrg.breakoutsample.DeviceFunctions.VibrationEnums;
import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

/**
 * Created by nismrg on 2016-07-11.
 */
public class IndestructibleBrick extends BrickBase
{

    //region Constants

    //region Private

    //region Vibrations

    private static final boolean VIBRATES_ON_HIT = true;
    private static final long[] VIBRATE_PATTERN_ON_HIT = new long[]{ 0, VibrationEnums.LENGTH_SMALLEST_POSSIBLE };
    private static final int VIBRATIONS_REPEAT_ON_HIT = -1;

    //endregion

    private final int HIT_POINTS = Integer.MAX_VALUE;
    private final int ANIMATION_LENGTH_IN_MILLI_SECONDS = 1300;
    private final float ANIMATION_SCALE_TO_1_TO_1_RATIO = 0.3f;
    private final int EFFECT_NUMBER_OF_WAVES = 4;

    //endregion

    //endregion

    //region Variables

    //region Private

    private Long mLastHitAt;
    private BitMapAndName image;

    //endregion

    //endregion

    //region Encapsulation

    @Override
    public int getM_ZTag()
    {
        // Check out for changes in struck by ball.
        if(this.mLastHitAt == null)
        {
            super.setM_ZTag(0);
        }
        else
        {
            super.setM_ZTag(1);
        }
        return super.getM_ZTag();
    }

    //endregion

    //region Constructors

    public IndestructibleBrick(World world, BrickLocationAndSize brickLocationAndSize)
    {
        super(world,
              brickLocationAndSize,
              VIBRATES_ON_HIT,
              VIBRATE_PATTERN_ON_HIT,
              VIBRATIONS_REPEAT_ON_HIT);

        this.initializeMe();
    }

    //endregion

    //region Methods

    //region Public

    public void takeDamage(UpdateInformation updateInformation)
    {

        this.mLastHitAt = updateInformation.getmGameTimeRunMillis();

        super.takeDamage(updateInformation);

    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation) {

        Canvas canvas = drawInformation.getmCanvas();

        super.drawMe(drawInformation);

        if(this.mLastHitAt == null)
        {
            canvas.drawBitmap(this.image.getmBitmap(),
                              null,
                              super.getM_DrawingRectangle(),
                              null);
        }
        else
        {
            float effectFluctuation = 1;

            long passedTime = drawInformation.getmGameTimeRunMillis() -
                              this.mLastHitAt.longValue();

            double radians = ((passedTime) /
                              (double)this.ANIMATION_LENGTH_IN_MILLI_SECONDS) *
                             this.EFFECT_NUMBER_OF_WAVES * Math.PI;

            float yValue = (float)(Math.sin(radians));

            float fadeFactor = 1 - ((float)passedTime / (float)this.ANIMATION_LENGTH_IN_MILLI_SECONDS);

            effectFluctuation += yValue *
                                 this.ANIMATION_SCALE_TO_1_TO_1_RATIO *
                                 fadeFactor;

            canvas.drawBitmap(this.image.getmBitmap(),
                              null,
                              this.scaleRectF(super.getM_DrawingRectangle(), effectFluctuation),
                              null);
        }
    }


    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        super.updateMe(updateInformation);

        if(this.mLastHitAt != null &&
           updateInformation.getmGameTimeRunMillis() - this.mLastHitAt.longValue() > this.ANIMATION_LENGTH_IN_MILLI_SECONDS)
        {
            this.mLastHitAt = null;
        }
    }

    //endregion

    //endregion

    //region Private

    private void initializeMe()
    {
        super.setM_HitPointsLeft(HIT_POINTS);
        this.mLastHitAt = null;
        this.image = super.scaleBitmapToBrickSize(ResourceHolderImages.getInstance().getmIndestructible()/*, false*/);

    }

    private RectF scaleRectF(RectF rect, float factor)
    {
        RectF newRect = new RectF(rect.left,
                                  rect.top,
                                  rect.right,
                                  rect.bottom);

        float diffHorizontal = (rect.right-rect.left) * (factor-1f);
        float diffVertical = (rect.bottom-rect.top) * (factor-1f);

        newRect.top -= diffVertical * 0.5f; // Performance
        newRect.bottom += diffVertical * 0.5f; // Performance

        newRect.left -= diffHorizontal * 0.5f; // Performance
        newRect.right += diffHorizontal * 0.5f; // Performance

        return newRect;
    }

    //endregion

    //endregion

}
