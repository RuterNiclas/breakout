package com.example.nismrg.breakoutsample.GameObjects.Worlds;

import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickBase;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;

import java.util.ArrayList;

/**
 * Created by nismrg on 2016-07-10.
 */
public class WorldDescriptor
{

    //region Variables

    //region Private

    private ArrayList<BrickBase> mBricks;
    private ArrayList<BallBase> mBallBaseSetUp;
    private BitMapAndName mBackground;
    private float mGammaBackground;

    //endregion

    //endregion

    //region Encapsulation

    public ArrayList<BrickBase> getmBricks()
    {
        return mBricks;
    }

    public void setmBricks(ArrayList<BrickBase> mBricks)
    {
        this.mBricks = mBricks;
    }

    public ArrayList<BallBase> getmBallBaseSetUp()
    {
        return mBallBaseSetUp;
    }

    public void setmBallBaseSetUp(ArrayList<BallBase> mBallBaseSetUp)
    {
        this.mBallBaseSetUp = mBallBaseSetUp;
    }

    public BitMapAndName getmBackground() {
        return mBackground;
    }

    public void setmBackground(BitMapAndName mBackground) {
        this.mBackground = mBackground;
    }

    public float getmGammaBackground() {
        return mGammaBackground;
    }

    public void setmGammaBackground(float mGammaBackground) {
        this.mGammaBackground = mGammaBackground;
    }

    //endregion

    //region Constructors

    public WorldDescriptor()
    {
        this.mBricks = new ArrayList<>();
        this.mBallBaseSetUp = new ArrayList<>();
    }

    //endregion

}
