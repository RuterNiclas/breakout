package com.example.nismrg.breakoutsample.GameObjects.Controls;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.GameObject;
import com.example.nismrg.breakoutsample.Math.Vector2;

/**
 * Created by nismrg on 2016-07-15.
 */
public abstract class GameButtonBase extends GameObject
{

    //region Variables

    //region Private

    private boolean mVisible;
    private String mName;
    private String mCaption;
    private Vector2 mLocation;

    public GameButtonOnClickListener listener;

    //endregion

    //endregion

    //region Encapsulation

    public boolean ismVisible() {
        return mVisible;
    }

    public void setmVisible(boolean mVisible) {
        this.mVisible = mVisible;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmCaption() {
        return mCaption;
    }

    public void setmCaption(String mCaption) {
        this.mCaption = mCaption;
    }

    public Vector2 getmLocation() {
        return mLocation;
    }

    public void setmLocation(Vector2 mLocation) {
        this.mLocation = mLocation;
    }

    //endregion

    //region Constructors

    public GameButtonBase(boolean visible,
                          String name,
                          String caption,
                          Vector2 location)
    {
        this.initialize(visible,
                        name,
                        caption,
                        location);
    }

    //endregion

    //region Methods

    //region Private

    private void initialize(boolean visible,
                            String name,
                            String caption,
                            Vector2 location)
    {
        this.setmVisible(visible);
        this.setmLocation(location);
        this.setmCaption(caption);
        this.setmName(name);
    }

    //endregion

    //region Public

    //region IGameLogic

    public abstract void drawMe(DrawInformation drawInformation);

    public abstract void updateMe(UpdateInformation updateInformation);

    // This kind of control don't updates location.
    public int getMaxAreaUnitsASecond()
    {
        return 0;
    }

    //endregion

    //endregion

    //endregion

}
