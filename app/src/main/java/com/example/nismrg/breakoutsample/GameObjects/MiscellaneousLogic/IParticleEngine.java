package com.example.nismrg.breakoutsample.GameObjects.MiscellaneousLogic;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by nismrg on 2016-09-09.
 */
public interface IParticleEngine<T>
{

    ArrayList<T> pullParticles(int numberOfObjects);
    T pullSingleParticle();

    void pushBackParticles(Collection<T> particles);

}
