package com.example.nismrg.breakoutsample.GameObjects.Balls;

import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;

import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-10-30.
 */
public abstract class RotatingBallBase extends BallBase
{

    //region Variables

    //region Protected

    protected Float m_AnglesASecond;
    protected float m_currentRotation;

    //endregion

    //endregion

    //region Constructors

    public RotatingBallBase(World world)
    {
        super(world);

        this.myInitialize();
    }

    public RotatingBallBase(World world, float radius)
    {
        super(world, radius);

        this.myInitialize();
    }

    //endregion

    //region Methods

    //region Public

    //region IGameLogic

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        if(this.m_AnglesASecond != null)
        {
            this.m_currentRotation += this.m_AnglesASecond * updateInformation.mOneDividedByFPS;
        }

        super.updateMe(updateInformation);

    }

    //endregion

    //region Protected

    protected void assignRandomRotation(float avgValue, float stdDeviation)
    {
        if(this.m_AnglesASecond == null)
        {
            double randomValue = RandomProvider.getInstance().nextGaussian();
            this.m_AnglesASecond = (float) (randomValue * stdDeviation + avgValue);
        }
    }

    //endregion

    //region Private

    private void myInitialize()
    {
        this.m_currentRotation = 0;
    }

    //endregion

    //endregion

    //endregion

}
