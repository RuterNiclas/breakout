package com.example.nismrg.breakoutsample.DeviceFunctions;

import android.content.Context;
import android.os.Vibrator;

/**
 * Created by nismrg on 2016-11-29.
 */
public class DeviceFunctions
{

    //region Instance

    //region Variables

    //region Private

    private Vibrator mVibrator;

    //endregion

    //endregion

    //region Constructors

    private DeviceFunctions()
    {
    }

    //endregion

    //region Methods

    //region Public

    public void InitializeSingleTon(Context context)
    {
        this.mVibrator = ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE));

    }

    public void vibrate(long[] pattern, int repeats)
    {
        if(this.mVibrator.hasVibrator())
        {
            this.mVibrator.vibrate(pattern, repeats);
        }
    }

    public void cancelVibrations()
    {
        if(this.mVibrator.hasVibrator())
        {
            this.mVibrator.cancel();
        }
    }

    //endregion

    //endregion

    //endregion

    //region Static

    //region Variables

    //region Private

    private static DeviceFunctions ourInstance = new DeviceFunctions();

    //endregion

    //endregion

    //region Encapsulation

    //region Public

    public static DeviceFunctions getInstance() {
        return ourInstance;
    }

    //endregion

    //endregion

    //endregion

}
