package com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks;

import android.graphics.Bitmap;
import android.graphics.RectF;
import android.util.Pair;
import android.util.Size;

import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.Balls.FragmentBall;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickBase;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickLocationAndSize;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.IHitEffect;
import com.example.nismrg.breakoutsample.GameObjects.HitFrom;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.PaintParticleLine;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.PaintParticleLineSystem;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.ParticleSystemBase;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.UV_Instructions.UV;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.Math.Vector3;
import com.example.nismrg.breakoutsample.Math.Vector3NotEncapsulated;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-07-25.
 */
public abstract class SteelBrickBase extends BrickBase implements IHitEffect
{

    //region Constants

    //region Private

    private final int INVERT_FREQUENCY_DENOMINATOR = 3;

    //endregion

    //endregion

    //region Variables

    //region Protected

    protected BitMapAndName noDamageImage;
    protected BitmapAndCustomInfo oneDamageImage;
    protected BitmapAndCustomInfo twoDamageImage;
    protected float m_BrickTilt;

    //endregion

    //region Private

    //Explosion results
    private ArrayList<ParticleSystemBase> mNewParticles;
    private ArrayList<BallBase> mNewBallBases;

    private Map<Integer, HitFromAndSteelBrickExplosionResultSet> damageToSteelBrickExplosionResultSet;

    private int mRunningValueUVMap;

    //endregion

    //endregion

    //region Encapsulation

    //region Protected

    protected ArrayList<ParticleSystemBase> getmNewParticles()
    {
        return mNewParticles;
    }

    protected void setmNewParticles(ArrayList<ParticleSystemBase> mNewParticles)
    {
        this.mNewParticles = mNewParticles;
    }

    protected ArrayList<BallBase> getmNewBallBases() {
        return mNewBallBases;
    }

    protected void setmNewBallBases(ArrayList<BallBase> mNewBallBases) {
        this.mNewBallBases = mNewBallBases;
    }

    //endregion

    //region Public

    public Map<Integer, HitFromAndSteelBrickExplosionResultSet> getDamageToSteelBrickExplosionResultSet() {
        return damageToSteelBrickExplosionResultSet;
    }

    public void setDamageToSteelBrickExplosionResultSet(Map<Integer, HitFromAndSteelBrickExplosionResultSet> damageToSteelBrickExplosionResultSet) {
        this.damageToSteelBrickExplosionResultSet = damageToSteelBrickExplosionResultSet;
    }

    //endregion

    //endregion

    //region Constructors

    public SteelBrickBase(World world,
                          BrickLocationAndSize brickLocationAndSize,
                          boolean vibratesOnHit,
                          long[] vibrationPatternOnHit,
                          int vibrationRepeatsOnHit)
    {
        super(world,
              brickLocationAndSize,
              vibratesOnHit,
              vibrationPatternOnHit,
              vibrationRepeatsOnHit);

        this.myInitialize();
    }

    //endregion

    //region Methods

    //region Public

    //region IHitEffect

    public Collection<BallBase> pullAllNewBalls()
    {
        Collection<BallBase> ballBases = this.getmNewBallBases();
        this.setmNewBallBases(new ArrayList<BallBase>());

        return ballBases;
    }

    public Collection<ParticleSystemBase> pullAllNewParticleSystems()
    {
        Collection<ParticleSystemBase> particleBases = this.getmNewParticles();
        this.setmNewParticles(new ArrayList<ParticleSystemBase>());

        return particleBases;
    }

    //endregion

    //endregion

    //region Protected

    protected float getExplosionAngelSector(HitFrom hitFrom)
    {

        float x;
        float y;

        RectF drawingRect = super.getM_DrawingRectangle();

        //region Fail safe

        if(drawingRect.top == 0 &&
           drawingRect.left == 0 &&
           drawingRect.top == 0 &&
           drawingRect.bottom == 0)
        {
            return  0f;
        }

        //endregion

        //region Calculate

        float result;

        switch (hitFrom)
        {
            case NONE:

                //region None

                return  0f;

                //endregion

            case BELOW:

                //region Below

                x = Math.abs(drawingRect.left - drawingRect.centerX());
                y = Math.abs(drawingRect.bottom - drawingRect.centerY());

                result = 180 - 90 - (float)(Math.toDegrees(Math.asin(y/x)));

                return  result;

                // endregion

            case ABOVE:

                //region Above

                x = Math.abs(drawingRect.left - drawingRect.centerX());
                y = Math.abs(drawingRect.centerY() - drawingRect.top);

                result = 180 - 90 - (float)(Math.toDegrees(Math.asin(y/x)));

                return result;

                //endregion

            case LEFT:

                //region Left

                x = Math.abs(drawingRect.centerX() - drawingRect.left);
                y = Math.abs(drawingRect.centerY() - drawingRect.top);

                result = (float)(Math.toDegrees(Math.asin(y/x)));

                return  result;

                //endregion

            case RIGHT:

                //region Right

                x = Math.abs(drawingRect.right - drawingRect.centerX());
                y = Math.abs(drawingRect.centerY() - drawingRect.top);

                result = (float)(Math.toDegrees(Math.asin(y/x)));

                return result;

                //endregion

        }

        //endregion

        return 0f;

    }

    protected float getExplosionAngelSectorForDepth()
    {
        //                                                Performance
        float x = this.getM_DrawingRectangle().height() * 0.5f;
        //                          Performance
        float y = this.getmDepth() * 0.5f;

        float angle = (float)Math.toDegrees(Math.asin(y/x));

        return angle;

    }

    protected float getHitFromInAngles()
    {
        return this.getHitFromInAnglesClockWiseRotation(this.getM_FirstHitFrom(), HitFrom.ABOVE);
    }

    // Direction origo is from where at the clock angle origo takes it point.
    protected float getHitFromInAnglesClockWiseRotation(HitFrom hitFrom, HitFrom directionOrigo)
    {

        float degrees = 0;

        if(directionOrigo == HitFrom.ABOVE)
        {

            //region Noon

            switch (hitFrom)
            {

                case NONE:
                    degrees = 0;
                    break;
                case BELOW:
                    degrees = 180;
                    break;
                case ABOVE:
                    degrees = 0;
                    break;
                case LEFT:
                    degrees = -90;
                    break;
                case RIGHT:
                    degrees = 90;
                    break;
            }

            //endregion

        }
        else
        {

            //region Assumption other integration goes from 9 o'clock

            switch (hitFrom)
            {

                case NONE:
                    degrees = 0;
                    break;
                case BELOW:
                    degrees = 270;
                    break;
                case ABOVE:
                    degrees = 90;
                    break;
                case LEFT:
                    degrees = 0;
                    break;
                case RIGHT:
                    degrees = 180;
                    break;
            }

            //endregion

        }

        return degrees;

    }

    protected abstract Bitmap getRandomDMG1Bitmap();

    // Makes an average value of width and height;
    protected float evaluateFragmentRadius(Bitmap bitmap)
    {
        //                              Performance
        float width = bitmap.getWidth() * 0.5f;
        float height = bitmap.getHeight() * 0.5f;

        return (width + height) / 2f;

    }

    protected Vector2 getHitFromEjectionLocation(float radiusOfBall)
    {
        Vector2 location = new Vector2(this.getM_DrawingRectangle().centerX(), this.getM_DrawingRectangle().centerY());

        switch (this.getM_FirstHitFrom())
        {
            case NONE:
                // No need to adjust.
                break;
            case BELOW:
                location = new Vector2(this.getM_DrawingRectangle().centerX(), this.getM_DrawingRectangle().bottom + radiusOfBall);
                break;
            case ABOVE:
                location = new Vector2(this.getM_DrawingRectangle().centerX(), this.getM_DrawingRectangle().top - radiusOfBall);
                break;
            case LEFT:
                location = new Vector2(this.getM_DrawingRectangle().left - radiusOfBall, this.getM_DrawingRectangle().centerY());
                break;
            case RIGHT:
                location = new Vector2(this.getM_DrawingRectangle().right + radiusOfBall, this.getM_DrawingRectangle().centerY());
                break;
        }

        return location;

    }

    protected abstract int getRandomDMGColorWeighted();

    protected void feedDamageResult(int damage, HitFrom hitFrom)
    {
        if(this.getDamageToSteelBrickExplosionResultSet().containsKey(damage))
        {
            HitFromAndSteelBrickExplosionResultSet hitFromAndSteelBrickExplosionResultSet =
                this.getDamageToSteelBrickExplosionResultSet().get(damage);

            if(hitFromAndSteelBrickExplosionResultSet.getM_HitFromMap().containsKey(hitFrom))
            {
                SteelBrickExplosionResultSet steelBrickExplosionResultSet =
                    hitFromAndSteelBrickExplosionResultSet.getM_HitFromMap().get(hitFrom);

                this.getmNewBallBases().addAll(steelBrickExplosionResultSet.getmNewBallBases());
                this.getmNewParticles().addAll(steelBrickExplosionResultSet.getmNewParticles());

                steelBrickExplosionResultSet.getmNewBallBases().clear();
                steelBrickExplosionResultSet.getmNewParticles().clear();
            }
        }
    }

    protected Pair<HitFrom, SteelBrickExplosionResultSet>
        feedDamageResultPreBuilt(HitFrom hitFrom,
                                 int averageFragmentBalls,
                                 int standardDeviationFragmentBalls,
                                 int averageParticles,
                                 int standardDeviationParticles,
                                 int maxDeviationOfShrapnelAngleDegrees,
                                 ArrayList<BitMapAndName> possibleFragmentBitmaps,
                                 // Defines if the explosion is subsequent of previous explosions. If so the brick i so broken it spreads shrapnel i all directions
                                 boolean chaoticExplosion)
    {

        ArrayList<PaintParticleLine> newParticles = new ArrayList<>();
        PaintParticleLineSystem paintParticleLineSystem = new PaintParticleLineSystem();
        ArrayList<BallBase> mNewBallBases = new ArrayList<>();

        RandomProvider randomProvider = RandomProvider.getInstance();

        float explosionAngel = this.getExplosionAngelSector(hitFrom);

        if(chaoticExplosion)
        {
            explosionAngel = 180;
        }

        float initialDirection = this.getHitFromInAnglesClockWiseRotation(hitFrom, HitFrom.ABOVE);
        float deviationValue;
        float currentAngle;

        /*
        // At this point 0 is when x = -1 and y = 0,
        // Rotate forward to x = 0 and y = 1.
        initialDirection += 90;
        */

        Vector3 direction;

        //region Balls

        int numberOfBalls =
                (int)(Math.round(Math.abs(randomProvider.nextGaussian()) *
                        standardDeviationFragmentBalls) +
                        averageFragmentBalls);

        float ballSectorAngle = (explosionAngel * 2) / numberOfBalls;

        float halfSectorAngle = ballSectorAngle * 0.5f;
        FragmentBall fragmentBall;
        Bitmap randomFragmentBitMap;

        // distribute fragmentation even through left and right. Seen from a below collision.
        for(float i = -explosionAngel;
            i < explosionAngel - ballSectorAngle;
            i+= ballSectorAngle)
        {

            //region Loop

            deviationValue = (randomProvider.nextFloat() * maxDeviationOfShrapnelAngleDegrees) - (maxDeviationOfShrapnelAngleDegrees * 0.5f);

            currentAngle = i + deviationValue + initialDirection + halfSectorAngle ;
            randomFragmentBitMap = possibleFragmentBitmaps.get(randomProvider.nextInt(possibleFragmentBitmaps.size())).getmBitmap();
            direction = new Vector3(0f,-1f, 0f);
            direction.rotateAmongZAxis(currentAngle);

            float expectedRadius = this.evaluateFragmentRadius(randomFragmentBitMap);

            fragmentBall =
                    new FragmentBall(this.getmParentWorld(),
                            expectedRadius,
                            randomFragmentBitMap,
                            this.getHitFromEjectionLocation(expectedRadius),
                            new Vector2(direction.getmX(), direction.getmY()),
                            FragmentBall.getStartingSpeedSlightRandomized());

            mNewBallBases.add(fragmentBall);

            //endregion

        }

        //endregion

        //region Fragments

        float fragmentOneDirectionDeviationPossibilities = this.getExplosionAngelSectorForDepth();
        float xAxisRandomRotation;

        int numberOfFragments =
                (int)(Math.round(Math.abs(randomProvider.nextGaussian()) *
                        standardDeviationParticles)) +
                        averageParticles;

        float fragmentSectorAngle = (explosionAngel * 2) / numberOfFragments;

        float halfFragmentSectorAngle = fragmentSectorAngle * 0.5f;
        PaintParticleLine paintParticleLine;
        int randomFragmentColor;

        Stack<PaintParticleLine> newParticlesStack = new Stack<>();


        // This equation can give one less than the loop below
        int objectsToPull = (int)(Math.ceil((explosionAngel * 2) / fragmentSectorAngle)) + 1;
        newParticlesStack.addAll(SteelBrickParticleEngine.getInstance().pullParticles(objectsToPull));

        // distribute fragmentation even through left and right. Seen from a below collision.
        for(float i = -explosionAngel;
            i < explosionAngel - fragmentSectorAngle;
            i+= fragmentSectorAngle)
        {

            //region Loop

            /* Don't deviate too much performance requirements
            deviationValue = (r.nextFloat() * maxDeviationOfShrapnelAngleDegrees) -
                    (maxDeviationOfShrapnelAngleDegrees * 0.5f);

            */
            deviationValue = 0;

            currentAngle = i + deviationValue + halfFragmentSectorAngle + initialDirection;
            randomFragmentColor = this.getRandomDMGColorWeighted();
            direction = new Vector3(0f,-1f, 0f);
            direction.rotateAmongZAxis(currentAngle);

            xAxisRandomRotation = (randomProvider.nextFloat() - 0.5f) * (2 * fragmentOneDirectionDeviationPossibilities);
            direction.rotateAmongXAxis(xAxisRandomRotation);

            paintParticleLine = newParticlesStack.pop();
            paintParticleLine.setmOwnedBy(paintParticleLineSystem);
            // Adjust particle
            paintParticleLine.mLocation3d = new Vector3NotEncapsulated(this.getM_DrawingRectangle().centerX(),
                    //paintParticleLine.setmLocation3d(new Vector3NotEncapsulated(this.getM_DrawingRectangle().centerX(),
                    this.getM_DrawingRectangle().centerY(),
                    0f);
            paintParticleLine.updateMotionSecondOnlyDirection(direction.toVector3NotEncapsulated());
            paintParticleLine.setmColor(randomFragmentColor);

            newParticles.add(paintParticleLine);

            //endregion

        }

        //endregion


        paintParticleLineSystem.mParticles.addAll(newParticles);

        ArrayList<PaintParticleLineSystem> inputSystems = new ArrayList<>();
        inputSystems.add(paintParticleLineSystem);

        Pair<HitFrom, SteelBrickExplosionResultSet> result =
            new Pair<>(hitFrom, new SteelBrickExplosionResultSet(inputSystems, mNewBallBases));
        return result;

    }

    protected void setUpOneDamageImage(BitMapAndName steelDamage1Bottom,
                                       BitMapAndName steelDamage1Top,
                                       BitMapAndName steelDamage1Left,
                                       BitMapAndName steelDamage1Right,
                                       Size steelOriginalSize,
                                       BitMapAndName fragment1,
                                       BitMapAndName fragment2,
                                       BitMapAndName fragment3)
    {

        //region Shuffle image uv direction. Mirror images along interesting axis

        steelDamage1Bottom = this.passImageThroughUVInverter(UV.U, steelDamage1Bottom);
        steelDamage1Top = this.passImageThroughUVInverter(UV.U, steelDamage1Top);
        steelDamage1Left = this.passImageThroughUVInverter(UV.V, steelDamage1Left);
        steelDamage1Right = this.passImageThroughUVInverter(UV.V, steelDamage1Right);

        fragment1 = this.passImageThroughUVInverter(UV.V, fragment1);
        fragment2 = this.passImageThroughUVInverter(UV.V, fragment2);
        fragment3 = this.passImageThroughUVInverter(UV.V, fragment3);

        //endregion

        BitmapAndCustomInfo bitmapAndCustomInfo = new BitmapAndCustomInfo();

        //region The image

        Map<HitFrom, BitMapAndName> newBitmapHitFromMap = new HashMap<>();

        newBitmapHitFromMap.put(HitFrom.NONE, super.scaleBitmapToBrickSize(steelDamage1Bottom));
        newBitmapHitFromMap.put(HitFrom.BELOW, super.scaleBitmapToBrickSize(steelDamage1Bottom));
        newBitmapHitFromMap.put(HitFrom.ABOVE, super.scaleBitmapToBrickSize(steelDamage1Top));
        newBitmapHitFromMap.put(HitFrom.LEFT, super.scaleBitmapToBrickSize(steelDamage1Left));
        newBitmapHitFromMap.put(HitFrom.RIGHT, super.scaleBitmapToBrickSize(steelDamage1Right));

        bitmapAndCustomInfo.setmBitMap(newBitmapHitFromMap);

        //endregion

        //region The scale

        Vector2 scale =
            new Vector2(super.getM_DrawingRectangle().width() / steelOriginalSize.getWidth(),
            super.getM_DrawingRectangle().height() / steelOriginalSize.getHeight());

        bitmapAndCustomInfo.setmScale(scale);

        //endregion

        //region Fragments

        float degrees;
        float imageDegrees;
        BitMapAndName fragment1Resized = null;
        BitMapAndName fragment2Resized = null;
        BitMapAndName fragment3Resized = null;

        bitmapAndCustomInfo.setmFragments(new HashMap<HitFrom, ArrayList<BitMapAndName>>());

        //region None

        /*

        bitmapAndCustomInfo.getmFragments().put(HitFrom.NONE, new ArrayList<Bitmap>());
        degrees = this.getHitFromInAngles(HitFrom.NONE);
        imageDegrees = degrees + 90;

        fragment1Resized = this.scaleFragment(false,
                fragment1,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.NONE).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(false,
                fragment2,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.NONE).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(false,
                fragment3,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.NONE).add(fragment3Resized);

*/

        //endregion

        //region Above

        bitmapAndCustomInfo.getmFragments().put(HitFrom.ABOVE, new ArrayList<BitMapAndName>());
        degrees = this.getHitFromInAnglesClockWiseRotation(HitFrom.ABOVE, HitFrom.LEFT);
        imageDegrees = degrees;

        fragment1Resized = this.scaleFragment(false,
                                              fragment1,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.ABOVE).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(false,
                                              fragment2,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.ABOVE).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(false,
                                              fragment3,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.ABOVE).add(fragment3Resized);

        //endregion

        //region Below

        bitmapAndCustomInfo.getmFragments().put(HitFrom.BELOW, new ArrayList<BitMapAndName>());
        degrees = this.getHitFromInAnglesClockWiseRotation(HitFrom.BELOW, HitFrom.LEFT);
        imageDegrees = degrees;

        fragment1Resized = this.scaleFragment(false,
                                              fragment1,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.BELOW).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(false,
                                              fragment2,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.BELOW).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(false,
                                              fragment3,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.BELOW).add(fragment3Resized);

        //endregion

        //region Left

        bitmapAndCustomInfo.getmFragments().put(HitFrom.LEFT, new ArrayList<BitMapAndName>());
        degrees = this.getHitFromInAnglesClockWiseRotation(HitFrom.LEFT, HitFrom.LEFT);
        imageDegrees = degrees;

        fragment1Resized = this.scaleFragment(true,
                                              fragment1,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.LEFT).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(true,
                                              fragment2,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.LEFT).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(true,
                                              fragment3,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.LEFT).add(fragment3Resized);

        //endregion

        //region Right

        bitmapAndCustomInfo.getmFragments().put(HitFrom.RIGHT, new ArrayList<BitMapAndName>());
        degrees = this.getHitFromInAnglesClockWiseRotation(HitFrom.RIGHT, HitFrom.LEFT);
        imageDegrees = degrees;

        fragment1Resized = this.scaleFragment(true,
                                              fragment1,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.RIGHT).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(true,
                                              fragment2,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.RIGHT).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(true,
                                              fragment3,
                                              scale,
                                              imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.RIGHT).add(fragment3Resized);

        //endregion

        //endregion

        this.oneDamageImage = bitmapAndCustomInfo;

    }

    protected void setUpTwoDamageImage(BitMapAndName steelDamage2Bottom,
                                       BitMapAndName steelDamage2Top,
                                       BitMapAndName steelDamage2Left,
                                       BitMapAndName steelDamage2Right,
                                       Size steelOriginalSize,
                                       BitMapAndName fragment1,
                                       BitMapAndName fragment2,
                                       BitMapAndName fragment3)
    {

        //region Shuffle image uv direction. Mirror images along interesting axis

        steelDamage2Bottom = this.passImageThroughUVInverter(UV.U, steelDamage2Bottom);
        steelDamage2Top = this.passImageThroughUVInverter(UV.U, steelDamage2Top);
        steelDamage2Left = this.passImageThroughUVInverter(UV.V, steelDamage2Left);
        steelDamage2Right = this.passImageThroughUVInverter(UV.V, steelDamage2Right);

        fragment1 = this.passImageThroughUVInverter(UV.V, fragment1);
        fragment2 = this.passImageThroughUVInverter(UV.V, fragment2);
        fragment3 = this.passImageThroughUVInverter(UV.V, fragment3);

        //endregion

        BitmapAndCustomInfo bitmapAndCustomInfo = new BitmapAndCustomInfo();

        //region The image

        Map<HitFrom, BitMapAndName> newBitmapHitFromMap = new HashMap<>();

        newBitmapHitFromMap.put(HitFrom.NONE, super.scaleBitmapToBrickSize(steelDamage2Bottom));
        newBitmapHitFromMap.put(HitFrom.BELOW, super.scaleBitmapToBrickSize(steelDamage2Bottom));
        newBitmapHitFromMap.put(HitFrom.ABOVE, super.scaleBitmapToBrickSize(steelDamage2Top));
        newBitmapHitFromMap.put(HitFrom.LEFT, super.scaleBitmapToBrickSize(steelDamage2Left));
        newBitmapHitFromMap.put(HitFrom.RIGHT, super.scaleBitmapToBrickSize(steelDamage2Right));

        bitmapAndCustomInfo.setmBitMap(newBitmapHitFromMap);

        //endregion

        //region The scale

        Vector2 scale =
                new Vector2(super.getM_DrawingRectangle().width() / steelOriginalSize.getWidth(),
                        super.getM_DrawingRectangle().height() / steelOriginalSize.getHeight());

        bitmapAndCustomInfo.setmScale(scale);

        //endregion

        //region Fragments

        float degrees;
        float imageDegrees;
        BitMapAndName fragment1Resized = null;
        BitMapAndName fragment2Resized = null;
        BitMapAndName fragment3Resized = null;

        bitmapAndCustomInfo.setmFragments(new HashMap<HitFrom, ArrayList<BitMapAndName>>());

        //region None

        /*

        bitmapAndCustomInfo.getmFragments().put(HitFrom.NONE, new ArrayList<Bitmap>());
        degrees = this.getHitFromInAngles(HitFrom.NONE);
        imageDegrees = degrees + 90;

        fragment1Resized = this.scaleFragment(false,
                fragment1,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.NONE).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(false,
                fragment2,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.NONE).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(false,
                fragment3,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.NONE).add(fragment3Resized);

*/

        //endregion

        //region Above

        bitmapAndCustomInfo.getmFragments().put(HitFrom.ABOVE, new ArrayList<BitMapAndName>());
        degrees = this.getHitFromInAnglesClockWiseRotation(HitFrom.ABOVE, HitFrom.LEFT);
        imageDegrees = degrees;

        fragment1Resized = this.scaleFragment(false,
                fragment1,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.ABOVE).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(false,
                fragment2,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.ABOVE).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(false,
                fragment3,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.ABOVE).add(fragment3Resized);

        //endregion

        //region Below

        bitmapAndCustomInfo.getmFragments().put(HitFrom.BELOW, new ArrayList<BitMapAndName>());
        degrees = this.getHitFromInAnglesClockWiseRotation(HitFrom.BELOW, HitFrom.LEFT);
        imageDegrees = degrees;

        fragment1Resized = this.scaleFragment(false,
                fragment1,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.BELOW).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(false,
                fragment2,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.BELOW).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(false,
                fragment3,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.BELOW).add(fragment3Resized);

        //endregion

        //region Left

        bitmapAndCustomInfo.getmFragments().put(HitFrom.LEFT, new ArrayList<BitMapAndName>());
        degrees = this.getHitFromInAnglesClockWiseRotation(HitFrom.LEFT, HitFrom.LEFT);
        imageDegrees = degrees;

        fragment1Resized = this.scaleFragment(true,
                fragment1,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.LEFT).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(true,
                fragment2,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.LEFT).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(true,
                fragment3,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.LEFT).add(fragment3Resized);

        //endregion

        //region Right

        bitmapAndCustomInfo.getmFragments().put(HitFrom.RIGHT, new ArrayList<BitMapAndName>());
        degrees = this.getHitFromInAnglesClockWiseRotation(HitFrom.RIGHT, HitFrom.LEFT);
        imageDegrees = degrees;

        fragment1Resized = this.scaleFragment(true,
                fragment1,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.RIGHT).add(fragment1Resized);

        fragment2Resized = this.scaleFragment(true,
                fragment2,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.RIGHT).add(fragment2Resized);

        fragment3Resized = this.scaleFragment(true,
                fragment3,
                scale,
                imageDegrees);

        bitmapAndCustomInfo.getmFragments().get(HitFrom.RIGHT).add(fragment3Resized);

        //endregion

        //endregion

        this.twoDamageImage = bitmapAndCustomInfo;

    }

    //endregion

    //region Private

    private void myInitialize()
    {
        this.setmNewBallBases(new ArrayList<BallBase>());
        this.setmNewParticles(new ArrayList<ParticleSystemBase>());
        this.setDamageToSteelBrickExplosionResultSet(new HashMap<Integer, HitFromAndSteelBrickExplosionResultSet>());

        this.mRunningValueUVMap = RandomProvider.getInstance().nextInt(this.INVERT_FREQUENCY_DENOMINATOR);

        this.m_BrickTilt = 0f;
    }

    private BitMapAndName passImageThroughUVInverter(UV uV, BitMapAndName bitMapAndName)
    {

        this.mRunningValueUVMap++;

        if(uV == UV.NONE)
        {
            return  bitMapAndName;
        }

        if(this.mRunningValueUVMap % this.INVERT_FREQUENCY_DENOMINATOR == 0)
        {
            return ResourceHolderImages.getInstance().invertImage(bitMapAndName, uV);
        }
        else
        {
            return bitMapAndName;
        }
    }

    private BitMapAndName scaleFragment(boolean invertImageDimensions,
                                        BitMapAndName fragment,
                                        Vector2 scale,
                                        float imageDegrees)
    {

        BitMapAndName fragmentResizedAndRotated =
            ResourceHolderImages.getInstance().rotateBitMapInvertAndScale(fragment,
                                                                          imageDegrees,
                                                                          invertImageDimensions,
                                                                          scale);

        return fragmentResizedAndRotated;

    }

    //endregion

    //endregion

}
