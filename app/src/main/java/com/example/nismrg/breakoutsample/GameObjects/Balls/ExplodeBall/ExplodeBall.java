package com.example.nismrg.breakoutsample.GameObjects.Balls.ExplodeBall;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Size;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.Balls.BasicBall;
import com.example.nismrg.breakoutsample.GameObjects.Balls.FastBall.FastBall;
import com.example.nismrg.breakoutsample.GameObjects.Balls.RotatingBallBase;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.Explosion;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.ParticleBase;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by nismrg on 2016-07-10.
 */
public class ExplodeBall extends RotatingBallBase
{

    //region Constants

    //region Private

    private final int LAUNCH_SPEED_UNITS_SECOND = 600;
    private final int DEFAULT_RADIUS = 25;
    private final int MIN_FRAGMENTS = 3;
    private final int MAX_FRAGMENTS = 7;
    private final float AVERAGE_HIT_ROTATION_IN_REVELATIONS_A_SECOND = 0f;
    private final float STD_HIT_ROTATION_IN_REVELATIONS_A_SECOND = 180f;
    private final Vector2 IMAGE_SCALE = new Vector2(2f, 2f);
    private final float EXPLOSION_SIZE_TO_THIS = 3f;
    private final int EXPLOSION_LIFE_TIME_IN_MILLI_SECONDS = 1000;

    //endregion

    //endregion

    //region Variables

    //region Private

    private BitMapAndName mMyImage;
    private Size mImageDisplacement;
    private Explosion mMyExplosion;

    //endregion

    //endregion

    //region Constructors

    public ExplodeBall(World world)
    {
        super(world);

        this.setM_Radius(DEFAULT_RADIUS);

        this.myInitialize();

    }

    public ExplodeBall(World world, float radius)
    {

        super(world, radius);

        this.myInitialize();
    }

    //endregion

    //region Methods

    //region Public

    public float getStartingSpeed()
    {
        return LAUNCH_SPEED_UNITS_SECOND;
    }

    public void drawMeAt(DrawInformation drawInformation,
                         Vector2 location)
    {

        Canvas canvas = drawInformation.getmCanvas();


        Matrix matrix = new Matrix();

        matrix.preRotate(super.m_currentRotation, this.mImageDisplacement.getWidth(), this.mImageDisplacement.getHeight());

        matrix.postTranslate(location.getM_x() - this.mImageDisplacement.getWidth(),
                             location.getM_Y() - this.mImageDisplacement.getHeight());


        canvas.drawBitmap(this.mMyImage.getmBitmap(),
                          matrix,
                          null);


    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation) {

        //super.drawMe(canvas, gameTimeRunMillis);

        this.drawMeAt(drawInformation, this.getM_Location());
    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        if(super.mStruckSomething)
        {
            super.assignRandomRotation(this.AVERAGE_HIT_ROTATION_IN_REVELATIONS_A_SECOND, this.STD_HIT_ROTATION_IN_REVELATIONS_A_SECOND);
        }

        super.updateMe(updateInformation);

    }

    public NewBallsAndExplosions explode()
    {

        ArrayList<BallBase> fragments = new ArrayList<>();

        this.setM_Dead(true);

        Random r = new Random();

        int numberOfFragments =
            MAX_FRAGMENTS - MIN_FRAGMENTS > 0 ?
                r.nextInt(MAX_FRAGMENTS - MIN_FRAGMENTS) + MIN_FRAGMENTS :
                MIN_FRAGMENTS;

        for(int i = 0; i < numberOfFragments; i++)
        {
            int randomBallType = r.nextInt(4);

            switch(randomBallType)
            {
                case 0: fragments.add(new BasicBall(super.getM_World()));
                        break;
                case 1: fragments.add(new FastBall(super.getM_World()));
                        break;
                case 2: fragments.add(new BasicBall(super.getM_World()));
                        break;
                case 3: fragments.add(new ExplodeBall(super.getM_World()));
                        break;
                default: break;
            }

        }

        float degrees = 360f / fragments.size();

        for(int i = 0; i < fragments.size(); i++)
        {
            fragments.get(i).setM_Location(new Vector2(this.getM_Location().getM_x(), this.getM_Location().getM_Y()));
            fragments.get(i).setMotionSecondExternal(new Vector2(this.getMotionSecond().getM_x(), this.getMotionSecond().getM_Y()));
            this.setDistributedVectorForExplosion(fragments.get(i), degrees * (i));
        }

        this.mMyExplosion.setLocationCascade(this.getM_Location());
        ArrayList<ParticleBase> particles = new ArrayList<>();
        particles.add(this.mMyExplosion);

        return new NewBallsAndExplosions(fragments, particles);

    }

    public void setDistributedVectorForExplosion(BallBase ballBase, float degrees)
    {
        //Random random = new Random();
        //, random.setSeed(System.currentTimeMillis());
        //float rotationLaps = random.nextFloat();



        ballBase.getMotionSecond().rotateAmongZAxis(degrees);
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize()
    {

        ResourceHolderImages resourceHolderImages = ResourceHolderImages.getInstance();

        this.mMyImage = resourceHolderImages.getResizedBitmap(resourceHolderImages.getmBomb(),
                                                              Math.round(this.getM_Radius() * 2 * this.IMAGE_SCALE.getM_x()),
                                                              Math.round(this.getM_Radius() * 2 * this.IMAGE_SCALE.getM_Y()));
        this.mImageDisplacement = new Size(Math.round(this.mMyImage.getmBitmap().getWidth() * 0.5f), Math.round(this.mMyImage.getmBitmap().getHeight() * 0.5f));

        this.assignExplosion();
    }

    private void assignExplosion()
    {
        this.mMyExplosion = ExplosionBrickParticleEngine.getInstance().pullSingleParticle();
        this.mMyExplosion.updateSize((int)(this.getM_Radius() * 2), (int)(this.getM_Radius() * 2));
        this.mMyExplosion.setExplosionSizeToOwner(this.EXPLOSION_SIZE_TO_THIS);
    }

    //endregion

    //endregion

}
