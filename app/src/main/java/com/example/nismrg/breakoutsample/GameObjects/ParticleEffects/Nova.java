package com.example.nismrg.breakoutsample.GameObjects.ParticleEffects;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.Math.Vector1;
import com.example.nismrg.breakoutsample.Math.Vector2;

/**
 * Created by nismrg on 2016-08-19.
 */
public abstract class Nova extends ParticleBase
{

    //region Variables

    //region Private

    private float mExplosionSpeedPixelsSecond;
    private Vector1 mExplosionEdgePixels;

    //endregion

    //endregion

    //region Encapsulation

    public float getmExplosionSpeedPixelsSecond() {
        return mExplosionSpeedPixelsSecond;
    }

    public void setmExplosionSpeedPixelsSecond(float mExplosionSpeedPixelsSecond) {
        this.mExplosionSpeedPixelsSecond = mExplosionSpeedPixelsSecond;
    }

    public Vector1 getmExplosionEdgePixels() {
        return mExplosionEdgePixels;
    }

    public void setmExplosionEdgePixels(Vector1 mExplosionEdgePixels) {
        this.mExplosionEdgePixels = mExplosionEdgePixels;
    }

    //endregion

    //region Constructors

    public Nova(Vector2 location,
                Vector2 motionSecond,
                int millisecondsToDeath,
                float explosionSpeedPixelsSecond)
    {
        super(location,
              motionSecond,
              millisecondsToDeath);

        this.myInitialize(explosionSpeedPixelsSecond);
    }

    //endregion

    //region Methods

    //region Public

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        super.drawMe(drawInformation);

    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        super.updateMe(updateInformation);

        this.getmExplosionEdgePixels().addVector(new Vector1(updateInformation.mOneDividedByFPS * this.getmExplosionSpeedPixelsSecond()));

    }

    @Override
    public int getM_AtFrameFragmentsFrame() {
        return super.getM_AtFrameFragmentsFrame();
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize(float explosionSpeedPixelsSecond)
    {
        this.setmExplosionSpeedPixelsSecond(explosionSpeedPixelsSecond);
        this.setmExplosionEdgePixels(new Vector1(0f));
    }

    //endregion

}
