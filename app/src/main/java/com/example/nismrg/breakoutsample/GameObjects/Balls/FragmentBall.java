package com.example.nismrg.breakoutsample.GameObjects.Balls;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Size;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector2;

import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-07-10.
 */
public class FragmentBall extends BallBase
{

    //region Instance

    //region Variables

    //region Private

    private Bitmap mFragmentImage;
    private Size mHalfImageSize;

    //endregion

    //endregion

    //region Encapsulation

    public Bitmap getmFragmentImage() {
        return mFragmentImage;
    }

    public void setmFragmentImage(Bitmap mFragmentImage) {
        this.mFragmentImage = mFragmentImage;
    }

    //endregion

    //region Constructors

    public FragmentBall(World world,
                        float radius,
                        Bitmap fragmentImage,
                        Vector2 location,
                        Vector2 direction,
                        Float speed) //should be normalized.
    {

        super(world, radius);

        this.myInitialize(radius,
                          fragmentImage,
                          location,
                          direction,
                          speed);

    }

    //endregion

    //region Methods

    //region Public

    public float getStartingSpeed()
    {
        return LAUNCH_SPEED_UNITS_SECOND;
    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        super.updateMe(updateInformation);
    }

        public void drawMeAt(DrawInformation drawInformation, Vector2 location)
    {

        Canvas canvas = drawInformation.getmCanvas();

        Bitmap bitmap = this.getmFragmentImage();
       // RectF rectf = super.getLocationInRectangle();


        /*


        Paint p = new Paint();
        p.setColor(Color.YELLOW);

        Paint pp = new Paint();
        pp.setColor(Color.GRAY);


        canvas.drawCircle(this.getM_Location().getM_x(), this.getM_Location().getM_Y(), 10, p);
        canvas.drawCircle(this.getTopDerivative().getM_x(), this.getTopDerivative().getM_Y(), 10, pp);
        canvas.drawCircle(this.getBottomDerivative().getM_x(), this.getBottomDerivative().getM_Y(), 10, pp);
        canvas.drawCircle(this.getLeftDerivative().getM_x(), this.getLeftDerivative().getM_Y(), 10, pp);
        canvas.drawCircle(this.getRightDerivative().getM_x(), this.getRightDerivative().getM_Y(), 10, pp);
*/

        canvas.drawBitmap(bitmap,
                super.getM_Location().getM_x() - this.mHalfImageSize.getWidth(),
                super.getM_Location().getM_Y() - this.mHalfImageSize.getHeight(),
                null);


    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation) {

        //super.drawMe(canvas, gameTimeRunMillis);

        this.drawMeAt(drawInformation, this.getM_Location());
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize(float radius,
                              Bitmap fragmentImage,
                              Vector2 location,
                              Vector2 direction,
                              Float speed)
    {
        this.setmFragmentImage(fragmentImage);
        this.setM_Radius(radius);
        this.setM_Location(location);

        if(speed == null)
        {
            direction.scaleToLength(LAUNCH_SPEED_UNITS_SECOND);
            this.setMotionSecondExternal(direction);
        }
        else
        {
            direction.scaleToLength(speed);
            this.setMotionSecondExternal(direction);
        }

        this.mHalfImageSize = new Size((int)(this.getmFragmentImage().getWidth() * 0.5f),
                                       (int)(this.getmFragmentImage().getHeight() * 0.5f));

        // Make this true if balls can explode in a chaotic way. E.a. all directions
        this.setmCanInteractWithBricks(true);

    }

    //endregion

    //endregion

    //endregion

    //region Static

    //region Constants

    //region Private

    private static final float LAUNCH_SPEED_UNITS_SECOND = 700f;
    private static final float LAUNCH_SPEED_STANDARD_DEVIATION_UNITS_SECOND = 100f;

    //endregion

    //endregion

    //region Methods

    //region Public

    public static float getStartingSpeedSlightRandomized()
    {

        float result = FragmentBall.LAUNCH_SPEED_UNITS_SECOND;

        RandomProvider randomProvider = RandomProvider.getInstance();

        result += randomProvider.nextGaussian() * FragmentBall.LAUNCH_SPEED_STANDARD_DEVIATION_UNITS_SECOND;

        return result;
    }

    public static float getStartingSpeedExact()
    {
        return FragmentBall.LAUNCH_SPEED_UNITS_SECOND;
    }

    //endregion

    //endregion

    //endregion

}
