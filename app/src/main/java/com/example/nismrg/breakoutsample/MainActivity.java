package com.example.nismrg.breakoutsample;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Window;

import com.example.nismrg.breakoutsample.SensorFusion.OrientationProviders.OrientationProviders.AccelerometerCompassProvider;
import com.example.nismrg.breakoutsample.SensorFusion.OrientationProviders.OrientationProviders.CalibratedGyroscopeProvider;
import com.example.nismrg.breakoutsample.SensorFusion.OrientationProviders.OrientationProviders.GravityCompassProvider;
import com.example.nismrg.breakoutsample.SensorFusion.OrientationProviders.OrientationProviders.ImprovedOrientationSensor1Provider;
import com.example.nismrg.breakoutsample.SensorFusion.OrientationProviders.OrientationProviders.ImprovedOrientationSensor2Provider;
import com.example.nismrg.breakoutsample.SensorFusion.OrientationProviders.OrientationProviders.OrientationProvider;
import com.example.nismrg.breakoutsample.SensorFusion.OrientationProviders.OrientationProviders.RotationVectorProvider;

public class MainActivity extends Activity {

    //region Constants

    //region Private



    //endregion

    //endregion

    // region Variables

    // region private

    private GameView m_GameView = null;
    boolean useRotationSensors;


    /**
     * The current orientation provider that delivers device orientation.
     */
    public OrientationProvider currentOrientationProvider;

    //endregion

    //endregion


    //region Events

    @Override
    protected void onResume() {
        super.onResume();

        if(this.getUsesSensors()) {
            currentOrientationProvider.start();
        }

        this.m_GameView.resume();

//this.tester.start();


    }

    @Override
    protected void onPause() {
        super.onPause();


        if(this.getUsesSensors()) {
            currentOrientationProvider.stop();
        }


        this.m_GameView.pause();

//        this.tester.pause();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /*
        int colorNo = ContextCompat.getColor(getApplicationContext(),R.color.green);
        Paint p = new Paint(colorNo);
        */
/*
        public static final int getColor(Context context, int id) {
            final int version = Build.VERSION.SDK_INT;
            if (version >= 23) {
                return ContextCompatApi23.getColor(context, id);
            } else {
                return context.getResources().getColor(id);
            }
        }
  */

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        this.m_GameView = (GameView) this.findViewById(R.id.game);

        this.sensorInitialization();
        this.m_GameView.setmOrientationProvider(this.currentOrientationProvider);

        //this.tester = new Tester((TextView) this.findViewById(R.id.textViewinfo), this);

    }



    public void sensorInitialization(/*LayoutInflater inflater,
                                     ViewGroup container,
                                     Bundle savedInstanceState*/) {

        final int selection = 2;

        SensorManager sm = (SensorManager) this.getSystemService(MainActivity.SENSOR_SERVICE);

        // Initialise the orientationProvider
        switch (selection) {
            case 1:

                currentOrientationProvider = new ImprovedOrientationSensor1Provider(sm);

                break;
            case 2:
                currentOrientationProvider = new ImprovedOrientationSensor2Provider(sm);
                break;
            case 3:
                currentOrientationProvider = new RotationVectorProvider(sm);
                break;
            case 4:
                currentOrientationProvider = new CalibratedGyroscopeProvider(sm);
                break;
            case 5:
                currentOrientationProvider = new GravityCompassProvider(sm);
                break;
            case 6:
                currentOrientationProvider = new AccelerometerCompassProvider(sm);
                break;
            default:
                break;
        }
    }

    private boolean getUsesSensors()
    {

        ApplicationInfo applicationInfo = null;

        boolean useSensors = false;

        try
        {
            applicationInfo = this.getPackageManager().getApplicationInfo(this.getPackageName(), PackageManager.GET_META_DATA);
            useSensors = (boolean)applicationInfo.metaData.get("UseSensors");

        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }

        return useSensors;

    }

    @Override
    public PackageManager getPackageManager() {
        return super.getPackageManager();
    }

    @Override
    public String getPackageName() {
        return super.getPackageName();
    }

    /*
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent)
    {


        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                int iii = 2;
                break;
            case MotionEvent.ACTION_UP:
                int a = 2;
                break;
        }

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                int iii = 2;
                break;
            case MotionEvent.ACTION_UP:
                int a = 2;
                break;
        }

        return false;
    }
*/
    //region IOnClickListener
/*
    @Override
    public void onClick(DialogInterface dialogInterface, int i)
    {
        // This is hidden by on touch and should be implemented at view level
    }
*/
    //endregion

    //endregion

}
