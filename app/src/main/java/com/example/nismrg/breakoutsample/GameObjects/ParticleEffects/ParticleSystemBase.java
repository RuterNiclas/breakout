package com.example.nismrg.breakoutsample.GameObjects.ParticleEffects;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.GameObject;

import java.util.ArrayList;

/**
 * Created by nismrg on 2017-01-11.
 */
public class ParticleSystemBase<T extends ParticleBase> extends GameObject
{

    //region Variables

    //region Public

    // Non encapsulated du performance
    public ArrayList<T> mParticles;
    // Non encapsulated du performance
    public boolean misDead;

    //endregion

    //endregion

    //region Constructors

    public ParticleSystemBase()
    {
        super();

        this.myInitialize();
    }

    //endregion

    //region Methods

    //region Public

    public void assignChildrenWithOwner()
    {
        for(ParticleBase particleBase : this.mParticles)
        {
            particleBase.setmOwnedBy(this);
        }
    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {
        for(ParticleBase particleBase : this.mParticles)
        {
            particleBase.drawMe(drawInformation);
        }
    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        this.misDead = true;

        for(ParticleBase particleBase : this.mParticles)
        {
            particleBase.updateMe(updateInformation);
        }
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize()
    {
        this.mParticles = new ArrayList<>();
        this.misDead = false;
    }

    //endregion

    //endregion

}
