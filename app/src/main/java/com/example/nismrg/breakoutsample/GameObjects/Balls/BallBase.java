package com.example.nismrg.breakoutsample.GameObjects.Balls;

import android.graphics.RectF;

import com.example.nismrg.breakoutsample.DeviceFunctions.DeviceFunctions;
import com.example.nismrg.breakoutsample.DeviceFunctions.VibrationEnums;
import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.GameObject;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector2;

import Utility.MathUtility;

/**
 * Created by nismrg on 2016-05-17.
 */
public abstract class BallBase extends GameObject
{

    //region Constants

    //region Private

    //region Vibrations

    private static final boolean VIBRATES_ON_HIT_SCREEN_BOUNDARIES = true;
    private static final long[] VIBRATE_PATTERN_ON_HIT_SCREEN_BOUNDARIES = new long[]{ 0, VibrationEnums.LENGTH_SMALL };
    private static final int VIBRATIONS_REPEAT_ON_HIT_SCREEN_BOUNDARIES = -1;

    //endregion

    // Falling acceleration in pixels
    private final float GRAVITY_IN_PX = 5f;

    // Collision respite from creation in milliseconds
    private final int COLLISION_CREATION_RESPITE_MILLISECONDS = 500;

    //endregion

    //endregion

    //region variables

    //region private

    private int mCollisionRespiteTimeLeft;
    private float m_Radius;
    private Vector2 m_Location;
    private Vector2 motionSecond;
    private World m_World;
    private boolean m_Dead;

    private Vector2 m_AccelerationComponent;

    private float mMotionLengthBackup;

    private boolean mCanInteractWithBricks;

    //endregion

    //region Public

    public boolean mStruckSomething;

    //endregion

    //endregion

    //region Encapsulation

    //region Public

    public Vector2 getMotionSecond()
    {
        return this.motionSecond;
    }

    public float getM_Radius() {
        return m_Radius;
    }

    public void setM_Radius(float m_Radius) {
        this.m_Radius = m_Radius;
    }

    public Vector2 getM_Location() {
        return m_Location;
    }

    public void setM_Location(Vector2 m_Location) {
        this.m_Location = m_Location;
    }

    public boolean isM_Dead() {
        return m_Dead;
    }

    public void setM_Dead(boolean m_Dead) {
        this.m_Dead = m_Dead;
    }

    public World getM_World() {
        return m_World;
    }

    public void setM_World(World m_World) {
        this.m_World = m_World;
    }

    public int getmCollisionRespiteTimeLeft() {
        return mCollisionRespiteTimeLeft;
    }

    public void setmCollisionRespiteTimeLeft(int mCollisionRespiteTimeLeft) {
        this.mCollisionRespiteTimeLeft = mCollisionRespiteTimeLeft;
    }

    public boolean ismCanInteractWithBricks() {
        return mCanInteractWithBricks;
    }

    public void setmCanInteractWithBricks(boolean mCanInteractWithBricks) {
        this.mCanInteractWithBricks = mCanInteractWithBricks;
    }

    //region Vector derivates

    //region Vectors

    // Point where a circle have derivate of f'(x) = 0 and {y > 0}
    public Vector2 getTopDerivative()
    {
        return this.getM_Location().nonMatrixTranslate(0f, -this.getM_Radius());
    }

    // Point where a circle have derivate of f'(x) = 0 and  {y <= 0}
    public Vector2 getBottomDerivative()
    {
        return this.getM_Location().nonMatrixTranslate(0f, this.getM_Radius());
    }

    // Point where circle derivate of f'(x) = 1/0 and {x => 0}
    public Vector2 getLeftDerivative()
    {
        return this.getM_Location().nonMatrixTranslate(-this.getM_Radius(), 0f);
    }

    // Point where circle derivate of f'(x) = 1/0 and {x < 1}
    public Vector2 getRightDerivative()
    {
        return this.getM_Location().nonMatrixTranslate(this.getM_Radius(), 0f);
    }



    //endregion

    //region Private

    private void setMotionSecond(Vector2 motionSecond)
    {
        this.mMotionLengthBackup = motionSecond.getLength();
        this.motionSecond = motionSecond;
    }

    //endregion

    //endregion

    //region RectF

    // Point where a circle have derivate of f'(x) = 0 and {y > 0}
    public RectF getTopDerivativeRect()
    {
        Vector2 topDerivate = this.getTopDerivative();
        return new RectF(topDerivate.getM_x() -1, // Must give this rect an area.
                         topDerivate.getM_Y() - 1,
                         topDerivate.getM_x(),
                         topDerivate.getM_Y());
    }

    // Point where a circle have derivate of f'(x) = 0 and  {y <= 0}
    public RectF getBottomDerivativeRect()
    {
        Vector2 bottomDerivate = this.getBottomDerivative();
        return  new RectF(bottomDerivate.getM_x() - 1, // Must give this rect an area.
                          bottomDerivate.getM_Y(),
                          bottomDerivate.getM_x(),
                          bottomDerivate.getM_Y() + 1);
    }

    // Point where circle derivate of f'(x) = 1/0 and {x => 0}
    public RectF getLeftDerivativeRect()
    {
        Vector2 leftDerivate = this.getLeftDerivative();
        return new RectF(leftDerivate.getM_x() - 1,
                         leftDerivate.getM_Y(),
                         leftDerivate.getM_x(),
                         leftDerivate.getM_Y() + 1); // Must give this rect an area.
    }

    // Point where circle derivate of f'(x) = 1/0 and {x < 1}
    public RectF getRightDerivativeRect()
    {
        Vector2 rightDerivate = this.getRightDerivative();
        return  new RectF(rightDerivate.getM_x() + 1,
                          rightDerivate.getM_Y(),
                          rightDerivate.getM_x(),
                          rightDerivate.getM_Y() + 1); // Must give this rect an area.
    }

    public RectF getLocationInRectangle()
    {
        return  new RectF(this.getM_Location().getM_x() - 1,
                          this.getM_Location().getM_Y() - 1,
                          this.getM_Location().getM_x() + 1,
                          this.getM_Location().getM_Y() + 1);
    }

    //endregion

    //endregion

    //endregion

    //region Constructors

    public BallBase(World world)
    {

        super();

        //this.setM_Radius(DEFAULT_RADIUS);
        this.initializeMe(world);
    }

    public BallBase(World world, float radius)
    {

        super();

        this.setM_Radius(radius);

        this.initializeMe(world);

    }

    //endregion

    //region Methods

    //region Public

    public void setMotionSecondExternal(Vector2 motionSecond)
    {
        this.mMotionLengthBackup = motionSecond.getLength();
        this.motionSecond = motionSecond;
    }

    public abstract void drawMeAt(DrawInformation drawInformation, Vector2 location);

    public void zeroAcceleratedValue()
    {
        //Set property is faster than new instance.
        this.m_AccelerationComponent.setM_x(0);
        this.m_AccelerationComponent.setM_Y(0);
        //Remove component of gravity generated during progress in play.
        this.getMotionSecond().clampToLength(this.mMotionLengthBackup);
    }

    // Se if radius plus radius is more that Euclidean distance.
    public boolean collisionTestOtherBall(BallBase ballBase)
    {

        float radiusSum = this.getM_Radius() + ballBase.getM_Radius();

        float dX = this.getM_Location().getM_x() - ballBase.getM_Location().getM_x();
        float dY = this.getM_Location().getM_Y() - ballBase.getM_Location().getM_Y();

        double dx2 = Math.pow(dX, 2);
        double dy2 = Math.pow(dY, 2);

        //return radiusSum >= Math.sqrt(dx2 + dy2);
        return Math.pow(radiusSum,2) >= dx2 + dy2;

    }

    public boolean haveCreationRespite()
    {
        return this.getmCollisionRespiteTimeLeft() > 0;
    }

    //region GameObject

    @Override
    public abstract void drawMe(DrawInformation drawInformation);

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        super.updateMe(updateInformation);

        if(updateInformation.getmAtFps() > 0)
        {

            //region Borne respite collisions

            this.setmCollisionRespiteTimeLeft((int)(this.getmCollisionRespiteTimeLeft() - (1000 * updateInformation.mOneDividedByFPS)));

            //endregion

            // region Make motion

            this.getM_Location().addVector(this.getMotionSecond().addAndNewVector(this.m_AccelerationComponent).scale(updateInformation.mOneDividedByFPS));

            //endregion

            //region Bounces in screen

            //region Bounced to the right edge

            if (this.getRightDerivative().getM_x() >= this.getM_World().getM_Size().getWidth())
            {
                this.getMotionSecond().makeXNegative();

                if(VIBRATES_ON_HIT_SCREEN_BOUNDARIES)
                {
                    this.vibrateOnScreenCollision();
                }
            }

            //endregion

            //region Bounced to the left edge.

            if(this.getLeftDerivative().getM_x() <= 0)
            {
                this.getMotionSecond().makeXPositive();

                if(VIBRATES_ON_HIT_SCREEN_BOUNDARIES)
                {
                    this.vibrateOnScreenCollision();
                }
            }

            //endregion

            //region Struck bottom

            if(this.getBottomDerivative().getM_Y() >= this.getM_World().getM_Size().getHeight())
            {
               this.setM_Dead(true);
            }

            //endregion

            //region Bounced top edge

            if(this.getTopDerivative().getM_Y() <= 0)
            {
                this.getMotionSecond().makeYPositive();
                this.zeroAcceleratedValue();

                if(VIBRATES_ON_HIT_SCREEN_BOUNDARIES)
                {
                    this.vibrateOnScreenCollision();
                }
            }

            //endregion

            //endregion

            //region Apply gravity

            this.m_AccelerationComponent.setM_Y(this.m_AccelerationComponent.getM_Y() + (GRAVITY_IN_PX * updateInformation.mOneDividedByFPS));

            //endregion

        }

    }

    @Override
    public int getMaxAreaUnitsASecond()
    {
        int maxMotionOneDirection =
                MathUtility.roundUP(Math.abs(Math.max(this.getMotionSecond().getM_x(),
                                                      this.getMotionSecond().getM_Y())));
        return maxMotionOneDirection;
    }

    //endregion

    //endregion

    //region Abstract

    public abstract float getStartingSpeed();

    //endregion

    //region private

    private void initializeMe(World world)
    {
        this.setMotionSecond(new Vector2(0, 0));
        this.setM_Location(new Vector2(0, 0));
        this.setM_World(world);
        this.setM_Dead(false);
        this.m_AccelerationComponent = new Vector2(0,0);
        this.setmCollisionRespiteTimeLeft(this.COLLISION_CREATION_RESPITE_MILLISECONDS);
        this.setmCanInteractWithBricks(true);
        this.mStruckSomething = false;
    }

    private void vibrateOnScreenCollision()
    {
        DeviceFunctions.getInstance().cancelVibrations();
        DeviceFunctions.getInstance().vibrate(VIBRATE_PATTERN_ON_HIT_SCREEN_BOUNDARIES, VIBRATIONS_REPEAT_ON_HIT_SCREEN_BOUNDARIES);
    }

    //endregion

    //endregion

}
