package com.example.nismrg.breakoutsample.Math;

import java.io.Serializable;

import Utility.IDeepClone;

/**
 * Created by nismrg on 2016-05-21.
 */
public class Vector2 implements Serializable,
                                IVector<Vector2>,
                                IDeepClone<Vector2>
{

    //region Variables

    //region Private

    private float m_x;
    private float m_Y;

    //endregion

    //endregion

    //region Encapsulation

    public float getM_x()
    {
        return m_x;
    }

    public void setM_x(float m_x)
    {
        this.m_x = m_x;
    }

    public float getM_Y()
    {
        return m_Y;
    }

    public void setM_Y(float m_Y)
    {
        this.m_Y = m_Y;
    }

    //endregion

    //region Constructors

    public Vector2(float x, float y)
    {
        this.setM_x(x);
        this.setM_Y(y);
    }

    //endregion

    // region Methods

    // region Public

    public  void makeNorthGoing()
    {
        float length = this.getLength();

        this.setM_x(0);
        this.setM_Y(length);
        this.makeYNegative();
    }

    public Vector2 nonMatrixTranslate(float x, float y)
    {
        return new Vector2(this.getM_x() + x,
                           this.getM_Y() + y);
    }

    public void addVector(Vector2 v)
    {
        this.setM_x(this.getM_x() + v.getM_x());
        this.setM_Y(this.getM_Y() + v.getM_Y());
    }

    public Vector2 addAndNewVector(Vector2 v)
    {
        Vector2 newVector = new Vector2(this.getM_x() + v.getM_x(), this.getM_Y() + v.getM_Y());

        return newVector;
    }

    public void mirrorX()
    {
        if(this.getM_x() < 0)
        {
            this.setM_x(Math.abs(this.getM_x()));
        }
        else
        {
            this.setM_x(-this.getM_x());
        }
    }

    public void mirrorY()
    {
        if(this.getM_Y() < 0)
        {
            this.setM_Y(Math.abs(this.getM_Y()));
        }
        else
        {
            this.setM_Y(-this.getM_Y());
        }
    }

    public void makeXNegative()
    {
        this.setM_x(-Math.abs(this.getM_x()));
    }
    public void makeXPositive()
    {
        this.setM_x(Math.abs(this.getM_x()));
    }

    public void makeYNegative()
    {
        this.setM_Y(-Math.abs(this.getM_Y()));
    }

    public void makeYPositive()
    {
        this.setM_Y(Math.abs(this.getM_Y()));
    }

    @Override
    public String toString() {
        String value = String.format("x: %1.2f  y: %2.2f  Length: %3.2f ",
                this.getM_x(),
                this.getM_Y(),
                this.getLength());
        return value;

    }

    //region IVector

    // Pytagaros
    public float getLength() {
        double x2 = Math.pow(this.getM_x(), 2);
        double y2 = Math.pow(this.getM_Y(), 2);
        double length = Math.sqrt(x2 + y2);

        return (float)length;
    }

    public void scaleToLength(float length)
    {
        if(length == 0)
        {
            this.setM_x(0);
            this.setM_Y(0);

            return;
        }

        if(this.getLength() == 0)
        {
            this.setM_x(0);
            this.setM_Y(0);

            return;
        }

        float factor = length / this.getLength();

        Vector2 scaledVector = this.scale(factor);

        this.setM_x(scaledVector.getM_x());
        this.setM_Y(scaledVector.getM_Y());
    }

    public Vector2 scale(float scale)
    {
        return new Vector2(this.getM_x() * scale,
                           this.getM_Y() * scale);
    }

    //region Rotate

    public void rotateAmongZAxis(double angle)
    {

        double radians = Math.toRadians(angle);

        double cos = Math.cos(radians);
        double sin = Math.sin(radians);

        double px = this.getM_x() * cos - this.getM_Y() * sin;
        double py = this.getM_x() * sin + this.getM_Y() * cos;

        this.setM_x((float)px);
        this.setM_Y((float)py);
    }

    public void rotateAmongZAxis(float angle)
    {
        this.rotateAmongZAxis((double)angle);
    }

    @Override
    public Vector2 rotateAmongZAxisNewVector(float angle)
    {
        return this.rotateAmongZAxisNewVector((double)angle);
    }

    @Override
    public Vector2 rotateAmongZAxisNewVector(double angle)
    {
        double radians = Math.toRadians(angle);

        double cos = Math.cos(radians);
        double sin = Math.sin(radians);

        double px = this.getM_x() * cos - this.getM_Y() * sin;
        double py = this.getM_x() * sin + this.getM_Y() * cos;

        return new Vector2((float)px, (float)py);
    }

    public Vector2 clampToLength(float length)
    {

        if(length == 0)
        {
            return new Vector2(0f,0f);
        }

        if(this.getLength() == 0)
        {
            return new Vector2(0f, 0f);
        }

        float factor = length / this.getLength();
        return this.scale(factor);
    }

    @Override
    public void normalize()
    {
        this.scaleToLength(1f);
    }

    //endregion

    //endregion

    //region IDeepClone<Vector2>

    @Override
    public Vector2 deepClone() {
        return new Vector2(this.getM_x(), this.getM_Y());
    }

    //endregion

    //endregion

    //endregion

}
