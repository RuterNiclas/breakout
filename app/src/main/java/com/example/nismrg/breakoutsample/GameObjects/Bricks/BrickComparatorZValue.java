package com.example.nismrg.breakoutsample.GameObjects.Bricks;

import java.util.Comparator;

/**
 * Created by nismrg on 2016-10-24.
 */
public class BrickComparatorZValue implements Comparator<BrickBase>
{
    @Override
    public int compare(BrickBase b1, BrickBase b2)
    {
        if(b1.getM_ZTag() == b2.getM_ZTag())
        {
            return  0;
        }
        else
        {
            if(b1.getM_ZTag() > b2.getM_ZTag())
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
