package com.example.nismrg.breakoutsample.GameObjects.Balls.ExplodeBall;

import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.ParticleBase;

import java.util.ArrayList;

/**
 * Created by nismrg on 2016-11-02.
 */
public class NewBallsAndExplosions
{

    //region Variables

    //region Public

    public ArrayList<BallBase> mBallBases;
    public ArrayList<ParticleBase> mParticles;

    //endregion

    //endregion

    //region Constructors

    public NewBallsAndExplosions(ArrayList<BallBase> ballBases, ArrayList<ParticleBase> particles)
    {
        this.mBallBases = ballBases;
        this.mParticles = particles;
    }

    //endregion

}
