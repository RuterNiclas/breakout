package com.example.nismrg.breakoutsample.ResourceManaging;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Size;

import com.example.nismrg.breakoutsample.Math.UV_Instructions.UV;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.R;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by nismrg on 2016-07-16.
 */
public class ResourceHolderImages
{

    //region Instance

    //region Constants

    //region Private

    final int COLOR_PICK_UP_SKIP_RATE = 20;

    //endregion

    //endregion

    //region Variables

    //region Private

    //region Images

    //region Crate
    private BitMapAndName mCrate;

    private BitMapAndName mCrateCircle;

    //endregion

    //region Steel1
    private BitMapAndName mSteel1;
    private Size mSteel1OriginalSize;

    private BitMapAndName mSteel1Dmg1Top;
    private BitMapAndName mSteel1Dmg1Bottom;
    private BitMapAndName mSteel1Dmg1Right;
    private BitMapAndName mSteel1Dmg1Left;

    private BitMapAndName mSteel1Dmg2Top;
    private BitMapAndName mSteel1Dmg2Bottom;
    private BitMapAndName mSteel1Dmg2Right;
    private BitMapAndName mSteel1Dmg2Left;

    //endregion

    //region Steel2
    private BitMapAndName mSteel2;
    private Size mSteel2OriginalSize;
    private BitMapAndName mSteel2Dmg1Top;
    private BitMapAndName mSteel2Dmg1Bottom;
    private BitMapAndName mSteel2Dmg1Right;
    private BitMapAndName mSteel2Dmg1Left;

    private BitMapAndName mSteel2Dmg2Top;
    private BitMapAndName mSteel2Dmg2Bottom;
    private BitMapAndName mSteel2Dmg2Right;
    private BitMapAndName mSteel2Dmg2Left;

    //endregion

    //region Fragments steel1

    private BitMapAndName mSteel1Fragment1;
    private BitMapAndName mSteel1Fragment2;
    private BitMapAndName mSteel1Fragment3;

    //endregion

    //region Fragments steel2

    private BitMapAndName mSteel2Fragment1;
    private BitMapAndName mSteel2Fragment2;
    private BitMapAndName mSteel2Fragment3;

    //endregion

    //region Indestructible

    private BitMapAndName mIndestructible;

    //endregion

    //region Color densityToImage

    private ArrayList<Integer> mCrateImagePixels;
    private ArrayList<Integer> mSteel1ImagePixels;
    private ArrayList<Integer> mSteel2ImagePixels;

    //endregion

    //region Controls

    private BitMapAndName mPauseImage;
    private BitMapAndName mPlayImage;
    private BitMapAndName mExplodeImage;

    //endregion

    //region Paddle

    private BitMapAndName mPaddleWithTop;
    private int mPaddleCrossHairColor;

    //endregion

    //region Effects

    private GIFAndMetaData mExplosion;
    private ArrayList<Integer> mFirePixels;
    private BitMapAndName mFire;

    //endregion

    //region Ball things

    private BitMapAndName mBomb;
    private BitMapAndName mDefaultBall;
    private int mFastBallColor;
    private int mFastBallInterpolateToColor;
    private BitMapAndName mComputer;
    private BitMapAndName mCrossHairImage;

    //endregion

    //region World

    private BitMapAndName mSpace1;
    private BitMapAndName mSpace2;
    private BitMapAndName mSpace3;
    private BitMapAndName mSpace4;

    //endregion

    //endregion

    private int m_RunningUniqueID = Integer.MAX_VALUE;
    private Hashtable<String, BitMapAndName> mRezisedImageCache;

    private Context myContext = null;

    //endregion

    //endregion

    //region Encapsulation

    public BitMapAndName getmCrate() {
        return mCrate;
    }

    public void setmCrate(BitMapAndName mCrate) {
        this.mCrate = mCrate;
    }

    public BitMapAndName getmSteel1() {
        return mSteel1;
    }

    public void setmSteel1(BitMapAndName mSteel1) {
        this.mSteel1 = mSteel1;
    }

    public BitMapAndName getmSteel1Dmg1Top() {
        return mSteel1Dmg1Top;
    }

    public void setmSteel1Dmg1Top(BitMapAndName mSteel1Dmg1Top) {
        this.mSteel1Dmg1Top = mSteel1Dmg1Top;
    }

    public BitMapAndName getmSteel1Dmg1Bottom() {
        return mSteel1Dmg1Bottom;
    }

    public void setmSteel1Dmg1Bottom(BitMapAndName mSteel1Dmg1Bottom) {
        this.mSteel1Dmg1Bottom = mSteel1Dmg1Bottom;
    }

    public BitMapAndName getmSteel1Dmg1Right() {
        return mSteel1Dmg1Right;
    }

    public void setmSteel1Dmg1Right(BitMapAndName mSteel1Dmg1Right) {
        this.mSteel1Dmg1Right = mSteel1Dmg1Right;
    }

    public BitMapAndName getmSteel1Dmg1Left() {
        return mSteel1Dmg1Left;
    }

    public void setmSteel1Dmg1Left(BitMapAndName mSteel1Dmg1Left) {
        this.mSteel1Dmg1Left = mSteel1Dmg1Left;
    }

    public BitMapAndName getmSteel1Dmg2Top() {
        return mSteel1Dmg2Top;
    }

    public void setmSteel1Dmg2Top(BitMapAndName mSteel1Dmg2Top) {
        this.mSteel1Dmg2Top = mSteel1Dmg2Top;
    }

    public BitMapAndName getmSteel1Dmg2Bottom() {
        return mSteel1Dmg2Bottom;
    }

    public void setmSteel1Dmg2Bottom(BitMapAndName mSteel1Dmg2Bottom) {
        this.mSteel1Dmg2Bottom = mSteel1Dmg2Bottom;
    }

    public BitMapAndName getmSteel1Dmg2Right() {
        return mSteel1Dmg2Right;
    }

    public void setmSteel1Dmg2Right(BitMapAndName mSteel1Dmg2Right) {
        this.mSteel1Dmg2Right = mSteel1Dmg2Right;
    }

    public BitMapAndName getmSteel1Dmg2Left() {
        return mSteel1Dmg2Left;
    }

    public void setmSteel1Dmg2Left(BitMapAndName mSteel1Dmg2Left) {
        this.mSteel1Dmg2Left = mSteel1Dmg2Left;
    }

    public BitMapAndName getmSteel2() {
        return mSteel2;
    }

    public void setmSteel2(BitMapAndName mSteel2) {
        this.mSteel2 = mSteel2;
    }

    public BitMapAndName getmSteel2Dmg1Top() {
        return mSteel2Dmg1Top;
    }

    public void setmSteel2Dmg1Top(BitMapAndName mSteel2Dmg1Top) {
        this.mSteel2Dmg1Top = mSteel2Dmg1Top;
    }

    public BitMapAndName getmSteel2Dmg1Bottom() {
        return mSteel2Dmg1Bottom;
    }

    public void setmSteel2Dmg1Bottom(BitMapAndName mSteel2Dmg1Bottom) {
        this.mSteel2Dmg1Bottom = mSteel2Dmg1Bottom;
    }

    public BitMapAndName getmSteel2Dmg1Right() {
        return mSteel2Dmg1Right;
    }

    public void setmSteel2Dmg1Right(BitMapAndName mSteel2Dmg1Right) {
        this.mSteel2Dmg1Right = mSteel2Dmg1Right;
    }

    public BitMapAndName getmSteel2Dmg1Left() {
        return mSteel2Dmg1Left;
    }

    public void setmSteel2Dmg1Left(BitMapAndName mSteel2Dmg1Left) {
        this.mSteel2Dmg1Left = mSteel2Dmg1Left;
    }

    public BitMapAndName getmSteel2Dmg2Top() {
        return mSteel2Dmg2Top;
    }

    public void setmSteel2Dmg2Top(BitMapAndName mSteel2Dmg2Top) {
        this.mSteel2Dmg2Top = mSteel2Dmg2Top;
    }

    public BitMapAndName getmSteel2Dmg2Bottom() {
        return mSteel2Dmg2Bottom;
    }

    public void setmSteel2Dmg2Bottom(BitMapAndName mSteel2Dmg2Bottom) {
        this.mSteel2Dmg2Bottom = mSteel2Dmg2Bottom;
    }

    public BitMapAndName getmSteel2Dmg2Right() {
        return mSteel2Dmg2Right;
    }

    public void setmSteel2Dmg2Right(BitMapAndName mSteel2Dmg2Right) {
        this.mSteel2Dmg2Right = mSteel2Dmg2Right;
    }

    public BitMapAndName getmSteel2Dmg2Left() {
        return mSteel2Dmg2Left;
    }

    public void setmSteel2Dmg2Left(BitMapAndName mSteel2Dmg2Left) {
        this.mSteel2Dmg2Left = mSteel2Dmg2Left;
    }

    public BitMapAndName getmSteel1Fragment1() {
        return mSteel1Fragment1;
    }

    public void setmSteel1Fragment1(BitMapAndName mSteel1Fragment1) {
        this.mSteel1Fragment1 = mSteel1Fragment1;
    }

    public BitMapAndName getmSteel1Fragment2() {
        return mSteel1Fragment2;
    }

    public void setmSteel1Fragment2(BitMapAndName mSteel1Fragment2) {
        this.mSteel1Fragment2 = mSteel1Fragment2;
    }

    public BitMapAndName getmSteel1Fragment3() {
        return mSteel1Fragment3;
    }

    public void setmSteel1Fragment3(BitMapAndName mSteel1Fragment3) {
        this.mSteel1Fragment3 = mSteel1Fragment3;
    }

    public BitMapAndName getmSteel2Fragment1() {
        return mSteel2Fragment1;
    }

    public void setmSteel2Fragment1(BitMapAndName mSteel2Fragment1) {
        this.mSteel2Fragment1 = mSteel2Fragment1;
    }

    public BitMapAndName getmSteel2Fragment2() {
        return mSteel2Fragment2;
    }

    public void setmSteel2Fragment2(BitMapAndName mSteel2Fragment2) {
        this.mSteel2Fragment2 = mSteel2Fragment2;
    }

    public BitMapAndName getmSteel2Fragment3() {
        return mSteel2Fragment3;
    }

    public void setmSteel2Fragment3(BitMapAndName mSteel2Fragment3) {
        this.mSteel2Fragment3 = mSteel2Fragment3;
    }

    //region Special values

    public Size getmSteel1OriginalSize() {
        return mSteel1OriginalSize;
    }

    public void setmSteel1OriginalSize(Size mSteel1OriginalSize) {
        this.mSteel1OriginalSize = mSteel1OriginalSize;
    }

    public Size getmSteel2OriginalSize() {
        return mSteel2OriginalSize;
    }

    public void setmSteel2OriginalSize(Size mSteel2OriginalSize) {
        this.mSteel2OriginalSize = mSteel2OriginalSize;
    }

    public BitMapAndName getmPauseImage() {
        return mPauseImage;
    }

    public void setmPauseImage(BitMapAndName mPauseImage) {
        this.mPauseImage = mPauseImage;
    }

    public BitMapAndName getmPlayImage() {
        return mPlayImage;
    }

    public void setmPlayImage(BitMapAndName mPlayImage) {
        this.mPlayImage = mPlayImage;
    }

    public BitMapAndName getmExplodeImage() {
        return mExplodeImage;
    }

    public void setmExplodeImage(BitMapAndName mExplodeImage) {
        this.mExplodeImage = mExplodeImage;
    }

    //endregion

    //region Density values

    public ArrayList<Integer> getmCrateImagePixels() {
        return mCrateImagePixels;
    }

    public void setmCrateImagePixels(ArrayList<Integer> mCrateImagePixels) {
        this.mCrateImagePixels = mCrateImagePixels;
    }

    public ArrayList<Integer> getmSteel1ImagePixels() {
        return mSteel1ImagePixels;
    }

    public void setmSteel1ImagePixels(ArrayList<Integer> mSteel1ImagePixels) {
        this.mSteel1ImagePixels = mSteel1ImagePixels;
    }

    public ArrayList<Integer> getmSteel2ImagePixels() {
        return mSteel2ImagePixels;
    }

    public void setmSteel2ImagePixels(ArrayList<Integer> mSteel2ImagePixels) {
        this.mSteel2ImagePixels = mSteel2ImagePixels;
    }

    public ArrayList<Integer> getmFirePixels() {
        return mFirePixels;
    }

    public void setmFirePixels(ArrayList<Integer> mFirePixels) {
        this.mFirePixels = mFirePixels;
    }

    //endregion

    private static void setOurInstance(ResourceHolderImages ourInstance) {
        ResourceHolderImages.ourInstance = ourInstance;
    }

    public BitMapAndName getmCrateCircle() {
        return mCrateCircle;
    }

    public void setmCrateCircle(BitMapAndName mCrateCircle) {
        this.mCrateCircle = mCrateCircle;
    }

    public BitMapAndName getmIndestructible() {
        return mIndestructible;
    }

    public void setmIndestructible(BitMapAndName mIndestructible) {
        this.mIndestructible = mIndestructible;
    }

    public BitMapAndName getmPaddleWithTop() {
        return mPaddleWithTop;
    }

    public void setmPaddleWithTop(BitMapAndName mPaddleWithTop) {
        this.mPaddleWithTop = mPaddleWithTop;
    }

    public int getmPaddleCrossHairColor() {
        return mPaddleCrossHairColor;
    }

    public void setmPaddleCrossHairColor(int mPaddleCrossHairColor) {
        this.mPaddleCrossHairColor = mPaddleCrossHairColor;
    }

    //region Ball things

    public BitMapAndName getmBomb() {
        return mBomb;
    }

    public void setmBomb(BitMapAndName mBomb) {
        this.mBomb = mBomb;
    }

    public BitMapAndName getmDefaultBall() {
        return mDefaultBall;
    }

    public void setmDefaultBall(BitMapAndName mDefaultBall) {
        this.mDefaultBall = mDefaultBall;
    }

    public int getmFastBallColor() {
        return mFastBallColor;
    }

    public void setmFastBallColor(int mFastBallColor) {
        this.mFastBallColor = mFastBallColor;
    }

    public int getmFastBallInterpolateToColor() {
        return mFastBallInterpolateToColor;
    }

    public void setmFastBallInterpolateToColor(int mFastBallInterpolateToColor) {
        this.mFastBallInterpolateToColor = mFastBallInterpolateToColor;
    }

    public BitMapAndName getmComputer() {
        return mComputer;
    }

    public void setmComputer(BitMapAndName mComputer) {
        this.mComputer = mComputer;
    }

    public BitMapAndName getmCrossHairImage() {
        return mCrossHairImage;
    }

    public void setmCrossHairImage(BitMapAndName mCrossHairImage) {
        this.mCrossHairImage = mCrossHairImage;
    }

    //endregion

    //region Effects

    public GIFAndMetaData getmExplosion()
    {
        return mExplosion;
    }

    public void setmExplosion(GIFAndMetaData mExplosion)
    {
        this.mExplosion = mExplosion;
    }

    public BitMapAndName getmFire() {
        return mFire;
    }

    public void setmFire(BitMapAndName mFire) {
        this.mFire = mFire;
    }

    //endregion

    //region World

    //Delay loaded due big image.
    public BitMapAndName getmSpace1()
    {
        if(mSpace1 == null)
        {
            Resources resources = this.myContext.getResources();
            this.mSpace1 = new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.space1), R.drawable.space1);
        }

        return mSpace1;
    }

    public void setmSpace1(BitMapAndName mSpace1)
    {
        this.mSpace1 = mSpace1;
    }

    //Delay loaded due big image.
    public BitMapAndName getmSpace2()
    {
        if(mSpace2 == null)
        {
            Resources resources = this.myContext.getResources();
            this.mSpace2 = new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.space2), R.drawable.space2);
        }

        return mSpace2;
    }

    public void setmSpace2(BitMapAndName mSpace2)
    {
        this.mSpace2 = mSpace2;
    }

    //Delay loaded due big image.
    public BitMapAndName getmSpace3()
    {
        if(mSpace3 == null)
        {
            Resources resources = this.myContext.getResources();
            this.mSpace3 = new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.space3), R.drawable.space3);
        }

        return mSpace3;
    }

    public void setmSpace3(BitMapAndName mSpace3)
    {
        this.mSpace3 = mSpace3;
    }

    //Delay loaded due big image.
    public BitMapAndName getmSpace4()
    {
        if(mSpace4 == null)
        {
            Resources resources = this.myContext.getResources();
            this.mSpace4 = new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.space4), R.drawable.space4);
        }

        return mSpace4;
    }

    public void setmSpace4(BitMapAndName mSpace4)
    {
        this.mSpace4 = mSpace4;
    }

    //endregion

    //endregion

    //region Constructors

    //region Private

    private ResourceHolderImages()
    {
        this.mRezisedImageCache = new Hashtable<>();
    }

    //endregion

    //endregion

    //region Methods

    //region Public

    public BitMapAndName getResizedBitmap(BitMapAndName bitMapAndName, Size size)
    {
        return this.getResizedBitmap(bitMapAndName,
                                     size.getWidth(),
                                     size.getHeight());
    }

    public BitMapAndName getResizedBitmap(BitMapAndName bitMapAndName,
                                          int newWidth,
                                          int newHeight)
    {

        if(bitMapAndName.ismIsResizedFromOriginalOrAltered())
        {
            Bitmap originalBitmap = bitMapAndName.getmBitmap();
            Bitmap resizedBitmap =
                    Bitmap.createScaledBitmap(originalBitmap,
                            newWidth,
                            newHeight,
                            false);

            return new BitMapAndName(resizedBitmap,
                    bitMapAndName.getmId(),
                    true);

        }
        else
        {
            String hash = BitMapAndName.getHash(bitMapAndName.getmId(), newWidth, newHeight);

            if(this.mRezisedImageCache.containsKey(hash))
            {
                return this.mRezisedImageCache.get(hash);
            }
            else
            {
                Bitmap originalBitmap = bitMapAndName.getmBitmap();
                Bitmap resizedBitmap =
                        Bitmap.createScaledBitmap(originalBitmap,
                                newWidth,
                                newHeight,
                                false);

                BitMapAndName newBitMapAndName =
                    new BitMapAndName(resizedBitmap,
                                      bitMapAndName.getmId(),
                                      true);
                this.mRezisedImageCache.put(hash, newBitMapAndName);

                return newBitMapAndName;
            }
        }
    }

    public Bitmap rotateBitMap(Bitmap bitmap, float degrees)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);

        Bitmap result =
            Bitmap.createBitmap(bitmap,
                                0,
                                0,
                                bitmap.getWidth(),
                                bitmap.getHeight(),
                                matrix,
                                true);

        return result;

    }

    public BitMapAndName rotateBitMapInvertAndScale(BitMapAndName bitmap,
                                                    float degrees,
                                                    boolean invert,
                                                    Vector2 scale)
    {

        Matrix matrix = new Matrix();

        matrix.postRotate(degrees);

        float scaleX = invert ? scale.getM_Y() : scale.getM_x();
        float scaleY = invert ? scale.getM_x() : scale.getM_Y();

        matrix.postScale(scaleX, scaleY);

        Bitmap result =
                Bitmap.createBitmap(bitmap.getmBitmap(),
                        0,
                        0,
                        bitmap.getmBitmap().getWidth(),
                        bitmap.getmBitmap().getHeight(),
                        matrix,
                        true);

        return new BitMapAndName(result,
                                 bitmap.getmId(),
                                 true);

    }

    public BitMapAndName invertImage(BitMapAndName bitmap, UV uV)
    {
        switch (uV)
        {
            case NONE :
            {
                return bitmap;
            }
            case U :
            {
                Matrix matrix = new Matrix();
                matrix.preScale(-1, 1);
                Bitmap result = Bitmap.createBitmap(bitmap.getmBitmap(),
                                                    0,
                                                    0,
                                                    bitmap.getmBitmap().getWidth(),
                                                    bitmap.getmBitmap().getHeight(),
                                                    matrix,
                                                    false);
                return new BitMapAndName(result,
                                         bitmap.getmId(),
                                         true);
            }
            case V :
            {
                Matrix matrix = new Matrix();
                matrix.preScale(1, -1);
                Bitmap result = Bitmap.createBitmap(bitmap.getmBitmap(),
                                                    0,
                                                    0,
                                                    bitmap.getmBitmap().getWidth(),
                                                    bitmap.getmBitmap().getHeight(),
                                                    matrix,
                                                    false);
                return new BitMapAndName(result,
                                         bitmap.getmId(),
                                         true);
            }
            default :
            {
                return bitmap;
            }
        }
    }

    public void InitializeSingleTon(Context context)
    {

        this.myContext = context;

        Resources resources = context.getResources();

        this.setmCrate(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.cratesmall), R.drawable.cratesmall));

        //region Steel 1

        //undamaged
        this.setmSteel1(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1), R.drawable.metalbox1));

        //dmg1
        this.setmSteel1Dmg1Bottom(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1dmg1bottom), R.drawable.metalbox1dmg1bottom));
        this.setmSteel1Dmg1Left(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1dmg1left), R.drawable.metalbox1dmg1left));
        this.setmSteel1Dmg1Right(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1dmg1right), R.drawable.metalbox1dmg1right));
        this.setmSteel1Dmg1Top(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1dmg1top), R.drawable.metalbox1dmg1top));

        //dmg2
        this.setmSteel1Dmg2Bottom(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1dmg2bottom), R.drawable.metalbox1dmg2bottom));
        this.setmSteel1Dmg2Left(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1dmg2left), R.drawable.metalbox1dmg2left));
        this.setmSteel1Dmg2Right(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1dmg2right), R.drawable.metalbox1dmg2right));
        this.setmSteel1Dmg2Top(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1dmg2top), R.drawable.metalbox1dmg2top));

        //endregion

        //region Steel 2

        //undamaged
        this.setmSteel2(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2),  R.drawable.metalbox2));

        //dmg1
        this.setmSteel2Dmg1Bottom(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2dmg1bottom), R.drawable.metalbox2dmg1bottom));
        this.setmSteel2Dmg1Left(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2dmg1left), R.drawable.metalbox2dmg1left));
        this.setmSteel2Dmg1Right(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2dmg1right), R.drawable.metalbox2dmg1right));
        this.setmSteel2Dmg1Top(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2dmg1top), R.drawable.metalbox2dmg1top));

        //dmg2
        this.setmSteel2Dmg2Bottom(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2dmg2bottom), R.drawable.metalbox2dmg2bottom));
        this.setmSteel2Dmg2Left(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2dmg2left), R.drawable.metalbox2dmg2left));
        this.setmSteel2Dmg2Right(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2dmg2right), R.drawable.metalbox2dmg2right));
        this.setmSteel2Dmg2Top(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2dmg2top), R.drawable.metalbox2dmg2top));

        //endregion

        //region Fragments

        //Steel 1
        this.setmSteel1Fragment1(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1fragment1), R.drawable.metalbox1fragment1));
        this.setmSteel1Fragment2(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1fragment2), R.drawable.metalbox1fragment2));
        this.setmSteel1Fragment3(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox1fragment3), R.drawable.metalbox1fragment3));

        //Steel 2
        this.setmSteel2Fragment1(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2fragment1), R.drawable.metalbox2fragment1));
        this.setmSteel2Fragment2(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2fragment2), R.drawable.metalbox2fragment2));
        this.setmSteel2Fragment3(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.metalbox2fragment3), R.drawable.metalbox2fragment3));

        //endregion

        //region Special values

        this.setmSteel1OriginalSize(this.calculateImageSize(context, R.drawable.metalbox1));
        this.setmSteel2OriginalSize(this.calculateImageSize(context, R.drawable.metalbox2));

        //endregion

        //region Density values

        // region Crate

        this.setmCrateImagePixels(new ArrayList<Integer>());

        int crateWidth = this.getmCrate().getmBitmap().getWidth();
        int crateHeight = this.getmCrate().getmBitmap().getHeight();

        for(int x = 0; x < crateWidth; x+= COLOR_PICK_UP_SKIP_RATE)
        {
            for(int y = 0; y < crateHeight; y+= COLOR_PICK_UP_SKIP_RATE)
            {
                this.getmCrateImagePixels().add(this.getmCrate().getmBitmap().getPixel(x,y));
            }
        }

        //endregion

        //region Steel 1

        this.setmSteel1ImagePixels(new ArrayList<Integer>());

        int steel1Width = this.getmSteel1().getmBitmap().getWidth();
        int steel1Height = this.getmSteel1().getmBitmap().getHeight();

        for(int x = 0; x < steel1Width; x+= COLOR_PICK_UP_SKIP_RATE)
        {
            for(int y = 0; y < steel1Height; y+= COLOR_PICK_UP_SKIP_RATE)
            {
                this.getmSteel1ImagePixels().add(this.getmSteel1().getmBitmap().getPixel(x,y));
            }
        }

        //endregion

        //region Steel 2

        this.setmSteel2ImagePixels(new ArrayList<Integer>());

        int steel2Width = this.getmSteel2().getmBitmap().getWidth();
        int steel2Height = this.getmSteel2().getmBitmap().getHeight();

        for(int x = 0; x < steel2Width; x += COLOR_PICK_UP_SKIP_RATE)
        {
            for(int y = 0; y < steel2Height; y += COLOR_PICK_UP_SKIP_RATE)
            {
                this.getmSteel2ImagePixels().add(this.getmSteel2().getmBitmap().getPixel(x,y));
            }
        }

        //endregion

        //region Fire

        this.setmFire(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.flames), R.drawable.flames));
        this.setmFirePixels(new ArrayList<Integer>());

        int fireWidth = this.getmFire().getmBitmap().getWidth();
        int fireHeight = this.getmFire().getmBitmap().getHeight();

        for(int x = 0; x < fireWidth; x++)
        {
            for(int y = 0; y < fireHeight; y++)
            {
                this.getmFirePixels().add(this.getmFire().getmBitmap().getPixel(x,y));
            }
        }

        //endregion

        //endregion

        //region Controls

        this.setmPauseImage(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.pause), R.drawable.pause));
        this.setmPlayImage(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.play), R.drawable.play));
        this.setmExplodeImage(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.explode), R.drawable.explode));

        //endregion

        this.setmCrateCircle(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.cratecircle), R.drawable.cratecircle));

        this.setmIndestructible(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.indestructible), R.drawable.indestructible));

        // Paddle with top
        this.setmPaddleWithTop(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.paddlewithtop), R.drawable.paddlewithtop));
        this.setmPaddleCrossHairColor(ResourcesCompat.getColor(resources, R.color.paddleCrossHair, null));

        //region BallBase images

        this.setmBomb(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.bomb), R.drawable.bomb));
        this.setmDefaultBall(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.defaultball), R.drawable.defaultball));
        this.setmFastBallColor(ResourcesCompat.getColor(resources, R.color.fastBallColor, null));
        this.setmFastBallInterpolateToColor(ResourcesCompat.getColor(resources, R.color.fastBallInterpolateToColor, null));
        this.setmComputer(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.computerimage), R.drawable.computerimage));
        this.setmCrossHairImage(new BitMapAndName(BitmapFactory.decodeResource(resources, R.drawable.crosshair), R.drawable.crosshair));

        //endregion

        //region Effects

        // If you examine the image it is a sprite chart of 6*6 images.
        this.setmExplosion(new GIFAndMetaData(BitmapFactory.decodeResource(resources, R.drawable.explosion), new Size(9,9)));

        //endregion

    }

    public Bitmap makeGammaBitmapCopy(Bitmap bitmap, float adjustBy)
    {

        // Copy can result in the same object so the system writes to the original.
        //Bitmap copy = bitmap.copy(bitmap.getConfig(), true);

        Bitmap copy = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

        int height = copy.getHeight();
        int width = copy.getWidth();
        int pixel;

        // Iterate over each row (y-axis)
        for (int y = 0; y < height; y++)
        {
            // and each column (x-axis) on that row
            for (int x = 0; x < width; x++)
            {
                pixel = bitmap.getPixel(x, y);

                //pixel = this.lighterColor(pixel, adjustBy);
                pixel = this.manipulateColor(pixel, adjustBy);

                copy.setPixel(x, y, pixel);
            }
        }

        return copy;

    }

    public int feedUniqueImageId()
    {
        int valueToReturn = m_RunningUniqueID;
        m_RunningUniqueID--;

        return valueToReturn;
    }

    //endregion

    //region Private

    private Size calculateImageSize(Context context, int assetId)
    {


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(),
                                     assetId,
                                     options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;

        return new Size(imageWidth, imageHeight);

    }

    private int manipulateColor(int color, float factor)
    {
        int a = Color.alpha(color);
        int r = Math.round(Color.red(color) * factor);
        int g = Math.round(Color.green(color) * factor);
        int b = Math.round(Color.blue(color) * factor);

        int newColor =Color.argb(a,
                Math.min(r,255),
                Math.min(g,255),
                Math.min(b,255));

        return newColor;

    }

    private int lighterColor(int color, float factor)
    {
        int red = (int) ((Color.red(color) * (1 - factor) / 255 + factor) * 255);
        int green = (int) ((Color.green(color) * (1 - factor) / 255 + factor) * 255);
        int blue = (int) ((Color.blue(color) * (1 - factor) / 255 + factor) * 255);
        return Color.argb(Color.alpha(color), red, green, blue);
    }

    //endregion

    //endregion

    //endregion

    //region Static

    //region Variables

    //region Private

    private static ResourceHolderImages ourInstance = new ResourceHolderImages();

    //endregion

    //endregion

    //region Encapsulation

    //region Public

    public static ResourceHolderImages getInstance()

    {


        return getOurInstance();
    }

    //endregion

    //region Private

    private static ResourceHolderImages getOurInstance() {

        return ourInstance;
    }

    //endregion

    //endregion

    //endregion

}
