package com.example.nismrg.breakoutsample.Math;

import java.io.Serializable;

import Utility.IDeepClone;

/**
 * Created by nismrg on 2016-08-03.
 */
public class Vector3 implements Serializable,
                                IVector<Vector3>,
                                IDeepClone<Vector3>
{

    //region Variables

    //region Private

    private float mX;
    private float mY;
    private float mZ;

    //endregion

    //endregion

    //region Encapsulation

    public float getmX() {
        return mX;
    }

    public void setmX(float mX) {
        this.mX = mX;
    }

    public float getmY() {
        return mY;
    }

    public void setmY(float mY) {
        this.mY = mY;
    }

    public float getmZ() {
        return mZ;
    }

    public void setmZ(float mZ) {
        this.mZ = mZ;
    }

    //endregion

    //region Constructors

    public Vector3(float x,
                   float y,
                   float z)
    {
        this.setmX(x);
        this.setmY(y);
        this.setmZ(z);
    }

    //endregion

    //region Methods

    //region Public

    public Vector3NotEncapsulated toVector3NotEncapsulated()
    {
        return  new Vector3NotEncapsulated(this.getmX(),
                                           this.getmY(),
                                           this.getmZ());
    }

    //region Deep clone

    @Override
    public Vector3 deepClone()
    {
        return new Vector3(this.getmX(),
                           this.getmY(),
                           this.getmZ());
    }

    //endregion

    //region IVector

    public void rotateAmongZAxis(double angle)
    {

        double radians = Math.toRadians(angle);

        double cos = Math.cos(radians);
        double sin = Math.sin(radians);

        double px = this.getmX() * cos - this.getmY() * sin;
        double py = this.getmX() * sin + this.getmY() * cos;

        this.setmX((float)px);
        this.setmY((float)py);
    }

    public void rotateAmongZAxis(float angle)
    {
        this.rotateAmongZAxis((double)angle);
    }

    @Override
    public Vector3 rotateAmongZAxisNewVector(float angle)
    {
        return this.rotateAmongZAxisNewVector((double)angle);
    }

    @Override
    public Vector3 rotateAmongZAxisNewVector(double angle)
    {
        double radians = Math.toRadians(angle);

        double cos = Math.cos(radians);
        double sin = Math.sin(radians);

        double px = this.getmX() * cos - this.getmY() * sin;
        double py = this.getmX() * sin + this.getmY() * cos;

        return new Vector3((float)px,
                           (float)py,
                           this.getmZ());

    }

    public void rotateAmongXAxis(double angle)
    {

        double radians = Math.toRadians(angle);

        double py = this.getmY() * Math.cos(radians) - this.getmZ() * Math.sin(radians);
        double pz = this.getmY() * Math.sin(radians) + this.getmZ() * Math.cos(radians);

        this.setmY((float)py);
        this.setmZ((float)pz);

    }

    public void rotateAmongXAxis(float angle)
    {
        this.rotateAmongXAxis((double)angle);
    }

    @Override
    public Vector3 scale(float scale)
    {
        return new Vector3(this.getmX() * scale,
                           this.getmY() * scale,
                           this.getmZ() * scale);
    }

    @Override
    public Vector3 clampToLength(float length)
    {
        if(length == 0)
        {
            return new Vector3(0f,
                               0f,
                               0f);
        }

        if(this.getLength() == 0)
        {
            return new Vector3(0f,
                               0f,
                               0f);
        }

        float factor = length / this.getLength();
        return this.scale(factor);
    }

    @Override
    public void scaleToLength(float length)
    {
        if(length == 0)
        {
            this.setmX(0);
            this.setmY(0);
            this.setmZ(0);

            return;
        }

        if(this.getLength() == 0)
        {
            this.setmX(0);
            this.setmY(0);
            this.setmZ(0);

            return;
        }

        float factor = length / this.getLength();

        Vector3 scaledVector = this.scale(factor);

        this.setmX(scaledVector.getmX());
        this.setmY(scaledVector.getmY());
        this.setmZ(scaledVector.getmZ());
    }

    // Pytagaros in three dimensions
    public float getLength()
    {
        double x2 = Math.pow(this.getmX(), 2);
        double y2 = Math.pow(this.getmY(), 2);
        double z2 = Math.pow(this.getmZ(), 2);

        double length = Math.sqrt(x2 + y2 + z2);

        return (float)length;
    }

    @Override
    public void addVector(Vector3 v)
    {
        this.setmX(this.getmX() + v.getmX());
        this.setmY(this.getmY() + v.getmY());
        this.setmZ(this.getmZ() + v.getmZ());

    }

    @Override
    public Vector3 addAndNewVector(Vector3 v)
    {
        Vector3 newVector = new Vector3(this.getmX() + v.getmX(),
                                        this.getmY() + v.getmY(),
                                        this.getmZ() + v.getmZ());

        return newVector;
    }

    @Override
    public void normalize()
    {
        this.scaleToLength(1f);
    }

    //endregion

    //endregion

    //endregion

}
