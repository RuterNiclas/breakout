package com.example.nismrg.breakoutsample.GameObjects.Balls.FastBall;

/**
 * Created by nismrg on 2016-11-23.
 */
public class FastBallTrace
{

    //region Variables

    //region Public

    public float mX;
    public float mY;
    public long mAt;

    //endregion

    //endregion

    //region Constructors

    public FastBallTrace(float x, float y, long at)
    {
        this.mAt = at;
        this.mX = x;
        this.mY = y;
    }

    //endregion

}
