package com.example.nismrg.breakoutsample.GameObjects.ParticleEffects;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.GameObject;
import com.example.nismrg.breakoutsample.Math.Vector2;

/**
 * Created by nismrg on 2016-07-26.
 */
public class ParticleBase extends GameObject
{

    //region Variables

    //region Private

    private Vector2 mLocation;
    private Vector2 mMotionSecond;
    private int mMillisecondsToDeath;
    private ParticleSystemBase mOwnedBy;

    //endregion

    //endregion

    //region Encapsulation

    public Vector2 getmLocation() {
        return mLocation;
    }

    public void setmLocation(Vector2 mLocation) {
        this.mLocation = mLocation;
    }

    public Vector2 getmMotionSecond() {
        return mMotionSecond;
    }

    public void setmMotionSecond(Vector2 mMotionSecond) {
        this.mMotionSecond = mMotionSecond;

    }

    public int getmMillisecondsToDeath() {
        return mMillisecondsToDeath;
    }

    public void setmMillisecondsToDeath(int mMillisecondsToDeath) {
        this.mMillisecondsToDeath = mMillisecondsToDeath;
    }

    public ParticleSystemBase getmOwnedBy() {
        return mOwnedBy;
    }

    public void setmOwnedBy(ParticleSystemBase mOwnedBy) {
        this.mOwnedBy = mOwnedBy;
    }

    //endregion

    //region Constructors

    public ParticleBase(Vector2 location,
                        Vector2 motionSecond,
                        int millisecondsToDeath)
    {
        super();

        this.myInitialize(location,
                          motionSecond,
                          millisecondsToDeath);

    }

    /*
    public ParticleBase(Vector2 location,
                        Vector2 motionSecond,
                        int millisecondsToDeath,
                        ParticleSystemBase ownedBy)
    {
        super();

        this.mOwnedBy = ownedBy;

        this.myInitialize(location,
                motionSecond,
                millisecondsToDeath);

    }
    */

    //endregion

    //region Methods

    //region Public

    // If particle has died.
    public boolean isDead()
    {
        return this.getmMillisecondsToDeath() < 0;
    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        super.drawMe(drawInformation);

    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        if(this.isDead())
        {
        }
        else
        {
            if(this.getmOwnedBy() != null)
            {
                this.getmOwnedBy().misDead = false;
            }
            super.updateMe(updateInformation);

            this.getmLocation().addVector(new Vector2(this.getmMotionSecond().getM_x() * updateInformation.mOneDividedByFPS,
                    this.getmMotionSecond().getM_Y() * updateInformation.mOneDividedByFPS));

            this.setmMillisecondsToDeath(this.getmMillisecondsToDeath() - (int) (updateInformation.mOneDividedByFPS * 1000));
        }
    }

    @Override
    public int getM_AtFrameFragmentsFrame() {
        return super.getM_AtFrameFragmentsFrame();
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize(Vector2 location,
                              Vector2 motionSecond,
                              int millisecondsToDeath)
    {
        this.setmLocation(location);
        this.setmMotionSecond(motionSecond);
        this.setmMillisecondsToDeath(millisecondsToDeath);
    }

    //endregion

    //endregion

}
