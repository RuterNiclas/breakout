package com.example.nismrg.breakoutsample.Math;

import java.io.Serializable;

import Utility.IDeepClone;

public class Vector3NotEncapsulated implements Serializable,
                                               IVector<Vector3NotEncapsulated>,
                                               IDeepClone<Vector3NotEncapsulated>
{

    //region Variables

    //region Public

    public float mX;
    public float mY;
    public float mZ;

    //endregion

    //endregion


    //region Constructors

    public Vector3NotEncapsulated(float x,
                   float y,
                   float z)
    {
        this.mX = x;
        this.mY = y;
        this.mZ = z;
    }

    //endregion

    //region Methods

    //region Public

    //region Deep clone

    @Override
    public Vector3NotEncapsulated deepClone()
    {
        return new Vector3NotEncapsulated(this.mX,
                this.mY,
                this.mZ);
    }

    //endregion

    //region IVector

    public void rotateAmongZAxis(double angle)
    {

        double radians = Math.toRadians(angle);

        double cos = Math.cos(radians);
        double sin = Math.sin(radians);

        double px = this.mX * cos - this.mY * sin;
        double py = this.mX * sin + this.mY * cos;

        this.mX = (float)px;
        this.mY = (float)py;
    }

    public void rotateAmongZAxis(float angle)
    {
        this.rotateAmongZAxis((double)angle);
    }

    @Override
    public Vector3NotEncapsulated rotateAmongZAxisNewVector(float angle)
    {
        return this.rotateAmongZAxisNewVector((double)angle);
    }

    @Override
    public Vector3NotEncapsulated rotateAmongZAxisNewVector(double angle)
    {
        double radians = Math.toRadians(angle);

        double cos = Math.cos(radians);
        double sin = Math.sin(radians);

        double px = this.mX * cos - this.mY * sin;
        double py = this.mX * sin + this.mY * cos;

        return new Vector3NotEncapsulated((float)px,
                (float)py,
                this.mZ);

    }

    public void rotateAmongXAxis(double angle)
    {

        double radians = Math.toRadians(angle);

        double py = this.mY * Math.cos(radians) - this.mZ * Math.sin(radians);
        double pz = this.mY * Math.sin(radians) + this.mZ * Math.cos(radians);

        this.mY = (float)py;
        this.mZ = (float)pz;

    }

    public void rotateAmongXAxis(float angle)
    {
        this.rotateAmongXAxis((double)angle);
    }

    @Override
    public Vector3NotEncapsulated scale(float scale)
    {
        return new Vector3NotEncapsulated(this.mX * scale,
                this.mY * scale,
                this.mZ * scale);
    }

    @Override
    public Vector3NotEncapsulated clampToLength(float length)
    {
        if(length == 0)
        {
            return new Vector3NotEncapsulated(0f,
                    0f,
                    0f);
        }

        if(this.getLength() == 0)
        {
            return new Vector3NotEncapsulated(0f,
                    0f,
                    0f);
        }

        float factor = length / this.getLength();
        return this.scale(factor);
    }

    @Override
    public void scaleToLength(float length)
    {
        if(length == 0)
        {
            this.mX = 0;
            this.mY = 0;
            this.mZ = 0;

            return;
        }

        if(this.getLength() == 0)
        {
            this.mX = 0;
            this.mY = 0;
            this.mZ = 0;

            return;
        }

        float factor = length / this.getLength();

        Vector3NotEncapsulated scaledVector = this.scale(factor);

        this.mX = scaledVector.mX;
        this.mY = scaledVector.mY;
        this.mZ = scaledVector.mZ;
    }

    // Pytagaros in three dimensions
    public float getLength()
    {
        double x2 = Math.pow(this.mX, 2);
        double y2 = Math.pow(this.mY, 2);
        double z2 = Math.pow(this.mZ, 2);

        double length = Math.sqrt(x2 + y2 + z2);

        return (float)length;
    }

    @Override
    public void addVector(Vector3NotEncapsulated v)
    {
        this.mX = this.mX + v.mX;
        this.mY = this.mY + v.mY;
        this.mZ = this.mZ + v.mZ;

    }

    @Override
    public Vector3NotEncapsulated addAndNewVector(Vector3NotEncapsulated v)
    {
        Vector3NotEncapsulated newVector = new Vector3NotEncapsulated(this.mX + v.mX,
                this.mY + v.mY,
                this.mZ + v.mZ);

        return newVector;
    }

    @Override
    public void normalize()
    {
        this.scaleToLength(1f);
    }

    //endregion

    //endregion

    //endregion

}
