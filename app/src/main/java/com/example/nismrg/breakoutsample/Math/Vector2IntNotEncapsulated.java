package com.example.nismrg.breakoutsample.Math;

/**
 * Created by nismrg on 2016-11-02.
 */
public class Vector2IntNotEncapsulated
{

    //region Variables

    //region Public

    public  int mX;
    public  int mY;

    //endregion

    //endregion

    //region Constructors

    public  Vector2IntNotEncapsulated(int x, int y)
    {
        this.mX = x;
        this.mY = y;
    }

    //endregion

}
