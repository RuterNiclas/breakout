package com.example.nismrg.breakoutsample.GameObjects.Balls;

import android.graphics.Canvas;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

/**
 * Created by nismrg on 2016-07-10.
 */
public class FollowBall extends BallBase
{

    //region Constants

    //region Private

    private final int LAUNCH_SPEED_UNITS_SECOND = 800;
    private final int DEFAULT_RADIUS = 45;

    //endregion

    //endregion

    //region Variables

    //region Private

    private BitMapAndName mBitmap;

    //endregion

    //endregion

    //region Constructors

    public FollowBall(World world)
    {
        super(world);

        this.setM_Radius(DEFAULT_RADIUS);

        this.myInitialize();

    }

    public FollowBall(World world, float radius)
    {
        super(world, radius);

        this.myInitialize();
    }

    //endregion

    //region Methods

    //region Public

    public float getStartingSpeed()
    {
        return LAUNCH_SPEED_UNITS_SECOND;
    }

    public void drawMeAt(DrawInformation drawInformation, Vector2 location)
    {

        Canvas canvas = drawInformation.getmCanvas();

        canvas.drawBitmap(this.mBitmap.getmBitmap(),
                          location.getM_x() - super.getM_Radius(),
                          location.getM_Y() - super.getM_Radius(),
                          null);

    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation) {

        //super.drawMe(canvas, gameTimeRunMillis);

        this.drawMeAt(drawInformation, this.getM_Location());
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize()
    {
        this.mBitmap = ResourceHolderImages.getInstance().getResizedBitmap(ResourceHolderImages.getInstance().getmComputer(),
                                                                           Math.round(super.getM_Radius() * 2),
                                                                           Math.round(super.getM_Radius() * 2));
    }

    //endregion

    //endregion

}
