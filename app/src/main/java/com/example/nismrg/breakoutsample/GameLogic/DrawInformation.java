package com.example.nismrg.breakoutsample.GameLogic;

import android.graphics.Canvas;

/**
 * Created by nismrg on 2016-07-17.
 */
public class DrawInformation
{

    //region Variable

    //region Private

    private Canvas mCanvas;
    private long mGameTimeRunMillis;
    private long mTotalTimeMillis;
    private boolean mIsPaused;

    //endregion

    //endregion

    //region Encapsulation

    public Canvas getmCanvas() {
        return mCanvas;
    }

    public void setmCanvas(Canvas mCanvas) {
        this.mCanvas = mCanvas;
    }

    public long getmGameTimeRunMillis() {
        return mGameTimeRunMillis;
    }

    public void setmGameTimeRunMillis(long mGameTimeRunMillis) {
        this.mGameTimeRunMillis = mGameTimeRunMillis;
    }

    public long getmTotalTimeMillis() {
        return mTotalTimeMillis;
    }

    public void setmTotalTimeMillis(long mTotalTimeMillis) {
        this.mTotalTimeMillis = mTotalTimeMillis;
    }

    public boolean ismIsPaused() {
        return mIsPaused;
    }

    public void setmIsPaused(boolean mIsPaused) {
        this.mIsPaused = mIsPaused;
    }

    //endregion

    //region Constructors

    public DrawInformation(Canvas canvas,
                           long gameTimeRunMillis,
                           long totalTimeMillis,
                           boolean isPaused)
    {
        this.setmCanvas(canvas);
        this.setmGameTimeRunMillis(gameTimeRunMillis);
        this.setmTotalTimeMillis(totalTimeMillis);
        this.setmIsPaused(isPaused);
    }

    //endregion

}
