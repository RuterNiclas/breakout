package com.example.nismrg.breakoutsample.GameLogic.GameViewState;

import java.io.Serializable;

/**
 * Created by nismrg on 2016-06-09.
 */
public enum InputDirection implements Serializable
{
    LEFT,
    RIGHT,
    DOWN,
    UP
}
