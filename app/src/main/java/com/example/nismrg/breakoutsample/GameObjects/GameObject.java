package com.example.nismrg.breakoutsample.GameObjects;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.IGameLogic;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;

/**
 * Created by nismrg on 2016-05-20.
 */
public class GameObject implements IGameLogic {

    //region Variables

    //region Public

    private int m_CurrentFPS;
    private int m_AtFrameFragmentsFrame;
    private int m_ZTag;

    //endregion

    //endregion

    //region Encapsulation

    //region Public

    public int getM_CurrentFPS()
    {
        return m_CurrentFPS;
    }

    public int getM_AtFrameFragmentsFrame()
    {
        return m_AtFrameFragmentsFrame;
    }

    //endregion

    //region Private

    private void setM_CurrentFPS(int m_CurrentFPS)
    {
        this.m_CurrentFPS = m_CurrentFPS;
    }

    private void setM_AtFrameFragmentsFrame(int m_AtFrameFragmentsFrame)
    {
        this.m_AtFrameFragmentsFrame = m_AtFrameFragmentsFrame;
    }

    public int getM_ZTag()
    {
        return m_ZTag;
    }

    public void setM_ZTag(int m_ZTag)
    {
        this.m_ZTag = m_ZTag;
    }

    //endregion

    //endregion

    //region Constructors

    public GameObject()
    {
        this.myInitialize();
    }

    //endregion

    //region Methods

    //region Public

    //region IGameLogic

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {
        this.setM_CurrentFPS(updateInformation.getmAtFps());
        this.setM_AtFrameFragmentsFrame(updateInformation.getmAtFrameFragmentsFrame());
    }

    @Override
    public int getMaxAreaUnitsASecond()
    {
        return  0;
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize()
    {
        this.setM_ZTag(0);
    }

    //endregion

    //endregion

}
