package com.example.nismrg.breakoutsample.GameObjects.Worlds;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Size;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.Balls.ExplodeBall.ExplodeBall;
import com.example.nismrg.breakoutsample.GameObjects.Balls.ExplodeBall.NewBallsAndExplosions;
import com.example.nismrg.breakoutsample.GameObjects.Balls.FollowBall;
import com.example.nismrg.breakoutsample.GameObjects.Balls.LaserBall;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickBase;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickComparatorZValue;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.IHitEffect;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks.SteelBrickParticleEngine;
import com.example.nismrg.breakoutsample.GameObjects.Controls.ExplodeButton;
import com.example.nismrg.breakoutsample.GameObjects.Controls.GameButtonOnClickListener;
import com.example.nismrg.breakoutsample.GameObjects.Controls.PauseButton;
import com.example.nismrg.breakoutsample.GameObjects.GameObject;
import com.example.nismrg.breakoutsample.GameObjects.HitFrom;
import com.example.nismrg.breakoutsample.GameObjects.Paddle;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.PaintParticleLine;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.PaintParticleLineSystem;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.ParticleBase;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.ParticleSystemBase;
import com.example.nismrg.breakoutsample.GameView;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Utility.AntiConcurrentCollections.AntiConcurrentCollection;
import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-05-17.
 */
public class World extends GameObject
{

    //region Constants

    //region Private

    private final int PADDLE_BOTTOM_START_MARGIN = 0;
    private final int PADDLE_WIDTH = 400;
    private final int PADDLE_HEIGHT = 25;
    // The factor to multiply the reflection angle by at the paddle collision.
    private final float PADDLE_SHARPNESS = 0.75f;

    // Synthetic garbage collection variables
    private final int SKIP_FRAMES = 5;

    //endregion

    //endregion

    //region Variables

    //region Private

    private Size m_Size;

    private AntiConcurrentCollection<BallBase> m_Balls;
    private AntiConcurrentCollection<ParticleSystemBase> m_ParticleSystems;

    private ArrayList<BrickBase> m_Bricks;
    private Paddle m_Paddle;

    private WorldDescriptor mWorldDescriptor;

    private ExplodeButton explodeButton;
    private PauseButton pauseButton;

    private GameView mParentGameView;

    private int m_SkipFrameCountDown;

    private BitMapAndName m_BackGround;

    private RectF mScreenArea;

    //endregion

    //endregion

    //region Encapsulation

    //region Public

    public Size getM_Size()
    {
        return m_Size;
    }

    public void setM_Size(Size m_Size)
    {
        this.m_Size = m_Size;
    }

    public WorldDescriptor getmWorldDescriptor() {
        return mWorldDescriptor;
    }

    public void setmWorldDescriptor(WorldDescriptor mWorldDescriptor) {
        this.mWorldDescriptor = mWorldDescriptor;
    }

    public AntiConcurrentCollection<ParticleSystemBase> getM_ParticleSystems()
    {
        return m_ParticleSystems;
    }

    public void setM_ParticleSystems(AntiConcurrentCollection<ParticleSystemBase> m_ParticleSystems)
    {
        this.m_ParticleSystems = m_ParticleSystems;
    }

    public AntiConcurrentCollection<BallBase> getM_Balls() {
        return m_Balls;
    }

    public void setM_Balls(AntiConcurrentCollection<BallBase> m_Balls) {
        this.m_Balls = m_Balls;
    }

    public BitMapAndName getM_BackGround()
    {
        return m_BackGround;
    }

    public void setM_BackGround(BitMapAndName m_BackGround)
    {
        this.m_BackGround = m_BackGround;
    }

    //endregion

    //endregion

    //region Constructors

    public World(GameView parentGameView, WorldSetupFactory worldSetupFactory) throws Exception
    {

        super();

        this.mParentGameView = parentGameView;


        this.setM_Size(new Size(this.mParentGameView.getM_ScreenSize().getWidth(), this.mParentGameView.getM_ScreenSize().getHeight()));
        this.mScreenArea = new RectF(0,0, this.getM_Size().getWidth(), this.getM_Size().getHeight());

        this.SetUpWorldItems(worldSetupFactory);

        this.m_Paddle = new Paddle(this,
                                   new Vector2(this.getM_Size().getWidth() * 0.5f, // Performance
                                               this.getM_Size().getHeight() - (PADDLE_HEIGHT * 0.5f) - PADDLE_BOTTOM_START_MARGIN), // Performance
                                   new Vector2(PADDLE_WIDTH,
                                               PADDLE_HEIGHT),
                                   PADDLE_SHARPNESS);

        //region Explode button

        int explodeButtonWidthPX = 100;
        int explodeButtonHeightPX = 100;
        int explodeButtonPaddingPX = 3;

        this.explodeButton = new ExplodeButton(true,
                                                   "btnExploder",
                                                   "TestExplosion",
                                                   new Vector2(0 +
        //                                                                             Performance
                                                               (explodeButtonWidthPX * 0.5f) +
                                                               explodeButtonPaddingPX,
        //                                                                                     Performance
                                                               (this.getM_Size().getHeight() * 0.5f) -
                                                               explodeButtonHeightPX),
                                                   new Size(explodeButtonWidthPX,explodeButtonHeightPX));

        this.explodeButton.listener =  new GameButtonOnClickListener(this)
        {
            public void onClick()
            {
                this.getmWorld().explodeBalls();
            }
        };

        //endregion

        //region Pause button

        int pauseButtonWidthPX = 70;
        int pauseButtonHeightPX = 70;
        int pauseButtonPaddingPX = 3;

        this.pauseButton = new PauseButton(true,
                                           "btnPause",
                                           "Pause",
                                           new Vector2(this.getM_Size().getWidth() -
                                                       pauseButtonWidthPX -
                                                       pauseButtonPaddingPX,
        //                                                                             Performance
                                                       (this.getM_Size().getHeight() * 0.5f) -
        //                                                                    Performance
                                                       (pauseButtonHeightPX * 0.5f)),
                                           new Size(pauseButtonWidthPX, pauseButtonHeightPX));
        this.pauseButton.listener = new GameButtonOnClickListener(this)
        {
            public void onClick()
            {

//                if(this.getmWorld().m_Balls.size() > 0) {

                this.getmWorld().togglePause();
                //              }
            }
        };

        //endregion


    }

    //endregion

    //region Methods

    //region Public

    public void togglePause()
    {
        this.pauseButton.vibrate();
        this.mParentGameView.togglePauseInGame();
    }

    public boolean launchBall(BallBase ballBase,
                              Vector2 motion,
                              Vector2 location)
    {
        if(this.canLaunchBall())
        {
            ballBase.setMotionSecondExternal(motion);
            ballBase.setM_Location(location);
            this.getM_Balls().getM_Objects().add(ballBase);

            return true;
        }
        else
        {
            return false;
        }
    }

    public void explodeBalls()
    {

        this.explodeButton.vibrate();

        NewBallsAndExplosions newBallsAndExplosions = null;

        List<BallBase> ballsToAdd = new ArrayList<>();
        List<ParticleBase> particlesToAdd = new ArrayList<>();

        for (BallBase ballBase : this.getM_Balls().getM_Objects())
        {
            if(ballBase.getClass() == ExplodeBall.class)
            {
                newBallsAndExplosions = ((ExplodeBall) ballBase).explode();

                ballsToAdd.addAll(newBallsAndExplosions.mBallBases);
                particlesToAdd.addAll(newBallsAndExplosions.mParticles);
            }
        }
        this.getM_Balls().getM_NewObjects().addAll(ballsToAdd);

        ParticleSystemBase particleSystemBase = new ParticleSystemBase();
        particleSystemBase.mParticles.addAll(particlesToAdd);
        particleSystemBase.assignChildrenWithOwner();

        this.getM_ParticleSystems().getM_NewObjects().add(particleSystemBase);

        this.removeDeadBalls();

    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        super.drawMe(drawInformation);

        Canvas canvas = drawInformation.getmCanvas();
        long gameTimeRunMillis = drawInformation.getmGameTimeRunMillis();
        long totalTimeRunMillis = drawInformation.getmTotalTimeMillis();

        //region Draw background

        if(this.m_BackGround == null)
        {

            canvas.drawColor(Color.BLACK);
        }
        else
        {
            canvas.drawBitmap(this.getM_BackGround().getmBitmap(),0,0, null);
            //canvas.drawBitmap(this.getM_BackGround(), null, this.mScreenArea,null );
        }

        //endregion

        Paint paint = new Paint();
        paint.setColor(Color.GREEN);

        paint.setTextSize(25);
        canvas.drawText("FPS: " + super.getM_CurrentFPS(), 200, 400, paint);
        canvas.drawText("Paused:" + (drawInformation.ismIsPaused() ? "1": "0"), 200, 450, paint);

        //region Draw bricks

        // This can pull some juice of the system. NOTE To slef: See if we can keep this sorted. Sort insert?r4
        Collections.sort(this.m_Bricks, new BrickComparatorZValue());

        for (BrickBase brick : this.m_Bricks)
        {
            brick.drawMe(drawInformation);
        }

        //endregion

        //region Draw paddle

        this.m_Paddle.drawMe(drawInformation);

        //endregion

        //region Draw balls

        for (BallBase ballBase : this.getM_Balls().getM_Objects())
        {
            ballBase.drawMe(drawInformation);
        }

        //endregion

        //region Draw particle systems

        for(ParticleSystemBase particleSystemBase : this.getM_ParticleSystems().getM_Objects())
        {
            particleSystemBase.drawMe(drawInformation);
        }

        //endregion

        //region Draw explode button

        this.explodeButton.drawMe(drawInformation);

        //endregion

        //region Draw pause button

        this.pauseButton.drawMe(drawInformation);

        //endregion

    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        /*
        if(updateInformation.mOneDividedByFPS == Float.POSITIVE_INFINITY )
        {
            return;
        }
        */

        //region Paused and un paused

        this.pauseButton.updateMe(updateInformation);
        //region Update paddle

        this.m_Paddle.setM_ReadyBall(this.canLaunchBall());
        this.m_Paddle.updateMe(updateInformation);

        //endregion

        //endregion

        //region Not paused

        if(!updateInformation.ismIsPaused())
        {

            super.updateMe(updateInformation);

            //region Update explode button

            this.explodeButton.setmVisible(this.canExplode());
            this.explodeButton.updateMe(updateInformation);

            //endregion

            //region Update balls

            for (BallBase ballBase : this.getM_Balls().getM_Objects())
            {

                //region handle following ballBase

                this.handleFollowingBall(ballBase);

                //endregion

                ballBase.updateMe(updateInformation);

                if(ballBase.ismCanInteractWithBricks())
                {

                    //region Check collisions

                    HitFrom hitFrom = null;
                    for (BrickBase brick : this.m_Bricks)
                    {

                        // Don't handle broken bricks.
                        if(brick.ismBroken())
                        {
                            continue;
                        }

                        //region Handle brick collisions

                        hitFrom = brick.locationStrucksMeFrom(updateInformation, ballBase);

                        //region Set struck something on ballBase

                        if(hitFrom != HitFrom.NONE)
                        {
                            ballBase.mStruckSomething = true;
                        }

                        //endregion

                        switch (hitFrom)
                        {
                            case NONE:
                                break;
                            case BELOW:
                                ballBase.getMotionSecond().makeYPositive();
                                ballBase.zeroAcceleratedValue();
                                break;
                            case ABOVE:
                                ballBase.getMotionSecond().makeYNegative();
                                ballBase.zeroAcceleratedValue();
                                break;
                            case LEFT:
                                ballBase.getMotionSecond().makeXNegative();
                                ballBase.zeroAcceleratedValue();
                                break;
                            case RIGHT:
                                ballBase.getMotionSecond().makeXPositive();
                                ballBase.zeroAcceleratedValue();
                                break;
                        }

                        // Can only hit one brick in one update.
                        if (hitFrom != HitFrom.NONE) {

                            //ballBase.onBrickCollision();
                            if (IHitEffect.class.isAssignableFrom(brick.getClass()))
                            {
                                this.pickUpFragmentBallsAndParticleSystems((IHitEffect) brick);
                            }

                            break;
                        }

                        //endregion

                    }

                    //endregion

                }

                if (RectF.intersects(ballBase.getBottomDerivativeRect(), this.m_Paddle.getMyIntersection()))
                {
                    ballBase.mStruckSomething = true;
                    this.handleBallCollisionWithPaddle(ballBase);
                }
            }

            //endregion

            //region Update bricks

            for (BrickBase brick : this.m_Bricks)
            {
                brick.updateMe(updateInformation);
            }

            //endregion

            //region Update particle systems

            for(ParticleSystemBase particleSystemBase : this.getM_ParticleSystems().getM_Objects())
            {
                particleSystemBase.updateMe(updateInformation);
            }

            //endregion

            //region Clean

            this.cleanObjects();

            //endregion

            this.handleBallCollisionsWithOtherBalls();

            // region Concurrency commits

            this.getM_Balls().commit();
            this.getM_ParticleSystems().commit();

            //endregion

        }

        //endregion

    }

    @Override
    public int getMaxAreaUnitsASecond()
    {

        int maxValue = 0;

        //region Balls

        for(BallBase ballBase : this.getM_Balls().getM_Objects())
        {
            int areaUnitsSecondBall = ballBase.getMaxAreaUnitsASecond();
            maxValue = areaUnitsSecondBall;
        }

        //endregion

        //region Bricks

        int tmpAreaUnitSecond;
        for (BrickBase brick:
             this.m_Bricks)
        {
            tmpAreaUnitSecond = brick.getMaxAreaUnitsASecond();

            if(tmpAreaUnitSecond > maxValue)
            {
                maxValue = tmpAreaUnitSecond;
            }
        }

        //endregion

        return  maxValue;

    }

    //endregion

    //endregion

    //region Private

    private void cleanObjects()
    {
        this.m_SkipFrameCountDown--;
        if(this.m_SkipFrameCountDown < 1)
        {
            this.removeBrokenBricks();
            this.removeDeadBalls();
            this.removeExpiredParticles();

            this.m_SkipFrameCountDown = this.SKIP_FRAMES;
        }
    }

    private void handleFollowingBall(BallBase ballBase)
    {
        if(ballBase.getClass() == FollowBall.class)
        {
            int paddleLength = (int)this.m_Paddle.getM_Size().getLength();
            int paddleHalfLength = (int)(paddleLength * 0.5f);
            int randomDeviation = RandomProvider.getInstance().nextInt(paddleHalfLength) - (paddleHalfLength + 1);


            this.m_Paddle.getM_Location().setM_x(ballBase.getM_Location().getM_x() + randomDeviation);
        }
    }

    private void handleBallCollisionsWithOtherBalls()
    {

        BallBase ballBaseOne;
        BallBase ballBaseTwo;

        for(int i = 0; i < this.getM_Balls().getM_Objects().size(); i++)
        {
            for(int ii = i + 1; ii < this.getM_Balls().getM_Objects().size(); ii++)
            {

                ballBaseOne = this.getM_Balls().getM_Objects().get(i);
                ballBaseTwo = this.getM_Balls().getM_Objects().get(ii);

                if(!ballBaseOne.haveCreationRespite() &&
                   !ballBaseTwo.haveCreationRespite() &&
                    ballBaseOne.collisionTestOtherBall(ballBaseTwo))
                {
                    ballBaseOne.mStruckSomething = true;
                    ballBaseTwo.mStruckSomething = true;
                    this.conductBallCollision(ballBaseOne, ballBaseTwo);
                }
            }
        }
    }

    private void conductBallCollision(BallBase ballBaseOne, BallBase ballBaseTwo)
    {

        BallBase leftBallBase;
        BallBase rightBallBase;

        //region Assign balls

        if(ballBaseOne.getM_Location().getM_x() < ballBaseOne.getM_Location().getM_x())
        {
            leftBallBase = ballBaseOne;
            rightBallBase = ballBaseTwo;

            Vector2 vOneToTwo =
                    new Vector2(rightBallBase.getM_Location().getM_x() - leftBallBase.getM_Location().getM_x(),
                            rightBallBase.getM_Location().getM_Y() - leftBallBase.getM_Location().getM_Y());

            vOneToTwo.scaleToLength(1);

            Vector2 vOneToTwoBack90 = vOneToTwo.deepClone();
            vOneToTwoBack90.rotateAmongZAxis(-90f);

            Vector2 leftNew = new Vector2(vOneToTwoBack90.getM_x(), vOneToTwoBack90.getM_Y());
            leftNew.scaleToLength(ballBaseOne.getMotionSecond().getLength());

            Vector2 rightNew = new Vector2(-vOneToTwoBack90.getM_x(), -vOneToTwoBack90.getM_Y());
            rightNew.scaleToLength(ballBaseTwo.getMotionSecond().getLength());

            ballBaseOne.setMotionSecondExternal(leftNew);
            ballBaseTwo.setMotionSecondExternal(rightNew);

        }
        else
        {
            leftBallBase = ballBaseTwo;
            rightBallBase = ballBaseOne;

            Vector2 vOneToTwo =
                    new Vector2(rightBallBase.getM_Location().getM_x() - leftBallBase.getM_Location().getM_x(),
                            rightBallBase.getM_Location().getM_Y() - leftBallBase.getM_Location().getM_Y());

            vOneToTwo.scaleToLength(1);

            Vector2 vOneToTwoBack90 = vOneToTwo.deepClone();
            vOneToTwoBack90.rotateAmongZAxis(-90f);

            Vector2 leftNew = new Vector2(vOneToTwoBack90.getM_x(), vOneToTwoBack90.getM_Y());
            leftNew.scaleToLength(ballBaseTwo.getMotionSecond().getLength());

            Vector2 rightNew = new Vector2(-vOneToTwoBack90.getM_x(), -vOneToTwoBack90.getM_Y());
            rightNew.scaleToLength(ballBaseOne.getMotionSecond().getLength());

            ballBaseTwo.setMotionSecondExternal(leftNew);
            ballBaseOne.setMotionSecondExternal(rightNew);
        }

        //endregion

    }

    private boolean canExplode()
    {
        for(BallBase ballBase : this.getM_Balls().getM_Objects())
        {
            if(ballBase.getClass() == ExplodeBall.class)
            {
                return true;
            }
        }

        return false;

    }

    //region Remove things to remove

    private void removeBrokenBricks()
    {


        ArrayList<BrickBase> bricks = new ArrayList<>();

        for(BrickBase brick : this.m_Bricks)
        {
            if(brick.getM_HitPointsLeft() > 0) {
                bricks.add(brick);
            }
        }
        this.m_Bricks = bricks;

    }

    private void removeDeadBalls()
    {

        /*
        ArrayList<BallBase> balls = new ArrayList<>();

        for(BallBase ball: this.getM_Balls())
        {
            if(!ball.isM_Dead())
            {
                balls.add(ball);
            }
        }

        this.m_Balls = balls;
         */
        for(BallBase ballBase : this.getM_Balls().getM_Objects())
        {
            if(ballBase.isM_Dead())
            {
                this.getM_Balls().getM_ObjectsToDelete().add(ballBase);
            }
        }

        this.m_Paddle.setM_LastLaunchIsLaserBall(false);
        for(BallBase ballBase : this.getM_Balls().getM_Objects())
        {
            if(ballBase.getClass() == LaserBall.class)
            {
                this.m_Paddle.setM_LastLaunchIsLaserBall(true);
                break;
            }
        }

    }

    private void removeExpiredParticles()
    {

        SteelBrickParticleEngine steelBrickParticleEngine = SteelBrickParticleEngine.getInstance();
        ArrayList<PaintParticleLine> feedBackListPaintParticleLine = new ArrayList<>();

        for(ParticleSystemBase particleSystemBase : this.getM_ParticleSystems().getM_Objects())
        {
            if(particleSystemBase.misDead)
            {
                this.getM_ParticleSystems().getM_ObjectsToDelete().add(particleSystemBase);

                if(particleSystemBase.getClass() == PaintParticleLineSystem.class)
                {
                    PaintParticleLineSystem paintParticleLineSystem = (PaintParticleLineSystem)particleSystemBase;

                    for(PaintParticleLine paintParticleLine : paintParticleLineSystem.mParticles)
                    {
                        feedBackListPaintParticleLine.add(paintParticleLine);
                    }
                }
            }
        }

        steelBrickParticleEngine.pushBackParticles(feedBackListPaintParticleLine);

    }

    //endregion

    private boolean canLaunchBall()
    {
        if(this.getM_Balls().getM_Objects().size() == 0)
        {
            return  true;
        }
        else
        {
            return false;
        }
    }

    private void SetUpWorldItems(WorldSetupFactory worldSetupFactory) throws Exception
    {

        this.setM_Balls(new AntiConcurrentCollection<BallBase>());
        this.setM_ParticleSystems(new AntiConcurrentCollection<ParticleSystemBase>());

        //WorldSetupFactory randomSetup = this.getRandomWorldSetup();
        this.setmWorldDescriptor(worldSetupFactory.getWorldDescriptor(this));

        this.m_Bricks = this.getmWorldDescriptor().getmBricks();
        this.m_BackGround = this.getmWorldDescriptor().getmBackground();

    }

    /*
    private WorldSetupFactory getRandomWorldSetup()
    {
        Random random = new Random();
        int randomValue = random.nextInt();
        int value = 1;//randomValue % 3;

        switch(value) {
            case 0: return  WorldSetupFactory.GreenGrass;
            case 1: return  WorldSetupFactory.Factory;
            case 2: return  WorldSetupFactory.Hard;

            default: return  WorldSetupFactory.GreenGrass;
        }
    }
    */

    private void handleBallCollisionWithPaddle(BallBase ballBase)
    {

        this.m_Paddle.vibrate();

        ballBase.getMotionSecond().makeNorthGoing();
        //ballBase.getMotionSecond().makeYNegative();
        float byAngle = this.m_Paddle.getReflectionAngle(ballBase.getM_Location());
        byAngle = byAngle * this.m_Paddle.getM_PaddleSharpness();
        ballBase.zeroAcceleratedValue();
        ballBase.getMotionSecond().rotateAmongZAxis(byAngle);

        ballBase.setmCanInteractWithBricks(true);
    }
    
    private void pickUpFragmentBallsAndParticleSystems(IHitEffect hitEffect)
    {
        this.getM_Balls().getM_NewObjects().addAll(hitEffect.pullAllNewBalls());
        this.getM_ParticleSystems().getM_NewObjects().addAll(hitEffect.pullAllNewParticleSystems());
    }

    //endregion

    //endregion

}
