package com.example.nismrg.breakoutsample.GameObjects.Bricks;

import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.ParticleSystemBase;

import java.util.Collection;

/**
 * Created by nismrg on 2016-08-19.
 */
public interface IHitEffect
{
    Collection<BallBase> pullAllNewBalls();
    Collection<ParticleSystemBase> pullAllNewParticleSystems();
}
