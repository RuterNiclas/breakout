package com.example.nismrg.breakoutsample.GameLogic;

import com.example.nismrg.breakoutsample.GameLogic.GameViewState.GameViewState;

/**
 * Created by nismrg on 2016-07-17.
 */
public class UpdateInformation
{

    //region Variables

    //region Public

    public float mOneDividedByFPS;

    //endregion

    //region Private

    private int mAtFps;
    private int mAtFrameFragmentsFrame;
    private GameViewState mGameViewState;
    private long mGameTimeRunMillis;
    private long mTotalTimeMillis;
    private boolean mIsPaused;

    //endregion

    //endregion

    //region Encapsulation

    public int getmAtFps() {
        return mAtFps;
    }

    public void setmAtFps(int mAtFps) {
        this.mAtFps = mAtFps;
    }

    public int getmAtFrameFragmentsFrame() {
        return mAtFrameFragmentsFrame;
    }

    public void setmAtFrameFragmentsFrame(int mAtFrameFragmentsFrame) {
        this.mAtFrameFragmentsFrame = mAtFrameFragmentsFrame;
    }

    public GameViewState getmGameViewState() {
        return mGameViewState;
    }

    public void setmGameViewState(GameViewState mGameViewState) {
        this.mGameViewState = mGameViewState;
    }

    public long getmGameTimeRunMillis() {
        return mGameTimeRunMillis;
    }

    public void setmGameTimeRunMillis(long mGameTimeRunMillis) {
        this.mGameTimeRunMillis = mGameTimeRunMillis;
    }

    public long getmTotalTimeMillis() {
        return mTotalTimeMillis;
    }

    public void setmTotalTimeMillis(long mTotalTimeMillis) {
        this.mTotalTimeMillis = mTotalTimeMillis;
    }

    public boolean ismIsPaused() {
        return mIsPaused;
    }

    public void setmIsPaused(boolean mIsPaused) {
        this.mIsPaused = mIsPaused;
    }

    //endregion

    //region Constructors

    public UpdateInformation(int atFps,
                             int atFrameFragmentsFrame,
                             GameViewState gameViewState,
                             long gameTimeRunMillis,
                             long totalTimeMillis,
                             boolean isPaused)
    {
        this.setmAtFps(atFps);
        this.mOneDividedByFPS = 1f / this.getmAtFps();
        this.setmAtFrameFragmentsFrame(atFrameFragmentsFrame);
        this.setmGameViewState(gameViewState);
        this.setmGameTimeRunMillis(gameTimeRunMillis);
        this.setmTotalTimeMillis(totalTimeMillis);
        this.setmIsPaused(isPaused);
    }

    //endregion

}
