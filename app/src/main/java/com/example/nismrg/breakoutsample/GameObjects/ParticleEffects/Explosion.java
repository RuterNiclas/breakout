package com.example.nismrg.breakoutsample.GameObjects.ParticleEffects;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Size;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.Math.Vector2IntNotEncapsulated;
import com.example.nismrg.breakoutsample.ResourceManaging.GIFAndMetaData;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

import java.util.ArrayList;

import Utility.MiscConstants;
import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-11-02.
 */
public class Explosion extends ParticleBase
{

    //region Constants

    //region Private

    private final int NUMBER_OF_SHRAPNEL = 200;
    private final int SHRAPNEL_AVERAGE_SPEED_IN_PIXEL_A_SECOND = 800;
    private final int SHRAPNEL_STANDARD_DEVIATION_SPEED_IN_PIXEL_A_SECOND = 200;
    private final int SHRAPNEL_AVERAGE_LIFE_TIME_IN_MILLISECONDS = 4000;
    private final int SHRAPNEL_STANDARD_DEVIATION_LIFE_TIME_IN_MILLISECONDS = 200;
    private final int SHRAPNEL_AVERAGE_LENGTH_IN_PIXELS_POSITIVE_ONLY = 15;
    private final int SHRAPNEL_STANDARD_DEVIATION_LENGTH_IN_PIXELS = 5;
    private final int SHRAPNEL_PARTICLE_MAX_WIDTH = 3;

    //endregion

    //endregion

    //region Variables

    //region Private

    private int mOriginalMilliSeconds;
    private GIFAndMetaData mGif;
    private Size mMySize;
    private float mExplosionSizeToOwner;
    private ArrayList<PaintParticleLine2DSimple> mShrapnel;
    private Paint mMyPaint;

    //endregion

    //endregion

    //region Constructors

    public Explosion(Vector2 location,
                     Vector2 motionSecond,
                     int millisecondsToDeath,
                     Size size,
                     float explosionSizeToOwner)
    {
        super(location,
              motionSecond,
              millisecondsToDeath);

        this.myInitialize(millisecondsToDeath,
                          size,
                          explosionSizeToOwner);
    }

    //endregion

    //region Methods

    //region Public

    //region IGameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        super.drawMe(drawInformation);

        for(PaintParticleLine2DSimple shrapnel : this.mShrapnel)
        {
            shrapnel.drawMe(drawInformation);
        }

        float remainingQuotaToDeath = 1 - ((float)(super.getmMillisecondsToDeath()) / (float)(this.mOriginalMilliSeconds));

        Vector2IntNotEncapsulated at = this.mGif.getChartLocationByPositionInCycle(remainingQuotaToDeath);

        int sourceX = at.mX * mGif.getmElementSize().getWidth();
        int sourceY = at.mY * mGif.getmElementSize().getHeight();
        Rect sourceImage = new Rect(sourceX + 1,
                                    sourceY + 1,
                                    sourceX + mGif.getmElementSize().getWidth() - 1,
                                    sourceY + mGif.getmElementSize().getHeight() - 1);

        int locationX = (int)(super.getmLocation().getM_x());
        int locationY = (int)(super.getmLocation().getM_Y());

        Rect destination = new Rect(locationX - (int)(mMySize.getWidth()* this.mExplosionSizeToOwner * 0.5f),
                                    locationY - (int)(mMySize.getHeight()* this.mExplosionSizeToOwner * 0.5f),
                                    (int)(locationX + mMySize.getWidth() * this.mExplosionSizeToOwner),
                                    (int)(locationY  + mMySize.getHeight() * this.mExplosionSizeToOwner));

        this.mMyPaint.setAlpha((int)(MiscConstants.COLOR_CHANEL_MAX_FLOAT * remainingQuotaToDeath));

        drawInformation.getmCanvas().drawBitmap(this.mGif.getmBitmap(),
                                                sourceImage,
                                                destination,
                                                this.mMyPaint);

    }


    @Override
    public void updateMe(UpdateInformation updateInformation)
    {
        super.updateMe(updateInformation);

        for(ParticleBase particle : this.mShrapnel)
        {
            particle.updateMe(updateInformation);
        }
    }


    //endregion

    public void setLocationCascade(Vector2 location)
    {
        this.setmLocation(location);

        for(PaintParticleLine2DSimple shrapnel : this.mShrapnel)
        {
            shrapnel.setmLocation(super.getmLocation().deepClone());
        }
    }

    public void updateSize(int x, int y)
    {
        this.mMySize = new Size(x,y);
    }

    public void setExplosionSizeToOwner(float explosionSizeToOwner)
    {
        this.mExplosionSizeToOwner = explosionSizeToOwner;
    }

    //endregion

    //region Private

    private void myInitialize(int millisecondsToDeath,
                              Size size,
                              float explosionSizeToOwner)
    {
        this.mMySize = size;
        this.mOriginalMilliSeconds = millisecondsToDeath;
        this.mGif = ResourceHolderImages.getInstance().getmExplosion();
        this.mExplosionSizeToOwner = explosionSizeToOwner;
        this.initializeShrapnel();
        this.mMyPaint = new Paint();
        this.mMyPaint.setColor(Color.TRANSPARENT);
        this.mMyPaint.setAntiAlias(true);
        this.mMyPaint.setStyle(Paint.Style.FILL);
        this.mMyPaint.setStrokeJoin(Paint.Join.MITER);
    }

    private void initializeShrapnel()
    {

        this.mShrapnel = new ArrayList<>();

        RandomProvider randomProvider = RandomProvider.getInstance();
        ArrayList<Integer> colorDistribution = ResourceHolderImages.getInstance().getmFirePixels();

        PaintParticleLine2DSimple shrapnel;
        Vector2 motionSecond;
        float degreesRotated;
        int lengthOfFireImage = colorDistribution.size();

        for(float i = 0; i < this.NUMBER_OF_SHRAPNEL; i++)
        {
            degreesRotated = (i / this.NUMBER_OF_SHRAPNEL) * 360;

            motionSecond = new Vector2(0f, ((float)randomProvider.nextGaussian() * this.SHRAPNEL_STANDARD_DEVIATION_SPEED_IN_PIXEL_A_SECOND) + this.SHRAPNEL_AVERAGE_SPEED_IN_PIXEL_A_SECOND);
            motionSecond.rotateAmongZAxis(degreesRotated);
            int randomIndex = randomProvider.nextInt(lengthOfFireImage);

            shrapnel = new PaintParticleLine2DSimple(this.getmLocation(),
                                                     motionSecond,
                                                     (int)(Math.round(randomProvider.nextGaussian() * this.SHRAPNEL_STANDARD_DEVIATION_LIFE_TIME_IN_MILLISECONDS +
                                                     this.SHRAPNEL_AVERAGE_LIFE_TIME_IN_MILLISECONDS)),
                                                     (int)(Math.round(Math.abs(randomProvider.nextGaussian()) * this.SHRAPNEL_STANDARD_DEVIATION_LENGTH_IN_PIXELS +
                                                     this.SHRAPNEL_AVERAGE_LENGTH_IN_PIXELS_POSITIVE_ONLY)),
                                                     colorDistribution.get(randomIndex),
                                                     randomProvider.nextInt(this.SHRAPNEL_PARTICLE_MAX_WIDTH) + 1);

            this.mShrapnel.add(shrapnel);
        }
    }

    //endregion

    //endregion

}
