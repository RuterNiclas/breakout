package com.example.nismrg.breakoutsample.GameObjects.ParticleEffects;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

/**
 * Created by nismrg on 2016-08-19.
 */
public class CrateNova extends Nova
{

    //region Constants

    //region Private

    private final Integer APLPHA_MAX = 256;

    //end

    //endregion

    //region Variables

    //region Private

    private Bitmap mCrateCircle;
    private float mDiminishAtRadiusInPixels;
    private Paint mPaint;
    private Long mBornAt;

    //endregion

    //endregion

    //region Encapsulation

    public Bitmap getmCrateCircle() {
        return mCrateCircle;
    }

    public void setmCrateCircle(Bitmap mCrateCircle) {
        this.mCrateCircle = mCrateCircle;
    }

    public float getmDiminishAtRadiusInPixels() {
        return mDiminishAtRadiusInPixels;
    }

    public void setmDiminishAtRadiusInPixels(float mDiminishAtRadiusInPixels) {
        this.mDiminishAtRadiusInPixels = mDiminishAtRadiusInPixels;
    }

    public Paint getmPaint() {
        return mPaint;
    }

    public void setmPaint(Paint mPaint) {
        this.mPaint = mPaint;
    }

    public Long getmBornAt() {
        return mBornAt;
    }

    public void setmBornAt(Long mBornAt) {
        this.mBornAt = mBornAt;
    }

    //endregion

    //region Constructors

    public CrateNova(Vector2 location,
                     Vector2 motionSecond,
                     int millisecondsToDeath,
                     float explosionSpeedPixelsSecond,
                     float diminishAtRadiusInPixels)
    {
        super(location,
              motionSecond,
              millisecondsToDeath,
              explosionSpeedPixelsSecond);

        this.myInitialize(diminishAtRadiusInPixels);
    }

    //endregion

    //region Methods

    //region Public

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        super.drawMe(drawInformation);

        // Performance
        float edgeDistance = this.getmExplosionEdgePixels().getM_x();
        float x = super.getmLocation().getM_x();
        float y = super.getmLocation().getM_Y();

        Rect destinationRect = new Rect((int)(x - edgeDistance),
                                        (int)(y - edgeDistance),
                                        (int)(x + edgeDistance),
                                        (int)(y + edgeDistance));

        //this.getmPaint().setAlpha((int)( APLPHA_MAX * this.calculateTransparency() / 4f));

        /*
        drawInformation.getmCanvas().drawBitmap(this.getmCrateCircle(),
                                                null,
                                                destinationRect,
                                                this.getmPaint());
        */

        if(this.getmBornAt() != null) {
            float timeElapsed = (float) (drawInformation.getmGameTimeRunMillis() - this.getmBornAt());

            timeElapsed = timeElapsed / 10f;
            float value = 100 + (float) (Math.pow(timeElapsed, 2) - (10*timeElapsed) +1);

            this.getmPaint().setColor(Color.BLACK);
            this.getmPaint().setStyle(Paint.Style.STROKE);
            this.getmPaint().setStrokeWidth(3);

            if(value > 0) {
                drawInformation.getmCanvas().drawCircle(x, y, value, this.getmPaint());
            }
        }
    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {
        super.updateMe(updateInformation);

        if(this.getmBornAt() == null)
        {
            this.setmBornAt(updateInformation.getmGameTimeRunMillis());
        }
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize(float diminishAtRadiusInPixels)
    {
        this.setmCrateCircle(ResourceHolderImages.getInstance().getmCrateCircle().getmBitmap());
        this.setmDiminishAtRadiusInPixels(diminishAtRadiusInPixels);

        Paint myPaint = new Paint();
        this.setmPaint(myPaint);




    }

    private float calculateTransparency()
    {
        float factor = super.getmExplosionEdgePixels().getM_x() / this.getmDiminishAtRadiusInPixels();
        float factorInverted = 1 - factor;

        // Never return less than 0;
        return Math.max(factorInverted, 0f);

    }

    //endregion

    //endregion

}
