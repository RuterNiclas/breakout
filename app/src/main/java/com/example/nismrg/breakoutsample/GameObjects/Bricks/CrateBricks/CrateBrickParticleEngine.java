package com.example.nismrg.breakoutsample.GameObjects.Bricks.CrateBricks;

import com.example.nismrg.breakoutsample.GameObjects.MiscellaneousLogic.IParticleEngine;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.PaintParticleGravitron;
import com.example.nismrg.breakoutsample.Math.Vector2;

import java.util.ArrayList;
import java.util.Collection;

import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-08-21.
 */
public class CrateBrickParticleEngine implements IParticleEngine<PaintParticleGravitron>
{

    //region Instance

    //region Variables

    //region Private

    private ArrayList<PaintParticleGravitron> m_CacheBank;
    private RandomProvider m_RandomProvider;

    //endregion

    //endregion

    //region Encapsulation

    public ArrayList<PaintParticleGravitron> getM_CacheBank() {
        return m_CacheBank;
    }

    public void setM_CacheBank(ArrayList<PaintParticleGravitron> m_CacheBank) {
        this.m_CacheBank = m_CacheBank;
    }

    //endregion

    //region Constructors

    private CrateBrickParticleEngine()
    {
        this.m_RandomProvider = RandomProvider.getInstance();
    }

    //endregion

    //region Methods

    //region Public

    // Start the singleton build a cache of objects.
    public void InitializeSingleTon()
    {

        this.m_CacheBank = new ArrayList<>();

        this.replenishInstances(this.STARTING_SIZE);
    }

    public ArrayList<PaintParticleGravitron> pullParticles(int numberOfObjects)
    {

        ArrayList<PaintParticleGravitron> result = new ArrayList<>();

        if(this.m_CacheBank.size() < numberOfObjects)
        {
            int missingValues = numberOfObjects - this.m_CacheBank.size();

            for(int i = 0; i < missingValues; i+= this.ON_EMPTY_REFILL_NUMBER)
            {
                this.replenishInstances(this.ON_EMPTY_REFILL_NUMBER);
            }
        }

        for(int i = 0; i < numberOfObjects; i++)
        {
            result.add(this.m_CacheBank.get(0));
            this.m_CacheBank.remove(0);
        }

        return result;

    }

    public PaintParticleGravitron pullSingleParticle()
    {

        PaintParticleGravitron result = null;

        if(this.m_CacheBank.size() < 1)
        {
            this.replenishInstances(this.ON_EMPTY_REFILL_NUMBER);
        }

        result = this.m_CacheBank.get(0);
        this.m_CacheBank.remove(0);

        return result;

    }

    // External push values

    public void pushBackParticles(Collection<PaintParticleGravitron> particles)
    {

        for(PaintParticleGravitron paintParticleGravitron : particles)
        {
            //paintParticleGravitron.getmMotionSecond().scaleToLength(1f);
            paintParticleGravitron.setmMotionSecond(new Vector2(0f, 0f));
            //paintParticleLine.getmMotionSecond3d().scaleToLength(1f);
            paintParticleGravitron.setmGravity(this.getRandomGravity());
            paintParticleGravitron.setmMillisecondsToDeath(this.getRandomLifeTime());
        }

        this.m_CacheBank.addAll(particles);
    }


    //endregion

    //region Private

    private void replenishInstances(int numberOfElements)
    {
        PaintParticleGravitron paintParticleGravitron;
        Vector2 startingLocation = new Vector2(0f,0f);
        Vector2 motionDirection = new Vector2(0f,-0f);

        for(int i = 0; i < numberOfElements; i++)
        {

            paintParticleGravitron =
                    new PaintParticleGravitron(startingLocation,
                            motionDirection,
                            this.getRandomLifeTime(),
                            this.getRandomGravity()
                            );

            this.m_CacheBank.add(paintParticleGravitron);

        }
    }

    private float getRandomGravity()
    {
        float result =
            (float)(Math.abs(this.m_RandomProvider.nextGaussian()) *
                    CrateBrickParticleEngine.GRAVITY_STANDARD_DEVIATION_POSITIVE_PX +
                    CrateBrickParticleEngine.GRAVITY_AVERAGE_PX);
        return result;
    }

    private int getRandomLifeTime()
    {


        // No need for rounding. Not to accurate value over all.
        int lifetime =
            (int)((Math.abs(this.m_RandomProvider.nextGaussian()) * CrateBrickParticleEngine.LIFETIME_IN_MILLISECONDS_STANDARD_DEVIATION) +
            CrateBrickParticleEngine.AVERAGE_PARTICLE_LIFETIME_IN_MILLISECONDS);

        return lifetime;

    }

    //endregion

    //endregion

    //endregion

    //region Static

    //region Constants

    //region Private

    // Bulk values
    private static final int STARTING_SIZE = 10000;
    private static final int ON_EMPTY_REFILL_NUMBER = 2000;
    // Life time
    private static final int AVERAGE_PARTICLE_LIFETIME_IN_MILLISECONDS = 3000;
    private static final int LIFETIME_IN_MILLISECONDS_STANDARD_DEVIATION = 1000;
    // Gravity
    private static final float GRAVITY_AVERAGE_PX = 280;
    private static final float GRAVITY_STANDARD_DEVIATION_POSITIVE_PX = 140;

    //endregion

    //endregion

    //region Variables

    //region Private

    private static CrateBrickParticleEngine ourInstance = new CrateBrickParticleEngine();

    //endregion

    //endregion

    //region Encapsulation

    public static CrateBrickParticleEngine getInstance() {
        return ourInstance;
    }

    //endregion

    //endregion

}
