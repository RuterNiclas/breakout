package com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks;

import com.example.nismrg.breakoutsample.GameObjects.HitFrom;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nismrg on 2016-09-14.
 */
public class HitFromAndSteelBrickExplosionResultSet
{

    //region Variables

    //region Private

    private Map<HitFrom, SteelBrickExplosionResultSet> m_HitFromMap;

    //endregion

    //endregion

    //region Encapsulation

    public Map<HitFrom, SteelBrickExplosionResultSet> getM_HitFromMap() {
        return m_HitFromMap;
    }

    public void setM_HitFromMap(Map<HitFrom, SteelBrickExplosionResultSet> m_HitFromMap) {
        this.m_HitFromMap = m_HitFromMap;
    }

    //endregion

    //region Constructors

    public HitFromAndSteelBrickExplosionResultSet()
    {
        this.setM_HitFromMap(new HashMap<HitFrom, SteelBrickExplosionResultSet>());
    }

    //endregion

}
