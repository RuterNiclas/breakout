package com.example.nismrg.breakoutsample.ResourceManaging;

import android.graphics.Bitmap;
import android.util.Size;

import com.example.nismrg.breakoutsample.Math.Vector2IntNotEncapsulated;

/**
 * Created by nismrg on 2016-11-02.
 */
public class GIFAndMetaData
{

    //region Variables

    //region Private

    private Bitmap mBitmap;
    private Size mDimensions;
    private int mImagesInTotal;
    private Size mElementSize;

    //endregion

    //endregion

    //region Encapsulation

    public Bitmap getmBitmap() {
        return mBitmap;
    }

    public void setmBitmap(Bitmap mBitmap) {
        this.mBitmap = mBitmap;
    }

    public Size getmDimensions() {
        return mDimensions;
    }

    public void setmDimensions(Size mDimensions) {
        this.mDimensions = mDimensions;
    }

    public int getmImagesInTotal() {
        return mImagesInTotal;
    }

    public void setmImagesInTotal(int mImagesInTotal) {
        this.mImagesInTotal = mImagesInTotal;
    }

    public Size getmElementSize() {
        return mElementSize;
    }

    public void setmElementSize(Size mElementSize) {
        this.mElementSize = mElementSize;
    }

    //endregion

    //region Constructors

    public GIFAndMetaData(Bitmap bitmap, Size dimensions)
    {
        this.myInitialize(bitmap, dimensions);
    }

    //endregion

    //region Methods

    //region Public

    public int getFrameNumberFromPositionInCycle(float position)
    {
        float inLoopValue = position % 1;

        return Math.round(this.getmImagesInTotal() * inLoopValue);
    }

    public Vector2IntNotEncapsulated getChartLocationByPositionInCycle(float position)
    {
        int frameNumber = this.getFrameNumberFromPositionInCycle(position);

        // Specific handling on value of exactly one.
        if(frameNumber == getmImagesInTotal())
        {
            //                                                Index start at 0                Index start at 0
            return  new Vector2IntNotEncapsulated(this.getmDimensions().getWidth() - 1, this.getmDimensions().getHeight() - 1);
        }
        else
        {

            int row = frameNumber / this.getmDimensions().getWidth();
            int column = frameNumber - (this.getmDimensions().getWidth() * row);

            return new Vector2IntNotEncapsulated(column, row);

        }
    }

    //endregion

    //region Private

    private void myInitialize(Bitmap bitmap, Size dimensions)
    {

        this.setmBitmap(bitmap);
        this.setmDimensions(dimensions);

        this.setmImagesInTotal(this.getmDimensions().getWidth() * this.getmDimensions().getHeight());

        // Calculate fragment size
        int width = this.mBitmap.getWidth() / this.getmDimensions().getWidth();
        int height = this.mBitmap.getHeight() / this.getmDimensions().getHeight();
        this.setmElementSize(new Size(width, height));
    }

    //endregion

    //endregion

}
