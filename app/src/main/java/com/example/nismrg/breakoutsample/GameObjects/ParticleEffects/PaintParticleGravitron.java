package com.example.nismrg.breakoutsample.GameObjects.ParticleEffects;

import android.graphics.Color;
import android.graphics.Paint;
import android.util.Size;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.Math.Vector2;

/**
 * Created by nismrg on 2016-08-21.
 */
public class PaintParticleGravitron extends ParticleBase
{

    //region Variables

    //region Private

    private float mGravity;
    private Paint mYPaint;
    private int mColor;
    private Size mSize;

    //endregion

    //endregion

    //region Encapsulation

    public float getmGravity() {
        return mGravity;
    }
    public void setmGravity(float mGravity) {
        this.mGravity = mGravity;
    }

    public Paint getmYPaint() {
        return mYPaint;
    }

    public void setmYPaint(Paint mYPaint) {
        this.mYPaint = mYPaint;
    }

    public int getmColor() {
        return mColor;
    }

    public void setmColor(int mColor)
    {
        this.mColor = mColor;
        if(this.getmYPaint() == null)
        {
            this.setmYPaint(new Paint());
        }

        this.getmYPaint().setColor(mColor);

    }

    public Size getmSize() {
        return mSize;
    }

    public void setmSize(Size mSize)
    {
        this.mSize = mSize;

        if(this.getmYPaint() == null)
        {
            this.setmYPaint(new Paint());
        }

        this.getmYPaint().setStrokeWidth(mSize.getWidth());
    }

    //endregion

    //region Constructors

    public PaintParticleGravitron(Vector2 location,
                                  Vector2 motionSecond,
                                  int millisecondsToDeath,
                                  float atGravity)
    {
        super(location,
              motionSecond,
              millisecondsToDeath);

        this.myInitialize(atGravity);
    }

    //endregion

    //region Methods

    //region Public

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {
        super.drawMe(drawInformation);

        float x = super.getmLocation().getM_x();
        float y = super.getmLocation().getM_Y();



        drawInformation.getmCanvas().drawLine(x, y, x + this.getmSize().getHeight(), y + this.getmSize().getWidth(), this.getmYPaint());
    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {
        super.updateMe(updateInformation);

        // Need to make this control since  this can be added within a update frame.
        if(updateInformation.getmAtFps() > 0)
        {


            this.getmMotionSecond().addVector(new Vector2(0, this.getmGravity() * updateInformation.mOneDividedByFPS));

            super.getmLocation().addVector(super.getmMotionSecond().scale(updateInformation.mOneDividedByFPS));
        }

    }

    @Override
    public int getM_AtFrameFragmentsFrame() {
        return super.getM_AtFrameFragmentsFrame();
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize(float atGravity)
    {
        this.setmGravity(atGravity);
        this.setmYPaint(new Paint(Color.BLACK));
        this.setmSize(new Size(1,1));
    }

    //endregion

    //endregion

}
