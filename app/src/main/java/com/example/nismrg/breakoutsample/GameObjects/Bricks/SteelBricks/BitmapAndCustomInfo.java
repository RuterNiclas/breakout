package com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks;

import com.example.nismrg.breakoutsample.GameObjects.HitFrom;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nismrg on 2016-07-25.
 */
public class BitmapAndCustomInfo
{

    //region Variables

    //region Private

    private Map<HitFrom, BitMapAndName> mBitMap;
    private Vector2 mScale;
    private Map<HitFrom, ArrayList<BitMapAndName>> mFragments;

    //endregion

    //endregion

    //region Encapsulation

    public Map<HitFrom, BitMapAndName> getmBitMap() {
        return mBitMap;
    }

    public void setmBitMap(Map<HitFrom, BitMapAndName> mBitMap) {
        this.mBitMap = mBitMap;
    }

    public Vector2 getmScale() {
        return mScale;
    }

    public void setmScale(Vector2 mScale) {
        this.mScale = mScale;
    }

    public Map<HitFrom, ArrayList<BitMapAndName>> getmFragments() {
        return mFragments;
    }

    public void setmFragments(Map<HitFrom, ArrayList<BitMapAndName>> mFragments) {
        this.mFragments = mFragments;
    }

    //endregion

    //region Constructors

    public BitmapAndCustomInfo()
    {
        this.setmBitMap(new HashMap<HitFrom, BitMapAndName>());
        this.setmFragments(new HashMap<HitFrom, ArrayList<BitMapAndName>>());
    }

    //endregion

}
