package com.example.nismrg.breakoutsample.GameObjects;

/**
 * Created by nismrg on 2016-06-03.
 */
public enum HitFrom
{
    NONE,
    BELOW,
    ABOVE,
    LEFT,
    RIGHT
}
