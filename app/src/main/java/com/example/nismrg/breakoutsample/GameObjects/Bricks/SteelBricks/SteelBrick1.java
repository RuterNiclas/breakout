package com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Pair;

import com.example.nismrg.breakoutsample.DeviceFunctions.VibrationEnums;
import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickLocationAndSize;
import com.example.nismrg.breakoutsample.GameObjects.HitFrom;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

import java.util.ArrayList;

import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-07-11.
 */
public class SteelBrick1 extends SteelBrickBase
{

    //region Constants

    //region Private

    //region Vibrations

    private static final boolean VIBRATES_ON_HIT = true;
    private static final long[] VIBRATE_PATTERN_ON_HIT = new long[]{ 0, VibrationEnums.LENGTH_SMALLEST_POSSIBLE };
    private static final int VIBRATIONS_REPEAT_ON_HIT = -1;

    //endregion

    private final int HIT_POINTS = 3;

    private final int AVERAGE_FRAGMENT_BALLS_FIRST_HIT = 1;
    private final int STANDARD_DEVIATION_FRAGMENT_BALLS_FIRST_HIT = 1;
    private final int AVERAGE_PARTICLES_FIRST_HIT = 10;
    private final int STANDARD_DEVIATION_PARTICLES_FIRST_HIT = 60;
    private final int AVERAGE_FRAGMENT_BALLS_SECOND_HIT = 0;
    private final int STANDARD_DEVIATION_FRAGMENT_BALLS_SECOND_HIT = 0;
    private final int AVERAGE_PARTICLES_SECOND_HIT = 20;
    private final int STANDARD_DEVIATION_PARTICLES_SECOND_HIT = 70;

    private final int MAX_DEVIATION_OF_SHRAPNEL_ANGLE_DEGREES = 15;

    private final float AVERAGE_ROTATION_TILT_OF_HIT_IN_DEGREES = 4;

    //endregion

    //endregion

    //region Variables

    //region Private

    private ArrayList<BallBase> mExplodeResult;

    //endregion

    //endregion

    //region Encapsulation

    //region Private

    private BitmapAndCustomInfo getOneDamageImage()
    {

        //region On demand load

        if(super.oneDamageImage == null)
        {

            // Performance.
            ResourceHolderImages instance = ResourceHolderImages.getInstance();

            super.setUpOneDamageImage(instance.getmSteel1Dmg1Bottom(),
                                      instance.getmSteel1Dmg1Top(),
                                      instance.getmSteel1Dmg1Left(),
                                      instance.getmSteel1Dmg1Right(),
                                      instance.getmSteel1OriginalSize(),
                                      instance.getmSteel1Fragment1(),
                                      instance.getmSteel1Fragment2(),
                                      instance.getmSteel1Fragment3());

        }

        //endregion

        return super.oneDamageImage;

    }

    private void setOneDamageImage(BitmapAndCustomInfo oneDamageImage) {
        super.oneDamageImage = oneDamageImage;
    }

    private BitmapAndCustomInfo getTwoDamageImage()
    {

        //region On demand load

        if(super.twoDamageImage == null)
        {

            // Performance
            ResourceHolderImages instance = ResourceHolderImages.getInstance();

            super.setUpTwoDamageImage(instance.getmSteel1Dmg2Bottom(),
                                      instance.getmSteel1Dmg2Top(),
                                      instance.getmSteel1Dmg2Left(),
                                      instance.getmSteel1Dmg2Right(),
                                      instance.getmSteel1OriginalSize(),
                                      instance.getmSteel1Fragment1(),
                                      instance.getmSteel1Fragment2(),
                                      instance.getmSteel1Fragment3());
        }

        //endregion

        return super.twoDamageImage;

    }

    private void setTwoDamageImage(BitmapAndCustomInfo twoDamageImage) {
        super.twoDamageImage = twoDamageImage;
    }

    //endregion

    //endregion

    //region Constructors

    public SteelBrick1(World world, BrickLocationAndSize brickLocationAndSize)
    {
        super(world,
              brickLocationAndSize,
              VIBRATES_ON_HIT,
              VIBRATE_PATTERN_ON_HIT,
              VIBRATIONS_REPEAT_ON_HIT);

        this.initializeMe();
    }

    //endregion

    //region Methods

    //region Public

    @Override
    public void takeDamage(UpdateInformation updateInformation)
    {

        super.takeDamage(updateInformation);

        switch (super.getM_HitPointsLeft())
        {
            case HIT_POINTS :
                break;
            case (HIT_POINTS - 1):
                super.feedDamageResult(1, super.getM_FirstHitFrom());
                break;
            case (HIT_POINTS - 2):
                super.feedDamageResult(2, HitFrom.NONE);
                break;
            default:
                super.feedDamageResult(3, HitFrom.NONE);
                break;
        }

        super.m_BrickTilt += (RandomProvider.getInstance().nextFloat() *
                              this.AVERAGE_ROTATION_TILT_OF_HIT_IN_DEGREES *
                              2f) -
                             this.AVERAGE_ROTATION_TILT_OF_HIT_IN_DEGREES;
    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        Canvas canvas = drawInformation.getmCanvas();

        super.drawMe(drawInformation);

        // Bitmap matrix paint
        Matrix theMatrix = new Matrix();
        theMatrix.postRotate(super.m_BrickTilt);
        theMatrix.postTranslate(this.getM_DrawingRectangle().left, this.getM_DrawingRectangle().top);

        switch (super.getM_HitPointsLeft())
        {
            case HIT_POINTS :
                canvas.drawBitmap(this.noDamageImage.getmBitmap(), theMatrix, null);
                /*
                canvas.drawBitmap(this.noDamageImage,
                                  null,
                                  super.getM_DrawingRectangle(),
                                  null);
                                  */
                break;
            case (HIT_POINTS - 1):
                canvas.drawBitmap(this.getOneDamageImage().getmBitMap().get(super.getM_FirstHitFrom()).getmBitmap(), theMatrix, null);
                /*
                canvas.drawBitmap(this.getOneDamageImage().getmBitMap().get(super.getM_FirstHitFrom()),
                                  null,
                                  super.getM_DrawingRectangle(),
                                  null);
                                  */
                break;
            default:
                canvas.drawBitmap(this.getTwoDamageImage().getmBitMap().get(super.getM_FirstHitFrom()).getmBitmap(), theMatrix, null);
                /*
                canvas.drawBitmap(this.getTwoDamageImage().getmBitMap().get(super.getM_FirstHitFrom()),
                                  null,
                                  super.getM_DrawingRectangle(),
                                  null);
                                 */
                break;

        }
    }

    //endregion

    //endregion

    //region Protected

    protected Bitmap getRandomDMG1Bitmap()
    {

        RandomProvider randomProvider = RandomProvider.getInstance();

        int randomIndex = randomProvider.nextInt(3);

        Bitmap bitMap;

        switch (randomIndex)
        {
            case 0 : bitMap = this.getOneDamageImage().getmFragments().get(super.getM_FirstHitFrom()).get(0).getmBitmap();
                break;
            case 1 : bitMap = this.getOneDamageImage().getmFragments().get(super.getM_FirstHitFrom()).get(1).getmBitmap();
                break;
            case 2 : bitMap =this.getOneDamageImage().getmFragments().get(super.getM_FirstHitFrom()).get(2).getmBitmap();
                break;
            default : bitMap = this.getOneDamageImage().getmFragments().get(super.getM_FirstHitFrom()).get(0).getmBitmap();
                break;
        }

        return bitMap;
    }

    protected int getRandomDMGColorWeighted()
    {

        RandomProvider randomProvider = RandomProvider.getInstance();

        int lengthOfSteel1Image = ResourceHolderImages.getInstance().getmSteel1ImagePixels().size();
        int randomIndex = randomProvider.nextInt(lengthOfSteel1Image);

        return ResourceHolderImages.getInstance().getmSteel1ImagePixels().get(randomIndex);

    }

    //endregion

    //region Private

    private void initializeMe()
    {
        super.setM_HitPointsLeft(HIT_POINTS);

        this.noDamageImage = super.scaleBitmapToBrickSize(ResourceHolderImages.getInstance().getmSteel1()/*, false*/);

        //region Pre build damages

        //region Make possible damage results

        Pair<HitFrom, SteelBrickExplosionResultSet> dmg1Top =
            super.feedDamageResultPreBuilt(HitFrom.ABOVE,
                                           AVERAGE_FRAGMENT_BALLS_FIRST_HIT,
                                           STANDARD_DEVIATION_FRAGMENT_BALLS_FIRST_HIT,
                                           AVERAGE_PARTICLES_FIRST_HIT,
                                           STANDARD_DEVIATION_PARTICLES_FIRST_HIT,
                                           MAX_DEVIATION_OF_SHRAPNEL_ANGLE_DEGREES,
                                           this.getOneDamageImage().getmFragments().get(HitFrom.ABOVE),
                                           false);

        Pair<HitFrom, SteelBrickExplosionResultSet> dmg1Bottom =
            super.feedDamageResultPreBuilt(HitFrom.BELOW,
                                           AVERAGE_FRAGMENT_BALLS_FIRST_HIT,
                                           STANDARD_DEVIATION_FRAGMENT_BALLS_FIRST_HIT,
                                           AVERAGE_PARTICLES_FIRST_HIT,
                                           STANDARD_DEVIATION_PARTICLES_FIRST_HIT,
                                           MAX_DEVIATION_OF_SHRAPNEL_ANGLE_DEGREES,
                                           this.getOneDamageImage().getmFragments().get(HitFrom.BELOW),
                                           false);

        Pair<HitFrom, SteelBrickExplosionResultSet> dmg1Left =
                super.feedDamageResultPreBuilt(HitFrom.LEFT,
                                               AVERAGE_FRAGMENT_BALLS_FIRST_HIT,
                                               STANDARD_DEVIATION_FRAGMENT_BALLS_FIRST_HIT,
                                               AVERAGE_PARTICLES_FIRST_HIT,
                                               STANDARD_DEVIATION_PARTICLES_FIRST_HIT,
                                               MAX_DEVIATION_OF_SHRAPNEL_ANGLE_DEGREES,
                                               this.getOneDamageImage().getmFragments().get(HitFrom.LEFT),
                                               false);

        Pair<HitFrom, SteelBrickExplosionResultSet> dmg1Right =
            super.feedDamageResultPreBuilt(HitFrom.RIGHT,
                                           AVERAGE_FRAGMENT_BALLS_FIRST_HIT,
                                           STANDARD_DEVIATION_FRAGMENT_BALLS_FIRST_HIT,
                                           AVERAGE_PARTICLES_FIRST_HIT,
                                           STANDARD_DEVIATION_PARTICLES_FIRST_HIT,
                                           MAX_DEVIATION_OF_SHRAPNEL_ANGLE_DEGREES,
                                           this.getOneDamageImage().getmFragments().get(HitFrom.RIGHT),
                                           false);

        Pair<HitFrom, SteelBrickExplosionResultSet> dmg2None =
            feedDamageResultPreBuilt(HitFrom.NONE,
                                     AVERAGE_FRAGMENT_BALLS_SECOND_HIT,
                                     STANDARD_DEVIATION_FRAGMENT_BALLS_SECOND_HIT,
                                     AVERAGE_PARTICLES_SECOND_HIT,
                                     STANDARD_DEVIATION_PARTICLES_SECOND_HIT,
                                     MAX_DEVIATION_OF_SHRAPNEL_ANGLE_DEGREES,
                                     this.getOneDamageImage().getmFragments().get(HitFrom.LEFT),
                                     true);

        Pair<HitFrom, SteelBrickExplosionResultSet> dmg3None =
            feedDamageResultPreBuilt(HitFrom.NONE,
                                     AVERAGE_FRAGMENT_BALLS_SECOND_HIT,
                                     STANDARD_DEVIATION_FRAGMENT_BALLS_SECOND_HIT,
                                     AVERAGE_PARTICLES_SECOND_HIT,
                                     STANDARD_DEVIATION_PARTICLES_SECOND_HIT,
                                     MAX_DEVIATION_OF_SHRAPNEL_ANGLE_DEGREES,
                                     this.getOneDamageImage().getmFragments().get(HitFrom.NONE),
                                     true);

        //endregion

        //region Register

        HitFromAndSteelBrickExplosionResultSet dmg1Set = new HitFromAndSteelBrickExplosionResultSet();
        dmg1Set.getM_HitFromMap().put(dmg1Top.first, dmg1Top.second);
        dmg1Set.getM_HitFromMap().put(dmg1Bottom.first, dmg1Bottom.second);
        dmg1Set.getM_HitFromMap().put(dmg1Left.first, dmg1Left.second);
        dmg1Set.getM_HitFromMap().put(dmg1Right.first, dmg1Right.second);
        super.getDamageToSteelBrickExplosionResultSet().put(1, dmg1Set);

        HitFromAndSteelBrickExplosionResultSet dmg2Set = new HitFromAndSteelBrickExplosionResultSet();
        dmg2Set.getM_HitFromMap().put(dmg2None.first, dmg2None.second);
        super.getDamageToSteelBrickExplosionResultSet().put(2, dmg2Set);

        HitFromAndSteelBrickExplosionResultSet dmg3Set = new HitFromAndSteelBrickExplosionResultSet();
        dmg3Set.getM_HitFromMap().put(dmg3None.first, dmg3None.second);
        super.getDamageToSteelBrickExplosionResultSet().put(3, dmg3Set);

        //endregion

        //endregion

    }

    //endregion

    //endregion

}
