package com.example.nismrg.breakoutsample.Math;

/**
 * Created by nismrg on 2016-06-09.
 */
public class Vector1 implements IVector<Vector1> {

    //region Variables

    //region Private

    private float m_x;

    //endregion

    //endregion

    //region Encapsulation

    public float getM_x() {
        return m_x;
    }

    public void setM_x(float m_x) {
        this.m_x = m_x;
    }

    //endregion

    //region Constructors

    public Vector1(float x)
    {
        this.setM_x(x);
    }

    //endregion

    //region Methods

    //region Public

    //region IVector

    public float getLength()
    {
        return this.getM_x();
    }

    @Override
    public void rotateAmongZAxis(float angle)
    {
        this.rotateAmongZAxis((double) angle);
    }

    @Override
    public void rotateAmongZAxis(double angle)
    {
        double radians = Math.toRadians(angle);

        this.setM_x((float)Math.cos(radians) * this.getM_x());
    }

    @Override
    public Vector1 rotateAmongZAxisNewVector(float angle)
    {
        return this.rotateAmongZAxisNewVector((double)angle);
    }

    @Override
    public Vector1 rotateAmongZAxisNewVector(double angle)
    {
        double radians = Math.toRadians(angle);

        Vector1 newVector = new Vector1((float)Math.cos(radians) * this.getM_x());

        return  newVector;
    }

    @Override
    public Vector1 scale(float scale)
    {
        return new Vector1(this.getM_x() * scale);
    }

    public void scaleToLength(float length)
    {
        if(length == 0)
        {
            this.setM_x(length);
            return;
        }

        if(this.getLength() > length)
        {
            float factor = length / this.getLength();
            this.setM_x(this.scale(factor).getM_x());
        }
        else
        {
            this.setM_x(length);
        }
    }

    public Vector1 clampToLength(float length)
    {

        if(length == 0)
        {
            return new Vector1(0);
        }

        if(this.getLength() > length)
        {
            float factor = length / this.getLength();
            return  this.scale(factor);
        }
        else
        {
            return  new Vector1(this.getM_x());
        }
    }

    @Override
    public void addVector(Vector1 v)
    {
        this.setM_x(this.getM_x() + v.getM_x());
    }

    @Override
    public Vector1 addAndNewVector(Vector1 v)
    {
        return new Vector1(this.getM_x() + v.getM_x());
    }

    @Override
    public void normalize()
    {
        this.scaleToLength(1f);
    }

    //endregion

    //endregion

    //endregion

}
