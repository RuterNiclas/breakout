package com.example.nismrg.breakoutsample.Math;

/**
 * Created by nismrg on 2016-06-18.
 */
public class DeveloperFriendlyMatrix4 {


    private final float[] mRotationMatrix = new float[16];

    public DeveloperFriendlyMatrix4() {
        this.makeIdentity();
    }

    public void makeIdentity() {
        this.getmRotationMatrix()[0] = 1;
        this.getmRotationMatrix()[4] = 1;
        this.getmRotationMatrix()[8] = 1;
        this.getmRotationMatrix()[12] = 1;
    }


    public float[] getmRotationMatrix() {
        return mRotationMatrix;
    }

    public float getValueAt(int x, int y) {
        if (x < 0) {
            throw new ArrayIndexOutOfBoundsException("x can't be smaller than 0");
        }
        if (x > 3) {
            throw new ArrayIndexOutOfBoundsException("x can't be greater than 3");
        }
        if (y < 0) {
            throw new ArrayIndexOutOfBoundsException("y can't be smaller than 0");
        }
        if (y > 3) {
            throw new ArrayIndexOutOfBoundsException("y can't be greater than 3");
        }

        return this.getmRotationMatrix()[y * 4 + x];
    }

    public float getRotationInZ() {
        return this.getValueAt(2, 2);
    }

    @Override
    public String toString() {
        String result = String.format("%1.2f %2.2f %3.2f %4.2f\n" +
                        "%5.2f %6.2f %7.2f %8.2f\n" +
                        "%9.2f %10.2f %11.2f %12.2f\n" +
                        "%13.2f %14.2f %15.2f %16.2f",
                this.mRotationMatrix[0],
                this.mRotationMatrix[1],
                this.mRotationMatrix[2],
                this.mRotationMatrix[3],
                this.mRotationMatrix[4],
                this.mRotationMatrix[5],
                this.mRotationMatrix[6],
                this.mRotationMatrix[7],
                this.mRotationMatrix[8],
                this.mRotationMatrix[9],
                this.mRotationMatrix[10],
                this.mRotationMatrix[11],
                this.mRotationMatrix[12],
                this.mRotationMatrix[13],
                this.mRotationMatrix[14],
                this.mRotationMatrix[15]);
        return result;
    }
}
