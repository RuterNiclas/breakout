package com.example.nismrg.breakoutsample.GameObjects.Balls.FastBall;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

import Utility.FifoListMaxSize;
import Utility.MathUtility;
import Utility.MiscConstants;

/**
 * Created by nismrg on 2016-07-10.
 */
public class FastBall extends BallBase {

    //region Constants

    //region Private

    private final int LAUNCH_SPEED_UNITS_SECOND = 650;
    private final int DEFAULT_RADIUS = 15;

    private final int DRAG_SEGMENTS = 100;
    private final int MILLISECONDS_BETWEEN_SEGMENTS = 10;
    private final float INTERPOLATOR_WEIGHT_TO_AVOID_MIDDLE_WAVE_LENGTH_COLORS = 1f;

    //endregion

    //endregion

    //region Variables

    //                       We use a struct similar thing here to improve performance, no encapsulation or similar. Vector 2 is a candidate.
    private FifoListMaxSize<FastBallTrace> mTrace;
    private Paint mMyPaint;

    private int mColor;
    private int mInterpolateToColor;

    //endregion

    //region Constructors

    public FastBall(World world)
    {
        super(world);

        this.setM_Radius(DEFAULT_RADIUS);

        this.myInitialize();

    }

    public FastBall(World world, float radius)
    {
        super(world, radius);

        this.myInitialize();

    }

    //endregion

    //region Methods

    //region Public

    public float getStartingSpeed()
    {
        return LAUNCH_SPEED_UNITS_SECOND;
    }

    public void drawMeAt(DrawInformation drawInformation, Vector2 location)
    {

        Canvas canvas = drawInformation.getmCanvas();

        this.mMyPaint.setAlpha(MiscConstants.COLOR_CHANEL_MAX);
        this.mMyPaint.setColor(this.mColor);



        canvas.drawCircle(location.getM_x(),
                location.getM_Y(),
                this.getM_Radius(),
                this.mMyPaint);

        float diminishOneToZero;

        for(int i = this.mTrace.size() -1; i > 0; i--)
        {

            diminishOneToZero =  (float)(i) / this.mTrace.size();

            this.mMyPaint.setColor(Math.round(MathUtility.linearInterpolation(this.mColor,
                                                                              this.mInterpolateToColor,
                                                                              (1f - diminishOneToZero) * this.INTERPOLATOR_WEIGHT_TO_AVOID_MIDDLE_WAVE_LENGTH_COLORS )));

            this.mMyPaint.setAlpha(Math.round(diminishOneToZero * MiscConstants.COLOR_CHANEL_MAX_FLOAT));

            canvas.drawCircle(this.mTrace.get(i).mX,
                              this.mTrace.get(i).mY,
                              this.getM_Radius() * diminishOneToZero,
                              this.mMyPaint);
        }
    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation) {

        //super.drawMe(canvas, gameTimeRunMillis);

        this.drawMeAt(drawInformation, this.getM_Location());
    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {
        super.updateMe(updateInformation);

        // No need to do this for ball on paddle.
        if(super.getMotionSecond().getLength() > 0)
        {

            if (this.mTrace.size() == 0) {
                this.mTrace.add(new FastBallTrace(super.getM_Location().getM_x(),
                        super.getM_Location().getM_Y(),
                        updateInformation.getmGameTimeRunMillis()));
            } else {
                if (this.mTrace.getLast().mAt + this.MILLISECONDS_BETWEEN_SEGMENTS < updateInformation.getmTotalTimeMillis()) {
                    this.mTrace.add(new FastBallTrace(super.getM_Location().getM_x(),
                            super.getM_Location().getM_Y(),
                            updateInformation.getmGameTimeRunMillis()));
                }
            }
        }
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize()
    {
        this.mTrace = new FifoListMaxSize<>(this.DRAG_SEGMENTS);
        this.mMyPaint = new Paint();

        ResourceHolderImages resourceHolderImages = ResourceHolderImages.getInstance();

        this.mColor = resourceHolderImages.getmFastBallColor();
        this.mInterpolateToColor = resourceHolderImages.getmFastBallInterpolateToColor();

    }

    //endregion

    //endregion

}
