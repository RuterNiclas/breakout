package com.example.nismrg.breakoutsample.GameObjects.Balls;

import android.graphics.Canvas;
import android.graphics.Color;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

/**
 * Created by nismrg on 2016-07-10.
 */
public class LaserBall extends BallBase {

    //region Constants

    //region Private

    private final int LAUNCH_SPEED_UNITS_SECOND = 400;
    private final int DEFAULT_RADIUS = 30;

    //endregion

    //region Variables

    //region Private

    private BitMapAndName mImage;

    //endregion

    //endregion

    //endregion

    //region Constructors

    public LaserBall(World world)
    {
        super(world);

        this.setM_Radius(DEFAULT_RADIUS);

        this.myInitialize();

    }

    public LaserBall(World world, float radius)
    {
        super(world, radius);

        this.myInitialize();

    }

    //endregion

    //region Methods

    //region Public

    public float getStartingSpeed()
    {
        return LAUNCH_SPEED_UNITS_SECOND;
    }

    public void drawMeAt(DrawInformation drawInformation, Vector2 location)
    {


        Canvas canvas = drawInformation.getmCanvas();

        canvas.drawBitmap(this.mImage.getmBitmap(),
                          location.getM_x() - super.getM_Radius(),
                          location.getM_Y() - super.getM_Radius(),
                          null);

    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation) {

        //super.drawMe(canvas, gameTimeRunMillis);

        this.drawMeAt(drawInformation, this.getM_Location());
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize()
    {
        this.mImage = ResourceHolderImages.getInstance().getResizedBitmap(ResourceHolderImages.getInstance().getmCrossHairImage(),
                                                                          Math.round(this.getM_Radius() * 2),
                                                                          Math.round(this.getM_Radius() * 2));
    }

    //endregion

    //endregion

    //region Static

    //region Variables

    //region Public

    public static final float LASER_HEIGHT_TO_SCREEN_RATIO = 0.3f;
    public static final float LASER_HEIGHT_TO_SCREEN_RATIO_ANGLE_INDICATOR = 0.2f;
    public static final int LASER_COLOR = Color.RED;
    public static final int LASER_GUIDE_COLOR = Color.GREEN;

    //endregion

    //endregion

    //endregion

}
