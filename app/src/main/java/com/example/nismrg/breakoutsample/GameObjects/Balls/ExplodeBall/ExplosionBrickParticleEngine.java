package com.example.nismrg.breakoutsample.GameObjects.Balls.ExplodeBall;

import android.util.Size;

import com.example.nismrg.breakoutsample.GameObjects.MiscellaneousLogic.IParticleEngine;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.Explosion;
import com.example.nismrg.breakoutsample.Math.Vector2;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by nismrg on 2016-11-11.
 */
public class ExplosionBrickParticleEngine implements IParticleEngine<Explosion>
{

    //region Instance

    //region Variables

    //region Private

    private ArrayList<Explosion> m_CacheBank;

    //endregion

    //endregion

    //region Constructors

    private ExplosionBrickParticleEngine()
    {
    }

    //endregion

    //region Methods

    //region Public

    // Start the singleton build a cache of objects.
    public void InitializeSingleTon()
    {

        this.m_CacheBank = new ArrayList<>();

        this.replenishInstances(this.STARTING_SIZE);
    }

    public ArrayList<Explosion> pullParticles(int numberOfObjects)
    {

        ArrayList<Explosion> result = new ArrayList<>();

        if(this.m_CacheBank.size() < numberOfObjects)
        {
            int missingValues = numberOfObjects - this.m_CacheBank.size();

            for(int i = 0; i < missingValues; i+= this.ON_EMPTY_REFILL_NUMBER)
            {
                this.replenishInstances(this.ON_EMPTY_REFILL_NUMBER);
            }
        }

        for(int i = 0; i < numberOfObjects; i++)
        {
            result.add(this.m_CacheBank.get(0));
            this.m_CacheBank.remove(0);
        }

        return result;

    }

    public Explosion pullSingleParticle()
    {

        Explosion result = null;

        if(this.m_CacheBank.size() < 1)
        {
            this.replenishInstances(this.ON_EMPTY_REFILL_NUMBER);
        }

        result = this.m_CacheBank.get(0);
        this.m_CacheBank.remove(0);

        return result;

    }

    // External push values

    public void pushBackParticles(Collection<Explosion> particles)
    {

        for(Explosion explosion : particles)
        {
            explosion.setLocationCascade(new Vector2(0f,0f));
            explosion.setmLocation(new Vector2(0f,0f));
            explosion.setmMillisecondsToDeath(this.EXPLOSION_LIFE_TIME_IN_MILLI_SECONDS);
            // Redundant in this case
            // explosion.setmMotionSecond(new Vector2(0f, 0f));
        }

        this.m_CacheBank.addAll(particles);
    }

    //endregion

    //endregion

    //region Private

    private void replenishInstances(int numberOfElements)
    {
        Explosion explosion;
        Vector2 startingLocation = new Vector2(0f,0f);
        Vector2 motionDirection = new Vector2(0f,-1f);
        Size originalSize = new Size(0,0);

        for(int i = 0; i < numberOfElements; i++)
        {
            explosion = new Explosion(startingLocation,
                                      motionDirection,
                                      this.EXPLOSION_LIFE_TIME_IN_MILLI_SECONDS,
                                      originalSize,
                                      1f);

            this.m_CacheBank.add(explosion);

        }
    }


    //endregion

    //endregion

    //endregion

    //region Static

    //region Constants

    //region Private

    // Bulk values
    private static final int STARTING_SIZE = 10;
    private static final int ON_EMPTY_REFILL_NUMBER = 10;

    // Life time
    private final int EXPLOSION_LIFE_TIME_IN_MILLI_SECONDS = 1500;


    //endregion

    //endregion

    //region Variables

    //region Private

    private static ExplosionBrickParticleEngine ourInstance = new ExplosionBrickParticleEngine();

    //endregion

    //endregion

    //region Encapsulation

    //region Public

    public static ExplosionBrickParticleEngine getInstance()
    {
        return ourInstance;
    }

    //endregion

    //endregion

    //endregion

}
