package com.example.nismrg.breakoutsample.GameObjects.Controls;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Size;

import com.example.nismrg.breakoutsample.DeviceFunctions.DeviceFunctions;
import com.example.nismrg.breakoutsample.DeviceFunctions.VibrationEnums;
import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

/**
 * Created by nismrg on 2016-08-10.
 */
public class PauseButton extends GameButtonBase
{

    //region Variables

    //region Private

    //region Vibrations

    private final long[] VIBRATE_PATTERN = new long[]{ 0, VibrationEnums.LENGTH_INFO };
    private final int VIBRATIONS_REPEAT = -1;

    //endregion

    //private boolean mPressed;
    private boolean mBeenUp;
    private boolean mToggled;

    private Size mSize;
    private Rect mRect;

    private BitMapAndName mPauseImage;
    private BitMapAndName mPlayImage;

    //endregion

    //region Public

    public GameButtonOnClickListener listener;

    //endregion

    //endregion

    //region Constructors

    public PauseButton(boolean visible,
                       String name,
                       String caption,
                       Vector2 location,
                       Size size)
    {
        super(visible,
              name,
              caption,
              location);

        this.myInitialize(size);
    }

    //endregion

    //region Methods

    //region Public

    public void vibrate()
    {
        //DeviceFunctions.getInstance().getmVibrator().vibrate(75);


        DeviceFunctions.getInstance().cancelVibrations();
        DeviceFunctions.getInstance().vibrate(VIBRATE_PATTERN, VIBRATIONS_REPEAT);

    }

    //region IGameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        Canvas canvas = drawInformation.getmCanvas();


        if(this.ismVisible())
        {
            canvas.drawBitmap((this.mToggled ?
                                   this.mPlayImage.getmBitmap() :
                                   this.mPauseImage.getmBitmap()),
                              null,
                              this.mRect,
                              null);

        }
    }

    public void updateMe(UpdateInformation updateInformation)
    {

        if(this.mRect.contains((int)(updateInformation.getmGameViewState().getAt().getM_x()),
                               (int)(updateInformation.getmGameViewState().getAt().getM_Y())) &&
            updateInformation.getmGameViewState().isM_IsDown())
        {
            if(mBeenUp)
            {
                this.mToggled = !this.mToggled;
                updateInformation.setmIsPaused(this.mToggled);
                this.listener.onClick();
            }

            //this.mPressed = true;
            this.mBeenUp = false;
            updateInformation.getmGameViewState().setM_IsDown(false);
        }
        else
        {
            this.mBeenUp = true;
            //this.mPressed = false;
        }
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize(Size size)
    {

        this.mSize = size;
        //                                                                                 Performance
        this.mRect = new Rect((int)(this.getmLocation().getM_x() - this.mSize.getWidth() * 0.5f),
        //                                                                                 Performance
                              (int)(this.getmLocation().getM_Y() - this.mSize.getHeight() * 0.5f),
        //                                                                                 Performance
                              (int)(this.getmLocation().getM_x() + this.mSize.getWidth() * 0.5f),
        //                                                                                  Performance
                              (int)(this.getmLocation().getM_Y() + this.mSize.getHeight() * 0.5f));


        //this.mPressed = false;
        this.mBeenUp = true;

        this.mToggled = false;

        this.mPlayImage = ResourceHolderImages.getInstance().getResizedBitmap(ResourceHolderImages.getInstance().getmPlayImage(), this.mSize);
        this.mPauseImage = ResourceHolderImages.getInstance().getResizedBitmap(ResourceHolderImages.getInstance().getmPauseImage(), this.mSize);

    }

    //endregion

    //endregion

}
