package com.example.nismrg.breakoutsample.GameObjects;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.example.nismrg.breakoutsample.DeviceFunctions.DeviceFunctions;
import com.example.nismrg.breakoutsample.DeviceFunctions.VibrationEnums;
import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.GameViewState.GameViewState;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.Balls.LaserBall;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector1;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by nismrg on 2016-06-07.
 */
public class Paddle extends GameObject
{

    //region Constants

    //region Private

    //region Vibrations

    private final long[] VIBRATE_PATTERN = new long[]{ 0, VibrationEnums.LENGTH_INFO };
    private final int VIBRATIONS_REPEAT = -1;

    //endregion

    private final int AREA_UNITS_A_SECOND = 800;
    // Life bar UI Behaviour.
    private final int BALLS_LEFT_BALL_PADDING = 3;

    private final float DEGREES_ROTATION_MARGIN = 9;
    private final int TRAINING_LENGTH = 50;
    private final int TRAINING_EXTREME_POINT_REMOVAL_SIZE = 10;
    private final int PADDLE_ACCESS_HEIGHT = 200;

    private final int WITHIN_PADDLE_MISS_ACCEPTANCE = 5;

    private final float IMAGE_REVELATIONS_A_SECOND = 0.5f;

    //Observe not final to be scalable
    private int IMAGE_BLEED_LEFT = 0;
    private int IMAGE_BLEED_RIGHT = 0;

    private final int CROSS_HAIR_AXIS_LENGTH = 8;
    private final int CROSS_HAIR_WIDTH = 4;
    private final int CROSS_HAIR_Y_TRANSLATION = 5;

    //endregion

    //endregion

    //region Variables

    //region Private

    private boolean m_PressedDirectly;
    private boolean mBeenUp;
    private boolean mLeftPaddle;
    private boolean mBeganOutsidePaddle;
    private boolean mLastWasUp;

    private boolean m_ReadyBall;
    private World m_World;
    private Vector2 m_Location;
    private Vector2 m_Size;
    private RectF myIntersection;
    private ArrayList<BallBase> m_CurrentBallBases;
    private float m_PaddleSharpness;

    private Float mPaddleZeroPoint = null;
    private ArrayList<Float> mTrainingSet;
    private boolean mLaunchMade;

    private float mRotation;

    private boolean m_LastLaunchIsLaserBall;

    private Paint mLaserPaint;
    private Paint mLaserGuidePaint;
    private Paint mCrossHairPaint;

    private float m_ImageScrollPosition;
    private BitMapAndName m_MyImage;
    private int m_HalfImageWidth;

    //endregion

    //endregion

    //region Encapsulation

    public ArrayList<BallBase> getM_CurrentBallBases()
    {
        return m_CurrentBallBases;
    }

    public void setM_CurrentBallBases(ArrayList<BallBase> m_CurrentBallBases)
    {
        this.m_CurrentBallBases = m_CurrentBallBases;
    }

    public boolean isM_ReadyBall()
    {
        return m_ReadyBall;
    }

    public void setM_ReadyBall(boolean m_ReadyBall)
    {
        this.m_ReadyBall = m_ReadyBall;
    }

    public Vector2 getM_Size()
    {
        return m_Size;
    }

    public void setM_Size(Vector2 m_Size)
    {
        this.m_Size = m_Size;
    }

    public RectF getMyIntersection()
    {
        return myIntersection;
    }

    public void setMyIntersection(RectF myIntersection) {
        this.myIntersection = myIntersection;
    }

    public float getM_PaddleSharpness() {
        return m_PaddleSharpness;
    }

    public void setM_PaddleSharpness(float m_PaddleSharpness) {
        this.m_PaddleSharpness = m_PaddleSharpness;
    }

    public boolean isM_PressedDirectly() {
        return m_PressedDirectly;
    }

    public void setM_PressedDirectly(boolean m_PressedDirectly) {
        this.m_PressedDirectly = m_PressedDirectly;
    }

    public Vector2 getM_Location() {
        return m_Location;
    }

    public void setM_Location(Vector2 m_Location) {
        this.m_Location = m_Location;
    }

    public boolean isM_LastLaunchIsLaserBall() {
        return m_LastLaunchIsLaserBall;
    }

    public void setM_LastLaunchIsLaserBall(boolean m_LastLaunchIsLaserBall) {
        this.m_LastLaunchIsLaserBall = m_LastLaunchIsLaserBall;
    }

    //endregion

    //region constructors

    public Paddle(World world,
                  Vector2 location,
                  Vector2 size,
                  float paddleSharpness)
    {

        super();

        this.myInitialize(world,
                          location,
                          size,
                          paddleSharpness);


    }

    //endregion

    //region Methods

    //region Public

    public void vibrate()
    {
        DeviceFunctions.getInstance().cancelVibrations();
        DeviceFunctions.getInstance().vibrate(this.VIBRATE_PATTERN, this.VIBRATIONS_REPEAT);
    }

    public float getReflectionAngle(Vector2 at)
    {
        float halfPaddleWidth = this.getM_Size().getM_x() / 2;
        float dx = at.getM_x() - this.getM_Location().getM_x();

        Vector1 pointingInX = new Vector1(dx);
        pointingInX = pointingInX.clampToLength(halfPaddleWidth);

        float factor =  pointingInX.getM_x() / halfPaddleWidth;

        float angle = 90 * factor;

        return  angle;

    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        Canvas canvas = drawInformation.getmCanvas();

        super.drawMe(drawInformation);

        int xAt = Math.round(this.m_HalfImageWidth * this.m_ImageScrollPosition);

        //region Draw image

        canvas.drawBitmap(this.m_MyImage.getmBitmap(),
                          new Rect(xAt + this.IMAGE_BLEED_LEFT,
                                   1,
                                   this.m_HalfImageWidth + xAt,
                                   this.m_MyImage.getmBitmap().getHeight()),
                          this.getMyIntersection(),
                         null);

        //endregion

        //region Draw cross hair

        canvas.drawLine(this.getMyIntersection().centerX() - this.CROSS_HAIR_AXIS_LENGTH,
                        this.getMyIntersection().centerY() + this.CROSS_HAIR_Y_TRANSLATION,
                        this.getMyIntersection().centerX() + this.CROSS_HAIR_AXIS_LENGTH,
                        this.getMyIntersection().centerY() + this.CROSS_HAIR_Y_TRANSLATION,
                        this.mCrossHairPaint);

        canvas.drawLine(this.getMyIntersection().centerX(),
                        this.getMyIntersection().centerY() - this.CROSS_HAIR_AXIS_LENGTH + this.CROSS_HAIR_Y_TRANSLATION,
                        this.getMyIntersection().centerX(),
                        this.getMyIntersection().centerY() + this.CROSS_HAIR_AXIS_LENGTH + this.CROSS_HAIR_Y_TRANSLATION,
                        this.mCrossHairPaint);

        //endregion

        //region Special laser ball handling

        if(this.isM_LastLaunchIsLaserBall())
        {
            canvas.drawLine(this.getMyIntersection().left,
                            this.getMyIntersection().top,
                            this.getMyIntersection().left,
                            this.getMyIntersection().top -
                            this.m_World.getM_Size().getHeight() * LaserBall.LASER_HEIGHT_TO_SCREEN_RATIO,
                            this.getLaserPaint());
            canvas.drawLine(this.getMyIntersection().right,
                            this.getMyIntersection().top,
                            this.getMyIntersection().right,
                            this.getMyIntersection().top -
                            this.m_World.getM_Size().getHeight() * LaserBall.LASER_HEIGHT_TO_SCREEN_RATIO,
                            this.getLaserPaint());

            //region Handle angel

            ArrayList<BallBase> ballsInWorld = this.m_World.getM_Balls().getM_Objects();

            for(BallBase ballBase : ballsInWorld)
            {
                if(ballBase.getClass() == LaserBall.class &&
                   ballBase.getM_Location().getM_x() >= this.getMyIntersection().left &&
                   ballBase.getM_Location().getM_x() <= this.getMyIntersection().right)
                {


                    /*
                    float yLaserGuide = this.m_World.getM_Size().getHeight() -
                            this.m_World.getM_Size().getHeight() *
                                    LaserBall.LASER_HEIGHT_TO_SCREEN_RATIO_ANGLE_INDICATOR;
                      */

                    Vector2 laserGuide =
                            new Vector2(0,
                                        LaserBall.LASER_HEIGHT_TO_SCREEN_RATIO_ANGLE_INDICATOR * this.m_World.getM_Size().getHeight());
                    float rotation = this.getReflectionAngle(ballBase.getM_Location()) * this.getM_PaddleSharpness();

                    laserGuide.rotateAmongZAxis(rotation);

                    canvas.drawLine(this.getM_Location().getM_x(),
                                    this.getMyIntersection().top,

                            this.getM_Location().getM_x() - laserGuide.getM_x(),
                            this.getMyIntersection().top -        laserGuide.getM_Y(),
                                    this.getLaserGuidePaint());

                    break;
                }
            }

            //endregion

        }

        //endregion

        if(this.getM_CurrentBallBases().size() > 0 &&
           this.isM_ReadyBall())
        {
            BallBase firstBallBase = this.getM_CurrentBallBases().get(0);
            firstBallBase.drawMeAt(drawInformation,
                               this.getLaunchLocation());
        }


        //region Draw balls left

        this.drawBallsLeft(drawInformation);

        //endregion


/*

        Paint ppp = new Paint();
        ppp.setColor(Color.MAGENTA);
        ppp.setTextSize(75);

        canvas.drawText("AngleA" +
                (this.mPaddleZeroPoint != null ? this.mPaddleZeroPoint.floatValue() : 0) , 500, 400, ppp);

        canvas.drawText("AngleD" +
                this.mRotation , 500, 500, ppp);

        canvas.drawText("Been up " + (this.mBeenUp ? "1" : "0"), 500, 600, ppp );
        canvas.drawText("Began outside " + (this.mBeganOutsidePaddle ? "1" : "0"), 500, 700, ppp );
        canvas.drawText("Left paddle " + (this.mLeftPaddle ? "1" : "0"), 500, 800, ppp );
        canvas.drawText("Last was up " + (this.mLastWasUp ? "1" : "0"), 500, 900, ppp );
*/


    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        GameViewState gameViewState = updateInformation.getmGameViewState();
        int atFps = updateInformation.getmAtFps();

        if(updateInformation.ismIsPaused())
        {

            //region Paused

            this.handleAnimationRotation(false, atFps);

            //endregion
        }
        else
        {

            //region Not paused

            super.updateMe(updateInformation);

            boolean isWithinPaddle = this.isWithinPaddle(gameViewState);

            this.mRotation = gameViewState.getmZAxisRotation();

            if (!gameViewState.isM_IsDown() &&
                    !isWithinPaddle) {
                this.setM_PressedDirectly(false);
            }

            boolean isRight = !(gameViewState.getAt().getM_x() < this.getM_Location().getM_x());
            boolean isLeft = gameViewState.getAt().getM_x() < this.getM_Location().getM_x();

            if (gameViewState.isM_IsDown()) {

                //region Is down

                if (this.mLastWasUp) {
                    this.mBeganOutsidePaddle = true;

                    if (isWithinPaddle) {
                        this.mBeganOutsidePaddle = false;
                    }
                }

                if (/*this.mBeenUp &&*/ !isWithinPaddle) {
                    this.mBeganOutsidePaddle = true;
                    if (!this.mBeenUp) {
                        this.mLeftPaddle = true;
                    }
                } else {


                }

                this.mLastWasUp = false;

                if (isWithinPaddle /*&& !this.isM_PressedDirectly()*/) {
                    this.setM_PressedDirectly(true);
                    this.mLeftPaddle = false;
                } else {
                    this.setM_PressedDirectly(false);
                    this.mLeftPaddle = true;
                }

                if (isWithinPaddle) {

                    //region In paddle

                    // Try launch

                    if (this.m_CurrentBallBases.size() > 0) {
                        BallBase nextBallBase = this.m_CurrentBallBases.get(0);

                        if (this.m_World.launchBall(nextBallBase,
                                new Vector2(0, -nextBallBase.getStartingSpeed()),
                                this.getLaunchLocation())) {
                            this.handleLaserBallLaunch(nextBallBase);
                            this.registerLaunchMade(gameViewState);
                            this.m_CurrentBallBases.remove(0);
                        }
                    }

                    if (!this.mBeganOutsidePaddle) {

                        if (this.isM_PressedDirectly()) {
                            this.setM_Location(new Vector2(gameViewState.getAt().getM_x(), this.getM_Location().getM_Y()));
                        }
                    }

                    //endregion

                } else {

                    //region Not in paddle

                    if (isLeft) {
                        this.goLeft(atFps);
                    }
                    if (isRight) {
                        this.goRight(atFps);
                    }
                    this.setM_PressedDirectly(false);

                    //endregion

                }

                //region Animate warning bar on move only

                this.handleAnimationRotation(isLeft, atFps);

                //endregion

                this.mBeenUp = false;

                //endregion

            } else {

                this.mBeenUp = true;
                this.mLeftPaddle = true;
                this.mBeganOutsidePaddle = true;
                this.mLastWasUp = true;

                //region Is not down

                if (this.mPaddleZeroPoint != null) {

                    if (this.mRotation > (this.mPaddleZeroPoint.floatValue() + DEGREES_ROTATION_MARGIN)) {
                        this.goRight(atFps);
                        isRight = true;
                        isLeft = false;
                    }
                    if (this.mRotation < (this.mPaddleZeroPoint.floatValue() - DEGREES_ROTATION_MARGIN)) {
                        this.goLeft(atFps);
                        isRight = false;
                        isLeft = true;
                    }
                }

                //endregion

            }

            this.keepOnScreen(isRight, isLeft);

            this.refreshIntersection();

            this.handleTraining(this.mRotation);

            //endregion

        }
    }

    @Override
    public int getM_AtFrameFragmentsFrame() {
        return super.getM_AtFrameFragmentsFrame();
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize(World world,
                              Vector2 location,
                              Vector2 size,
                              float paddleSharpness)
    {
        this.setM_PaddleSharpness(paddleSharpness);
        this.m_World = world;
        this.setM_Location(location);
        this.setM_Size(size);

        this.refreshIntersection();

        this.m_CurrentBallBases = world.getmWorldDescriptor().getmBallBaseSetUp();
        this.setM_ReadyBall(false);

        this.mLaunchMade = false;
        this.setM_LastLaunchIsLaserBall(false);
        this.mBeenUp = true;
        this.mLeftPaddle = true;
        this.mBeganOutsidePaddle = true;
        this.mLastWasUp = true;

        this.m_MyImage = ResourceHolderImages.getInstance().getmPaddleWithTop();

        this.IMAGE_BLEED_LEFT = this.IMAGE_BLEED_LEFT * Math.round(1f - this.m_MyImage.getmBitmap().getWidth() / this.getMyIntersection().width());
        this.IMAGE_BLEED_RIGHT = this.IMAGE_BLEED_RIGHT * Math.round(1f - this.m_MyImage.getmBitmap().getWidth() / this.getMyIntersection().width());

        this.m_HalfImageWidth = Math.round(this.m_MyImage.getmBitmap().getWidth() * 0.25f);

        this.mCrossHairPaint = new Paint();
        this.mCrossHairPaint.setColor(ResourceHolderImages.getInstance().getmPaddleCrossHairColor());
        this.mCrossHairPaint.setStrokeWidth(this.CROSS_HAIR_WIDTH);

    }

    //region Laser guide paints

    private Paint getLaserPaint()
    {
        if(this.mLaserPaint == null)
        {
            this.mLaserPaint = new Paint();
            this.mLaserPaint.setColor(LaserBall.LASER_COLOR);
            this.mLaserPaint.setStyle(Paint.Style.FILL_AND_STROKE);
            this.mLaserPaint.setAntiAlias(true);
        }

        return this.mLaserPaint;
    }

    private Paint getLaserGuidePaint()
    {
        if(this.mLaserGuidePaint == null)
        {
            this.mLaserGuidePaint = new Paint();
            this.mLaserGuidePaint.setColor(LaserBall.LASER_GUIDE_COLOR);
            this.mLaserGuidePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        }

        return this.mLaserGuidePaint;
    }

    //endregion

    private void goRight(int atFps)
    {

        this.getM_Location().addVector(new Vector2(this.AREA_UNITS_A_SECOND * (1f / atFps), 0));

    }

    private void goLeft(int atFps)
    {
        this.getM_Location().addVector(new Vector2(-(this.AREA_UNITS_A_SECOND * (1f / atFps)), 0));
    }

    private void handleTraining(float currentRotation)
    {
        if(this.mLaunchMade &&
           this.mPaddleZeroPoint == null)
        {
            if(this.mTrainingSet == null)
            {
                this.mTrainingSet = new ArrayList<>();
            }
            if(this.mTrainingSet.size() < TRAINING_LENGTH)
            {
                this.mTrainingSet.add(currentRotation);
            }
            else
            {

                Collections.sort(this.mTrainingSet);
                float sum = 0;

                if(TRAINING_LENGTH > (2 * TRAINING_EXTREME_POINT_REMOVAL_SIZE))
                {
                    for(int i = TRAINING_EXTREME_POINT_REMOVAL_SIZE;
                        i < this.mTrainingSet.size() - TRAINING_EXTREME_POINT_REMOVAL_SIZE;
                        i++)
                    {
                        sum += this.mTrainingSet.get(i);
                    }

                    this.mPaddleZeroPoint = sum / (this.mTrainingSet.size() - (2 * TRAINING_EXTREME_POINT_REMOVAL_SIZE));
                }
                else
                {
                    for(int i = 0;
                        i < this.mTrainingSet.size();
                        i++)
                    {
                        sum += this.mTrainingSet.get(i);
                    }

                    this.mPaddleZeroPoint = sum / this.mTrainingSet.size();
                }
            }
        }
    }

    private void registerLaunchMade(GameViewState gameViewState)
    {
        this.mLaunchMade = true;
    }

    private void keepOnScreen(boolean isRight, boolean isLeft)
    {
        if(this.getMyIntersection().right >= this.m_World.getM_Size().getWidth() && isRight) {
            this.getM_Location().setM_x(this.m_World.getM_Size().getWidth() - (this.getM_Size().getM_x() / 2f));
        }
        if(this.getMyIntersection().left <= 1 && isLeft)
        {
            this.getM_Location().setM_x(this.getM_Size().getM_x() / 2f);
        }
    }

    //USE: BALLS_LEFT_BALL_PADDING
    private void drawBallsLeft(DrawInformation drawInformation)
    {



        float maxBallRadius = 0;

        for(BallBase ballBase : this.m_CurrentBallBases)
        {
            if(ballBase.getM_Radius() > maxBallRadius)
            {
                maxBallRadius = ballBase.getM_Radius();
            }
        }

        // Handle what ball is feed to brick!

       Vector1 xLocation = new Vector1(0f);
        BallBase iBallBase;

        for(int i = this.m_CurrentBallBases.size() - 1;
            i > (this.isM_ReadyBall() ? 0 : -1);
            i--)
        {

            xLocation.addVector(new Vector1(BALLS_LEFT_BALL_PADDING + this.m_CurrentBallBases.get(i).getM_Radius()));

            iBallBase =this.m_CurrentBallBases.get(i);

            iBallBase.drawMeAt(drawInformation,
                                                new Vector2(xLocation.getM_x(), drawInformation.getmCanvas().getHeight() - maxBallRadius - BALLS_LEFT_BALL_PADDING));

            xLocation.addVector(new Vector1(BALLS_LEFT_BALL_PADDING + this.m_CurrentBallBases.get(i).getM_Radius()));

        }
    }

    private Vector2 getLaunchLocation()
    {
        return this.getM_Location().nonMatrixTranslate(0f,
                                                  -(this.getM_Size().getM_Y() * 0.5f) -
                                                  this.m_CurrentBallBases.get(0).getM_Radius());
    }

    private void refreshIntersection()
    {
        this.setMyIntersection(new RectF(this.getM_Location().getM_x() - (this.getM_Size().getM_x()  * 0.5f),
                this.getM_Location().getM_Y() - (this.getM_Size().getM_Y() * 0.5f),
                this.getM_Location().getM_x() + (this.getM_Size().getM_x() * 0.5f),
                this.getM_Location().getM_Y() + (this.getM_Size().getM_Y() * 0.5f)));
    }

    public void handleLaserBallLaunch(BallBase ballBase)
    {
        if(ballBase.getClass() == LaserBall.class)
        {
            this.setM_LastLaunchIsLaserBall(true);
        }
        else
        {
            this.setM_LastLaunchIsLaserBall(false);
        }
    }

    /*
    private void initializeBalls(int numberOfBalls)
    {
        this.setM_CurrentBallBases(new ArrayList<BallBase>());

        for(int i = 0; i < numberOfBalls; i++)
        {
            this.getM_CurrentBallBases().add(new BasicBall(this.m_World));
        }
    }
    */

    private boolean isWithinPaddle(GameViewState gameViewState)
    {
        boolean isWithinPaddle =
            //                                                                                           Performance
            gameViewState.getAt().getM_x() > this.getM_Location().getM_x() - (this.getM_Size().getM_x() * 0.5)- this.WITHIN_PADDLE_MISS_ACCEPTANCE &&
            //                                                                                           Performance
            gameViewState.getAt().getM_x() < this.getM_Location().getM_x() + (this.getM_Size().getM_x() * 0.5) + this.WITHIN_PADDLE_MISS_ACCEPTANCE &&
            gameViewState.getAt().getM_Y() > this.m_World.getM_Size().getHeight() - PADDLE_ACCESS_HEIGHT; // Need to be quite close

        return isWithinPaddle;
    }

    private void handleAnimationRotation(boolean isLeft, int atFps)
    {
        //region Animate warning bar on move only

        this.m_ImageScrollPosition +=
                (isLeft ?
                        this.IMAGE_REVELATIONS_A_SECOND :
                        this.IMAGE_REVELATIONS_A_SECOND) /
                        atFps;

        if(this.m_ImageScrollPosition > 1)
        {
            this.m_ImageScrollPosition -= 1;
        }

        //endregion

    }

    //endregion

    //endregion

}
