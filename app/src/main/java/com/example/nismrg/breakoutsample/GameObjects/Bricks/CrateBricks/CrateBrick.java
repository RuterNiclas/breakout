package com.example.nismrg.breakoutsample.GameObjects.Bricks.CrateBricks;

import android.graphics.Canvas;
import android.util.Size;

import com.example.nismrg.breakoutsample.DeviceFunctions.VibrationEnums;
import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickBase;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.BrickLocationAndSize;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.IHitEffect;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.PaintParticleGravitron;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.PaintParticleGravitronSystem;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.ParticleSystemBase;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.BitMapAndName;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-07-11.
 */


public class CrateBrick extends BrickBase implements IHitEffect
{

    //region Constants

    //region Private

    //region Vibrations

    private static final boolean VIBRATES_ON_HIT = true;
    private static final long[] VIBRATE_PATTERN_ON_HIT = new long[]{ 0, VibrationEnums.LENGTH_SMALLEST_POSSIBLE };
    private static final int VIBRATIONS_REPEAT_ON_HIT = -1;

    //endregion

    private final int HIT_POINTS = 1;
    private final float AVERAGE_NOVA_SPEED_PIXELS_A_SECOND = 2250;
    private final float NORMAL_DISTRIBUTION_NOVA_SPEED_PIXELS_A_SECOND = 100;
    private final int NOVA_LIFE_TIME_IN_MILLI_SECONDS = 50000;
    private final float DIMINISH_DISTANCE_IN_PIXELS = 9000;

    private final int AVERAGE_PARTICLE_DIMENSION_PX = 2;
    private final int NORMAL_DISTRIBUTION_PARTICLE_DIMENSION_POSITIVE_PX = 1;
    // If the crate consists of 1000 pixles this will make 200 objects
    private final int PARTICLE_SKIP_FACTOR = 3;
    private final int PARTICLE_SKIP_FACTOR_PADDING = 1;

    private final int AVERAGE_EJECTION_SPEED_PX_A_SECoNF = 10;
    private final int EJECTION_STANDARD_DEVIATION_PX_A_SECOND = 5;

    private BitMapAndName mScaledImage;

    //endregion

    //endregion

    //region Variables

    //region Private

    //Explosion results
    private ArrayList<ParticleSystemBase> mPreLoadedParticles;
    private ArrayList<ParticleSystemBase> mNewParticles;

    //endregion

    //endregion

    //region Encapsulation

    public ArrayList<ParticleSystemBase> getmNewParticles()
    {
        return mNewParticles;
    }

    public void setmNewParticles(ArrayList<ParticleSystemBase> mNewParticles)
    {
        this.mNewParticles = mNewParticles;
    }

    public ArrayList<ParticleSystemBase> getmPreLoadedParticles()
    {
        return mPreLoadedParticles;
    }

    public void setmPreLoadedParticles(ArrayList<ParticleSystemBase> mPreLoadedParticles)
    {
        this.mPreLoadedParticles = mPreLoadedParticles;
    }

    //endregion

    //region Constructors

    public CrateBrick(World world, BrickLocationAndSize brickLocationAndSize)
    {
        super(world,
              brickLocationAndSize,
              VIBRATES_ON_HIT,
              VIBRATE_PATTERN_ON_HIT,
              VIBRATIONS_REPEAT_ON_HIT);

        this.initializeMe();

    }

    //endregion

    //region Methods

    //region Public

    @Override
    public void takeDamage(UpdateInformation updateInformation)
    {
        super.takeDamage(updateInformation);

        this.makeEffectOfTakeDamage();
    }

    //region IHitEffect

    public Collection<BallBase> pullAllNewBalls()
    {
        return new ArrayList<>();
    }

    public Collection<ParticleSystemBase> pullAllNewParticleSystems()
    {
        Collection<ParticleSystemBase> particleBases = this.getmNewParticles();
        this.setmNewParticles(new ArrayList<ParticleSystemBase>());

        return particleBases;
    }

    //endregion

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation) {

        Canvas canvas = drawInformation.getmCanvas();

        super.drawMe(drawInformation);

        canvas.drawBitmap(this.mScaledImage.getmBitmap(),null, super.getM_DrawingRectangle(), null);

    }

    //endregion

    //endregion

    //region Private

    private void initializeMe()
    {
        super.setM_HitPointsLeft(HIT_POINTS);

        this.mScaledImage = super.scaleBitmapToBrickSize(ResourceHolderImages.getInstance().getmCrate()/*, false*/);

        this.setmNewParticles(new ArrayList<ParticleSystemBase>());
        this.setmPreLoadedParticles(new ArrayList<ParticleSystemBase>());

        this.preLoadParticles();

    }

    private void preLoadParticles()
    {

        PaintParticleGravitronSystem paintParticleGravitronSystem = new PaintParticleGravitronSystem();

        /*

        Vector2 location = new Vector2(super.getM_DrawingRectangle().centerX(), super.getM_DrawingRectangle().centerY());
        Vector2 motionSecond = new Vector2(0f, 0f);
        float speed = (float)((RandomProvider.getInstance().nextGaussian() * NORMAL_DISTRIBUTION_NOVA_SPEED_PIXELS_A_SECOND) + AVERAGE_NOVA_SPEED_PIXELS_A_SECOND);

        CrateNova crateNova = new CrateNova(location,
                                            motionSecond,
                                            NOVA_LIFE_TIME_IN_MILLI_SECONDS,
                                            speed,
                                            DIMINISH_DISTANCE_IN_PIXELS);

        this.getmNewParticles().add(crateNova);
        */

        int yToAdd;
        int xToAdd;
        int currentSizeX;
        int currentSizeY;

        List<PaintParticleGravitron> rowGravitrons;
        List<Integer> yList;
        int ySum;
        PaintParticleGravitron paintParticleGravitron;
        RandomProvider randomProvider = RandomProvider.getInstance();
        ResourceHolderImages resourceHolderImages = ResourceHolderImages.getInstance();
        int colorIndex;


        for(int y = 0; y < super.getM_DrawingRectangle().height(); y += yToAdd)
        {

            rowGravitrons = new ArrayList<>();
            yList = new ArrayList<>();
            ySum = 0;

            for (int x = 0; x < super.getM_DrawingRectangle().width(); x += xToAdd)
            {

                //region Make one

                currentSizeX =
                        (int)((Math.abs(randomProvider.nextGaussian()) *
                                this.NORMAL_DISTRIBUTION_PARTICLE_DIMENSION_POSITIVE_PX +
                                this.AVERAGE_PARTICLE_DIMENSION_PX) *
                                this.PARTICLE_SKIP_FACTOR -
                                this.PARTICLE_SKIP_FACTOR_PADDING);
                currentSizeY =
                        (int)((Math.abs(randomProvider.nextGaussian()) *
                                this.NORMAL_DISTRIBUTION_PARTICLE_DIMENSION_POSITIVE_PX +
                                this.AVERAGE_PARTICLE_DIMENSION_PX) *
                                this.PARTICLE_SKIP_FACTOR-
                                this.PARTICLE_SKIP_FACTOR_PADDING);

                yList.add(currentSizeY + this.PARTICLE_SKIP_FACTOR_PADDING);
                xToAdd = currentSizeX + this.PARTICLE_SKIP_FACTOR_PADDING;

                paintParticleGravitron = CrateBrickParticleEngine.getInstance().pullSingleParticle();

                paintParticleGravitron.setmOwnedBy(paintParticleGravitronSystem);

                paintParticleGravitron.setmSize(new Size(currentSizeX, currentSizeY));
                paintParticleGravitron.setmLocation(new Vector2(super.getM_DrawingRectangle().left + x,
                        super.getM_DrawingRectangle().top + y));
                colorIndex = randomProvider.nextInt(resourceHolderImages.getmCrateImagePixels().size());
                paintParticleGravitron.setmColor(resourceHolderImages.getmCrateImagePixels().get(colorIndex));

                Vector2 motionSecond =
                        new Vector2(paintParticleGravitron.getmLocation().getM_x() - super.getM_DrawingRectangle().centerX(),
                                paintParticleGravitron.getmLocation().getM_Y() - super.getM_DrawingRectangle().centerY());
                motionSecond.scaleToLength((float)(randomProvider.nextGaussian() *
                        this.EJECTION_STANDARD_DEVIATION_PX_A_SECOND +
                        this.AVERAGE_EJECTION_SPEED_PX_A_SECoNF));

                paintParticleGravitron.setmMotionSecond(motionSecond);

                rowGravitrons.add(paintParticleGravitron);

                //endregion

            }

            for(int i = 0; i < yList.size(); i++)
            {
                ySum += yList.get(i).intValue();
            }

            yToAdd = (ySum / rowGravitrons.size());

            paintParticleGravitronSystem.mParticles.addAll(rowGravitrons);

        }

        this.getmPreLoadedParticles().add(paintParticleGravitronSystem);

    }

    private void makeEffectOfTakeDamage()
    {
        this.getmNewParticles().addAll(this.getmPreLoadedParticles());
        this.getmPreLoadedParticles().clear();
    }

    //endregion

    //endregion

}
