package com.example.nismrg.breakoutsample.GameObjects.ParticleEffects;

import android.graphics.Paint;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.Math.Vector2;

/**
 * Created by nismrg on 2016-11-03.
 */
public class PaintParticleLine2DSimple extends ParticleBase
{

    //region Variables

    //region Public

    public int mParticleLength;
    public int mColor;
    public Paint mPaint;
    public Vector2 mNormalizedDirection;

    //endregion

    //endregion

    //region Constructors

    public PaintParticleLine2DSimple(Vector2 location,
                                     Vector2 motionSecond,
                                     int millisecondsToDeath,
                                     int particleLength,
                                     int color,
                                     int lineWidth)
    {
        super(location,
              motionSecond,
              millisecondsToDeath);

        this.myInitialize(particleLength,
                          color,
                          lineWidth);
    }

    //endregion

    //region Methods

    //region Public

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        super.drawMe(drawInformation);

        float x = super.getmLocation().getM_x();
        float y = super.getmLocation().getM_Y();

        drawInformation.getmCanvas().drawLine(x,
                                              y,
                                              x + (this.mNormalizedDirection.getM_x() * this.mParticleLength),
                                              y + (this.mNormalizedDirection.getM_Y() * this.mParticleLength),
                                              this.mPaint);

    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        super.updateMe(updateInformation);

    }

    //endregion

    //endregion

    //region Private

    private void myInitialize(int particleLength,
                              int color,
                              int lineWidth)
    {
        this.mColor = color;
        this.mParticleLength = particleLength;
        this.mPaint = new Paint();
        this.mPaint.setColor(this.mColor);
        this.mPaint.setStrokeWidth(lineWidth);
        this.mNormalizedDirection = this.getmMotionSecond().deepClone();
        this.mNormalizedDirection.scaleToLength(1f);
    }

    //endregion

    //endregion

}
