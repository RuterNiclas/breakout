package com.example.nismrg.breakoutsample.GameObjects.Controls;

import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;

/**
 * Created by nismrg on 2016-07-15.
 */
public class GameButtonOnClickListener //implements View.OnClickListener
{

    //region Variables

    //region Private

    private World mWorld;

    //endregion

    //endregion

    //region Encapsulation

    public World getmWorld() {
        return mWorld;
    }

    public void setmWorld(World mWorld) {
        this.mWorld = mWorld;
    }

    //endregion

    //region Constructors

    public GameButtonOnClickListener(World world)
    {
        this.setmWorld(world);
    }

    //endregion

    //region Methods

    //region Public

    //region View.OnClickListener

   // @Override
    public void onClick() {

    }

    //endregion

    //endregion

    //endregion

}
