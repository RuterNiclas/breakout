package com.example.nismrg.breakoutsample.GameLogic;

/**
 * Created by nismrg on 2016-05-17.
 */
public interface IGameLogic
{

    //region Methods

    //region Public

    void drawMe(DrawInformation drawInformation);

    void updateMe(UpdateInformation updateInformation);

    int getMaxAreaUnitsASecond();

    //endregion

    //endregion

}
