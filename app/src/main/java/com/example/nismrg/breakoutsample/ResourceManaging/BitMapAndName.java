package com.example.nismrg.breakoutsample.ResourceManaging;

import android.graphics.Bitmap;

/**
 * Created by nismrg on 2016-11-15.
 */
public class BitMapAndName
{

    //region Instance

    //region Variables

    //region Private

    private Bitmap mBitmap;
    private int mId;
    private boolean mIsResizedFromOriginalOrAltered;

    //endregion

    //endregion

    //region Encapsulation

    public Bitmap getmBitmap() {
        return mBitmap;
    }

    public void setmBitmap(Bitmap mBitmap) {
        this.mBitmap = mBitmap;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public boolean ismIsResizedFromOriginalOrAltered() {
        return mIsResizedFromOriginalOrAltered;
    }

    public void setmIsResizedFromOriginalOrAltered(boolean mIsResizedFromOriginalOrAltered) {
        this.mIsResizedFromOriginalOrAltered = mIsResizedFromOriginalOrAltered;
    }

    //endregion

    //region Constructors

    public BitMapAndName(Bitmap bitmap, int id)
    {
        this.setmBitmap(bitmap);
        this.setmId(id);
        this.setmIsResizedFromOriginalOrAltered(false);
    }

    public BitMapAndName(Bitmap bitmap,
                         int id,
                         boolean isOriginalSizeOrAltered)
    {
        this.setmBitmap(bitmap);
        this.setmId(id);
        this.setmIsResizedFromOriginalOrAltered(isOriginalSizeOrAltered);
    }

    //endregion

    //region Methods

    //region Public

    public String getHash()
    {

        String hash =String.format("%2$d%1$C%3$d%c%4$d",
                                   BitMapAndName.HASH_SEPARATOR,
                                   this.getmId(),
                                   this.getmBitmap().getWidth(),
                                   this.getmBitmap().getHeight());

        return hash;

    }

    //endregion

    //endregion

    //endregion

    //region Static

    //region Constants

    //region Protected

    protected static final char HASH_SEPARATOR = '_';

    //endregion

    //endregion

    //region Methods

    //region Public

    public static String getHash(int id,
                                 int width,
                                 int height)
    {

        String hash =String.format("%2$d%1$C%3$d%c%4$d",
                                   BitMapAndName.HASH_SEPARATOR,
                                   id,
                                   width,
                                   height);

        return hash;

    }

    //endregion

    //endregion

    //endregion

}
