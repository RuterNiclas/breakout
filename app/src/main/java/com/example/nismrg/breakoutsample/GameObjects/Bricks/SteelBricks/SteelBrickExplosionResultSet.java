package com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks;

import com.example.nismrg.breakoutsample.GameObjects.Balls.BallBase;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.PaintParticleLineSystem;

import java.util.ArrayList;

/**
 * Created by nismrg on 2016-09-14.
 */
public class SteelBrickExplosionResultSet
{

    //region Variables

    //region Private

    private ArrayList<PaintParticleLineSystem> mNewParticles;
    private ArrayList<BallBase> mNewBallBases;

    //endregion

    //endregion

    //region Encapsulation

    public ArrayList<PaintParticleLineSystem> getmNewParticles()
    {
        return mNewParticles;
    }

    public void setmNewParticles(ArrayList<PaintParticleLineSystem> mNewParticles)
    {
        this.mNewParticles = mNewParticles;
    }

    public ArrayList<BallBase> getmNewBallBases() {
        return mNewBallBases;
    }

    public void setmNewBallBases(ArrayList<BallBase> mNewBallBases) {
        this.mNewBallBases = mNewBallBases;
    }

    //endregion

    //region Constructors

    public SteelBrickExplosionResultSet()
    {
        this.setmNewBallBases(new ArrayList<BallBase>());
        this.setmNewParticles(new ArrayList<PaintParticleLineSystem>());
    }

    public SteelBrickExplosionResultSet(ArrayList<PaintParticleLineSystem> newParticles,
                                        ArrayList<BallBase> newBallBases)
    {
        this.setmNewBallBases(newBallBases);
        this.setmNewParticles(newParticles);
    }

    //endregion

}
