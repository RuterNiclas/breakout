package com.example.nismrg.breakoutsample.Math;

import java.util.ArrayList;

/**
 * Created by nismrg on 2016-05-31.
 */
public class FPSHelper
{

    //region Variables

    //region Private

    private Float mDegreesInRadian = null;

    //endregion

    //endregion

    //region Static

    //region Variables

    //region Private

    private static FPSHelper instance= null;
    private static Object mutex= new Object();

    //endregion

    //endregion

    //region Encapsulation

    public static FPSHelper getInstance()
    {
        if(instance==null){
            synchronized (mutex){
                if(instance==null) instance= new FPSHelper();
            }
        }
        return instance;
    }

    //endregion

    //endregion

    //region Constructors

    private FPSHelper()
    {
    }

    //endregion

    //region Methods

    //region Public

    public ArrayList<Long> getMinFpsBreakDownChunkLast(long currentFps, long minFps)
    {

        if(currentFps == 0)
        {
            currentFps = 1;
        }
        if(minFps == 0)
        {
            minFps = 0;
        }

        ArrayList<Long> brokeDownList = new ArrayList();

        float runsInCurrentFrame = (float)minFps / (float)currentFps;

        float rest = runsInCurrentFrame;
        float currentFragment;

        while(rest >0)
        {
            currentFragment = getMaxOne(rest);
            rest-= currentFragment;

            long fragmentFPSLong = (long)(minFps / currentFragment);

            brokeDownList.add(fragmentFPSLong);
        }

        return brokeDownList;

    }

    public ArrayList<Long> getMinFpsBreakDownEven(long currentFps, long minFps)
    {

        if(currentFps == 0)
        {
            currentFps = 1;
        }
        if(minFps == 0)
        {
            minFps = 0;
        }

        ArrayList<Long> brokeDownList = getMinFpsBreakDownChunkLast(currentFps, minFps);
        ArrayList<Long> brokeDownListToReturn = new ArrayList();

        long longSum = 0;

        for(Long l : brokeDownList)
        {
            longSum += l;
        }

        for(int i = 0; i < brokeDownList.size(); i++)
        {
            brokeDownListToReturn.add(longSum / brokeDownList.size());
        }

        return brokeDownListToReturn;

    }

    public float getDegreesInRadians()
    {
        if(this.mDegreesInRadian == null)
        {
            this.mDegreesInRadian = (float)(360 / (2 * Math.PI));
        }

        return  this.mDegreesInRadian.floatValue();

    }

    //endregion

    //region Private

    private float getMaxOne(float value)
    {
        if(value < 1)
        {
            return value;
        }
        else
        {
            return 1f;
        }
    }

    //endregion

    //endregion

}
