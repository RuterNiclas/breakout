package com.example.nismrg.breakoutsample.GameLogic.GameViewState;

import com.example.nismrg.breakoutsample.Math.Vector2;

import java.io.Serializable;

import Utility.IDeepClone;

/**
 * Created by nismrg on 2016-06-09.
 */
public class GameViewState implements IDeepClone<GameViewState>,
                                      Serializable
{

    //region Variables

    //region Private

    private boolean m_IsDown;
    private InputDirection m_InputDirection;
    private Vector2 at;
    private float mZAxisRotation;

    //endregion

    //endregion

    //region Encapsulation

    public InputDirection getM_InputDirection() {
        return m_InputDirection;
    }

    public void setM_InputDirection(InputDirection m_InputDirection) {
        this.m_InputDirection = m_InputDirection;
    }

    public boolean isM_IsDown() {
        return m_IsDown;
    }

    public void setM_IsDown(boolean m_IsDown) {
        this.m_IsDown = m_IsDown;
    }

    public Vector2 getAt() {
        return at;
    }

    public void setAt(Vector2 at) {
        this.at = at;
    }

    public float getmZAxisRotation() {
        return mZAxisRotation;
    }

    public void setmZAxisRotation(float mZAxisRotation) {
        this.mZAxisRotation = mZAxisRotation;
    }

    //endregion

    //region Constructors

    public GameViewState()
    {
        this.setM_IsDown(false);
        this.setM_InputDirection(InputDirection.LEFT);
        this.setAt(new Vector2(101f, 101f));

    }

    //endregion

    //region Methods

    //region Public

    @Override
    public GameViewState deepClone()
    {

        /* Sample clone

        GameViewState aGameViewState = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(this);
            oos.flush();
            oos.close();
            bos.close();
            byte[] byteData = bos.toByteArray();

            ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
            aGameViewState = (GameViewState) new ObjectInputStream(bais).readObject();

            int i = 2;
        } catch (OptionalDataException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {

        }

        */

        GameViewState newGameViewState = new GameViewState();
        newGameViewState.setM_IsDown(this.isM_IsDown());
        newGameViewState.setM_InputDirection(this.m_InputDirection);
        newGameViewState.setAt(new Vector2(this.getAt().getM_x(), this.getAt().getM_Y()));
        newGameViewState.setmZAxisRotation(this.getmZAxisRotation());

        return  newGameViewState;
    }

    //endregion

    //endregion

}
