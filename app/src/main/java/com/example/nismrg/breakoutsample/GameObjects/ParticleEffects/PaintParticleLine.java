package com.example.nismrg.breakoutsample.GameObjects.ParticleEffects;

import android.graphics.Paint;

import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.Math.Vector3;
import com.example.nismrg.breakoutsample.Math.Vector3NotEncapsulated;

/**
 * Created by nismrg on 2016-07-26.
 */
public class PaintParticleLine extends ParticleBase
{

    //region Constants

    //region Private

    private final float DISTANCE_FROM_CAMERA = 1.5f;

    //endregion

    //endregion

    //region Variables

    //region Private

    private int mColor;
    private float mLength;
    private int mWidth;
    private Paint mPaint;
    private float mSpeedInPixelsSecond;
    private Vector3NotEncapsulated mRotationSpeedInAllAxisDegreesSecond;
    private Vector3NotEncapsulated mRotationInAllAxisDegrees;

    //endregion

    //region Public

    // This object moves in ortogonal 3d environment.
    public Vector3NotEncapsulated mMotionSecond3d;
    public Vector3NotEncapsulated mLocation3d;

    //endregion

    //endregion

    // Due performance requirements don't use properties here they have a high const since communication with these are too frequent.

    //region Encapsulation

    public int getmColor() {
        return mColor;
    }

    public void setmColor(int mColor)
    {
        this.mColor = mColor;

        if(this.mPaint != null)
        //if(this.getmPaint() != null)
        {
            this.mPaint.setColor(mColor);
            //this.getmPaint().setColor(mColor);
        }
    }

    /*


    public float getmLength() {
        return mLength;
    }

    public void setmLength(float mLength)
    {
        this.mLength = mLength;
    }

    public Paint getmPaint() {
        return mPaint;
    }

    public void setmPaint(Paint mPaint) {
        this.mPaint = mPaint;
    }

    public Vector3 getmRotationSpeedInAllAxisDegreesSecond() {
        return mRotationSpeedInAllAxisDegreesSecond;
    }

    public void setmRotationSpeedInAllAxisDegreesSecond(Vector3 mRotationSpeedInAllAxisDegreesSecond) {
        this.mRotationSpeedInAllAxisDegreesSecond = mRotationSpeedInAllAxisDegreesSecond;
    }

    public Vector3 getmRotationInAllAxisDegrees() {
        return mRotationInAllAxisDegrees;
    }

    public void setmRotationInAllAxisDegrees(Vector3 mRotationInAllAxisDegrees) {
        this.mRotationInAllAxisDegrees = mRotationInAllAxisDegrees;
    }

    public int getmWidth() {
        return mWidth;
    }

    public void setmWidth(int mWidth)
    {
        this.mWidth = mWidth;

        if(this.getmPaint() != null)
        {
            this.getmPaint().setStrokeWidth(this.getmWidth());
        }
    }

    public Vector3 getmMotionSecond3d() {
        return mMotionSecond3d;
    }

    public void setmMotionSecond3d(Vector3 mMotionSecond3d) {
        this.mMotionSecond3d = mMotionSecond3d;
    }

    public Vector3 getmLocation3d() {
        return mLocation3d;
    }

    public void setmLocation3d(Vector3 mLocation3d)
    {
        this.mLocation3d = mLocation3d;

    }

   */

    //endregion

    //region Constructors

    public PaintParticleLine(Vector3 location,
                             Vector3 motionDirection,
                             float speedInPixelsSecond,
                             int color,
                             float length,
                             int width,
                             Vector3 rotationSpeedInAllAxisDegreesSecond,
                             Vector3 rotationInAllAxisDegrees,
                             int millisecondsToDeath)
    {

        super(new Vector2(0f, 0f), // 2d motion second
              new Vector2(0f, 0f), // 2d motion second
              millisecondsToDeath);

        this.myInitialize(color,
                          motionDirection.toVector3NotEncapsulated(),
                          length,
                          width,
                          speedInPixelsSecond,
                          rotationSpeedInAllAxisDegreesSecond.toVector3NotEncapsulated(),
                          rotationInAllAxisDegrees.toVector3NotEncapsulated(),
                          location.toVector3NotEncapsulated());

    }

    //endregion

    //region Methods

    //region Public

    public void updateMotionSecondOnlyDirection(Vector3NotEncapsulated direction)
    {

        direction.scaleToLength(this.mSpeedInPixelsSecond);

        this.mMotionSecond3d = direction;
        //this.setmMotionSecond3d(direction);

    }

    public void updateSpeed(float speedInPixelsSecond)
    {
        this.mMotionSecond3d.scaleToLength(speedInPixelsSecond);
        //this.getmMotionSecond3d().scaleToLength(speedInPixelsSecond);
    }

    //region GameObject

    @Override
    public void drawMe(DrawInformation drawInformation)
    {

        super.drawMe(drawInformation);



        Vector3NotEncapsulated normalizedDirection = this.mMotionSecond3d.deepClone();
        //Vector3 normalizedDirection = this.getmMotionSecond3d().deepClone();
        normalizedDirection.normalize();

        normalizedDirection.rotateAmongZAxis(this.mRotationInAllAxisDegrees.mZ);
        //normalizedDirection.rotateAmongZAxis(this.getmRotationInAllAxisDegrees().getmZ());

        // Project by x and z

        // float to double since Math.xxxxxxx usually takes double
        double initialLength = this.mLength;
        //double initialLength = this.getmLength();

        // Do camera projection

        double zPerspectiveScale = this.getCameraProjectionInZ();

        initialLength = initialLength * (zPerspectiveScale);

        initialLength = initialLength * Math.cos(Math.toRadians(this.mRotationInAllAxisDegrees.mX));
        //initialLength = initialLength * Math.cos(Math.toRadians(this.getmRotationInAllAxisDegrees().getmX()));

        initialLength = initialLength * Math.cos(Math.toRadians(this.mRotationInAllAxisDegrees.mY));
        //initialLength = initialLength * Math.cos(Math.toRadians(this.getmRotationInAllAxisDegrees().getmY()));



        //                                 Performance.
        float halfLength =(float)( initialLength * 0.5f);





        //                                                    Performance
        normalizedDirection.scaleToLength(this.mLength * 0.5f);
        //normalizedDirection.scaleToLength(this.getmLength() * 0.5f);

        this.adjustPaintStrokeWidthToYDirectionRotation();

        drawInformation.getmCanvas().drawLine(this.mLocation3d.mX - (normalizedDirection.mX * halfLength),
                                              //this.getmLocation3d().getmX() - (normalizedDirection.getmX() * halfLength),
                                              this.mLocation3d.mY - (normalizedDirection.mY * halfLength),
                                              //this.getmLocation3d().getmY() - (normalizedDirection.getmY() * halfLength),
                                              this.mLocation3d.mX + (normalizedDirection.mX * halfLength),
                                              //this.getmLocation3d().getmX() + (normalizedDirection.getmX() * halfLength),
                                              this.mLocation3d.mY + (normalizedDirection.mY * halfLength),
                                              //this.getmLocation3d().getmY() + (normalizedDirection.getmY() * halfLength),
                                              this.mPaint);
                                              //this.getmPaint());

    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {

        super.updateMe(updateInformation);

        // Need to make this control since  this can be added within a update frame.
        if(updateInformation.getmAtFps() > 0)
        {



            this.mLocation3d.addVector(new Vector3NotEncapsulated(this.mMotionSecond3d.mX * updateInformation.mOneDividedByFPS,
                    this.mMotionSecond3d.mY * updateInformation.mOneDividedByFPS,
                    this.mMotionSecond3d.mZ * updateInformation.mOneDividedByFPS));

            //this.getmLocation3d().addVector(new Vector3(this.getmMotionSecond3d().getmX() * oneDividedByAtFPS,
            //                                            this.getmMotionSecond3d().getmY() * oneDividedByAtFPS,
            //                                            this.getmMotionSecond3d().getmZ() * oneDividedByAtFPS));

            this.mRotationInAllAxisDegrees.addVector(this.mRotationSpeedInAllAxisDegreesSecond.scale(updateInformation.mOneDividedByFPS));
            //this.getmRotationInAllAxisDegrees().addVector(this.getmRotationSpeedInAllAxisDegreesSecond().scale(oneDividedByAtFPS));
        }
    }

    @Override
    public int getM_AtFrameFragmentsFrame() {
        return super.getM_AtFrameFragmentsFrame();
    }

    //endregion

    //endregion

    //region Private

    private void myInitialize(int color,
                              Vector3NotEncapsulated motionDirection,
                              float length,
                              int width,
                              float speedInPixelsSecond,
                              Vector3NotEncapsulated rotationSpeedInAllAxisDegreesSecond,
                              Vector3NotEncapsulated rotationInAllAxisDegrees,
                              Vector3NotEncapsulated location)
    {

        // Must take place before length.
        this.mLocation3d = location;
        //this.setmLocation3d(location);

        this.mColor = color;
        //this.setmColor(color);
        this.mLength = length;
        //this.setmLength(length);
        this.mWidth = width;
        //this.setmWidth(width);

        this.mRotationInAllAxisDegrees = rotationInAllAxisDegrees;
        //this.setmRotationInAllAxisDegrees(rotationInAllAxisDegrees);

        this.mRotationSpeedInAllAxisDegreesSecond = rotationSpeedInAllAxisDegreesSecond;
        //this.setmRotationSpeedInAllAxisDegreesSecond(rotationSpeedInAllAxisDegreesSecond);

        this.mSpeedInPixelsSecond = speedInPixelsSecond;

        //region Paint

        Paint newPaint = new Paint();
        newPaint.setColor(this.getmColor());
        //newPaint.setColor(this.getmColor());
        newPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        newPaint.setAntiAlias(true);
        newPaint.setStrokeWidth(this.mWidth);
        //newPaint.setStrokeWidth(this.getmWidth());
        this.mPaint = newPaint;
        //this.setmPaint(newPaint);

        //endregion

        this.mMotionSecond3d = motionDirection.scale(speedInPixelsSecond);
        //this.setmMotionSecond3d(motionDirection.scale(speedInPixelsSecond));




    }

    private void adjustPaintStrokeWidthToYDirectionRotation()
    {
        double rotationCosinus = Math.cos(Math.toRadians(this.mRotationInAllAxisDegrees.mY));
        int strokeWidth = (int)Math.round(this.mWidth * rotationCosinus);

        this.mPaint.setStrokeWidth(strokeWidth);

    }

    private double getCameraProjectionInZ()
    {


        double y = 1;
        double x = this.DISTANCE_FROM_CAMERA;

        double angle = Math.asin(y / x);

        double factor = Math.tan(angle);




        return  factor;

    }

    //endregion

    //endregion

}

