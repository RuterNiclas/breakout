package com.example.nismrg.breakoutsample.GameObjects.Bricks;

import android.util.Size;

import com.example.nismrg.breakoutsample.Math.Vector2;

/**
 * Created by nismrg on 2016-07-25.
 */
public class BrickLocationAndSize
{

    //region Variables

    //region Private

    private Vector2 mLocation;
    private Size mSize;

    //endregion

    //endregion

    //region Encapsulation

    public Vector2 getmLocation() {
        return mLocation;
    }

    public void setmLocation(Vector2 mLocation) {
        this.mLocation = mLocation;
    }

    public Size getmSize() {
        return mSize;
    }

    public void setmSize(Size mSize) {
        this.mSize = mSize;
    }

    //endregion

    //region Constructors

    public BrickLocationAndSize(Vector2 location, Size size)
    {
        this.setmLocation(location);
        this.setmSize(size);
    }

    //endregion

}
