package com.example.nismrg.breakoutsample;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.example.nismrg.breakoutsample.DeviceFunctions.DeviceFunctions;
import com.example.nismrg.breakoutsample.GameLogic.DrawInformation;
import com.example.nismrg.breakoutsample.GameLogic.GameViewState.GameViewState;
import com.example.nismrg.breakoutsample.GameLogic.GameViewState.InputDirection;
import com.example.nismrg.breakoutsample.GameLogic.IGameLogic;
import com.example.nismrg.breakoutsample.GameLogic.UpdateInformation;
import com.example.nismrg.breakoutsample.GameObjects.Balls.ExplodeBall.ExplosionBrickParticleEngine;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.CrateBricks.CrateBrickParticleEngine;
import com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks.SteelBrickParticleEngine;
import com.example.nismrg.breakoutsample.GameObjects.GameObject;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.World;
import com.example.nismrg.breakoutsample.GameObjects.Worlds.WorldSetupFactory;
import com.example.nismrg.breakoutsample.Math.FPSHelper;
import com.example.nismrg.breakoutsample.Math.Vector2;
import com.example.nismrg.breakoutsample.ResourceManaging.ResourceHolderImages;
import com.example.nismrg.breakoutsample.SensorFusion.OrientationProviders.OrientationProviders.OrientationProvider;

import java.util.ArrayList;

import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-05-17.
 */
public class GameView extends SurfaceView
                      implements Runnable,
                                 IGameLogic,
                                 SurfaceHolder.Callback
{

    // region Constants

    // region Private

    private final long MIN_FPS_INITIAL = 100;
    private final int MOTION_AREA_UNIT_SECOND_DEFAULT_VALUE = 1;

    //endregion

    //endregion

    //region Variables

    //region Private

    private GameViewState m_GameViewState;

    private Thread m_TheGameThread = null;
    private SurfaceHolder m_SurfaceHolder;
    private volatile boolean m_Playing;
    private boolean m_Paused;
    private boolean mInGamePaused;

    private Canvas m_Canvas;
    private Size m_ScreenSize;

    private long m_Fps;

    private long m_CurrentFPSMin = MIN_FPS_INITIAL;

    private long m_TimeThisFrame;

    private ArrayList<GameObject> m_GameObjects;

    private OrientationProvider mOrientationProvider;

    //endregion

    //endregion

    //region encapsulation

    public boolean isM_Paused() {
        return m_Paused;
    }

    public void setM_Paused(boolean m_Paused) {
        this.m_Paused = m_Paused;
    }

    public Size getM_ScreenSize() {
        return m_ScreenSize;
    }

    public void setM_ScreenSize(Size m_ScreenSize) {
        this.m_ScreenSize = m_ScreenSize;
    }

    //endregion

    //region Constructors

    public GameView(Context context)
    {
        super(context);

        this.myInitialize(context,
                          null,
                          -1);
    }

    public GameView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.myInitialize(context,
                          attrs,
                          -1);
    }

    public GameView(Context context,
                    AttributeSet attrs,
                    int defStyleAttr)
    {
        super(context,
              attrs,
              defStyleAttr);

        this.myInitialize(context,
                          attrs,
                          defStyleAttr);
    }

    //endregion

    //region Methods

    //region Public

    public void togglePauseInGame()
    {
        this.mInGamePaused = !this.mInGamePaused;
    }

    public void resume()
    {
        this.m_Playing = true;
        this.setM_Paused(false);
        this.m_TheGameThread = new Thread(this);
        this.m_TheGameThread.start();

        //this.mInGamePaused = false;


    }

    public void pause()
    {
        this.m_Playing = false;
        this.setM_Paused(true);

        try {
            this.m_TheGameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }

        //this.mInGamePaused = false;

    }

    //region IRunnable

    @Override
    public void run()
    {

        this.m_Fps = MIN_FPS_INITIAL;

        long gameTimeRun = System.currentTimeMillis();
        long totalTimeRun = System.currentTimeMillis();
        long originalTime = gameTimeRun;

        while (this.m_Playing)
        {

            long startFrameTime = System.currentTimeMillis();

            if (!isM_Paused())
            {

                ArrayList<Long> fpsSplits = null;

                // This takes too much performance below.
                /*
                this.m_CurrentFPSMin = getMaxAreaUnitsASecond();

                ArrayList<Long> fpsSplits = FPSHelper.getInstance().getMinFpsBreakDownEven(this.m_Fps, this.m_CurrentFPSMin);

                */

                fpsSplits = new ArrayList<>();
                // Do this since there are some really fast balls.
                fpsSplits.add(this.m_Fps * 1);
                //fpsSplits.add(this.m_Fps * 2);

                long now = System.currentTimeMillis();

                if(!this.mInGamePaused) {

                    gameTimeRun += now - gameTimeRun;
                }
                    totalTimeRun += now - totalTimeRun;

                for(long splitFPS : fpsSplits)
                {
                    this.updateGameView((int) splitFPS,
                            fpsSplits.size(),
                            gameTimeRun - originalTime,
                            totalTimeRun - originalTime);

                }


            }

            DrawInformation drawInformation = new DrawInformation(null, gameTimeRun - originalTime, totalTimeRun - originalTime, this.mInGamePaused);
            this.drawMe(drawInformation);


            this.m_TimeThisFrame = System.currentTimeMillis() - startFrameTime;

            if(m_TimeThisFrame > 0)
            {
                this.m_Fps = 1000 / this.m_TimeThisFrame;
            }
        }
    }

    //endregion

    //region IGameLogic

    public void drawMe(DrawInformation drawInformation)
    {
        if(this.m_SurfaceHolder.getSurface().isValid())
        {
            this.m_Canvas = this.m_SurfaceHolder.lockCanvas();



            drawInformation.setmCanvas(this.m_Canvas);

            for (GameObject object : this.m_GameObjects) {
                object.drawMe(drawInformation);
            }

            this.m_SurfaceHolder.unlockCanvasAndPost(this.m_Canvas);

        }
    }

    @Override
    public void updateMe(UpdateInformation updateInformation)
    {
        for (GameObject object: this.m_GameObjects) {
            object.updateMe(updateInformation);
        }
    }

    @Override
    public int getMaxAreaUnitsASecond()
    {
        int motionSecondOneDirectionMax = MOTION_AREA_UNIT_SECOND_DEFAULT_VALUE;
        int gameObjectMotion;

        for(GameObject gameObject: this.m_GameObjects) {
            gameObjectMotion = gameObject.getMaxAreaUnitsASecond();

            if(gameObjectMotion > motionSecondOneDirectionMax)
            {
                motionSecondOneDirectionMax = gameObjectMotion;
            }
        }

        return  motionSecondOneDirectionMax;

    }

    //endregion

    //endregion

    //region private

    private void updateGameView(int atFPS,
                                int atFrameFragmentsFrame,
                                long gameTimeRunMillis,
                                long totalTimeRunMillis)
    {

        this.m_GameViewState.setmZAxisRotation(FPSHelper.getInstance().getDegreesInRadians() *
                this.mOrientationProvider.getEulerAngles().getYaw());
        GameViewState thisFrameState = this.m_GameViewState.deepClone();


        UpdateInformation updateInformation =
                new UpdateInformation(atFPS,
                                      atFrameFragmentsFrame,
                                      thisFrameState,
                                      gameTimeRunMillis,
                                      totalTimeRunMillis,
                                      this.mInGamePaused);

        this.updateMe(updateInformation);
    }

    private void myInitialize(Context context,
                              AttributeSet attrs,
                              int defStyleAttr)
    {

        //region Load singleton cache data

        DeviceFunctions.getInstance().InitializeSingleTon(context);
        RandomProvider.getInstance().initializeSingleTon();
        ResourceHolderImages.getInstance().InitializeSingleTon(context);
        SteelBrickParticleEngine.getInstance().InitializeSingleTon();
        CrateBrickParticleEngine.getInstance().InitializeSingleTon();
        ExplosionBrickParticleEngine.getInstance().InitializeSingleTon();

        //endregion

        // Game view state
        this.m_GameViewState = new GameViewState();

        // Surface holder
        this.m_SurfaceHolder = super.getHolder();

        // GetScreen size
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager =
            (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);

        this.setM_ScreenSize(new Size(metrics.widthPixels, metrics.heightPixels));

        try
        {
            this.resetWorld();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        this.mInGamePaused = false;

        this.setLayerType(LAYER_TYPE_HARDWARE, null);

    }


    private void resetWorld() throws Exception
    {
        this.m_GameObjects = new ArrayList<>();
        this.m_GameObjects.add(new World(this, WorldSetupFactory.GreenGrass));
    }

    //endregion

    //endregion


    @Override
    public boolean onTouchEvent(MotionEvent motionEvent)
    {

        /*
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                int iii = 2;
                break;
            case MotionEvent.ACTION_UP:
                int a = 2;
                break;

        }
*/
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                this.m_GameViewState.setM_IsDown(true);
                this.m_GameViewState.setM_InputDirection(InputDirection.DOWN);
                this.m_GameViewState.setAt(new Vector2(motionEvent.getX(), motionEvent.getY()));
                break;
            case MotionEvent.ACTION_UP:
                this.m_GameViewState.setM_IsDown(false);
                this.m_GameViewState.setM_InputDirection(InputDirection.UP);
                this.m_GameViewState.setAt(new Vector2(motionEvent.getX(), motionEvent.getY()));
                break;
            case MotionEvent.ACTION_MOVE :
                this.m_GameViewState.setAt(new Vector2(motionEvent.getX(), motionEvent.getY()));
                break;
            case MotionEvent.ACTION_CANCEL:

                break;
        }

        return true;

    }

    public OrientationProvider getmOrientationProvider() {
        return mOrientationProvider;
    }

    public void setmOrientationProvider(OrientationProvider mOrientationProvider) {
        this.mOrientationProvider = mOrientationProvider;
    }

    //region SurfaceHolder.Callback

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    //endregion

}
