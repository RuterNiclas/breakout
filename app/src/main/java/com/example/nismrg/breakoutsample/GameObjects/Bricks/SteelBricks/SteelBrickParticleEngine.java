package com.example.nismrg.breakoutsample.GameObjects.Bricks.SteelBricks;

import android.graphics.Color;

import com.example.nismrg.breakoutsample.GameObjects.MiscellaneousLogic.IParticleEngine;
import com.example.nismrg.breakoutsample.GameObjects.ParticleEffects.PaintParticleLine;
import com.example.nismrg.breakoutsample.Math.Vector3;

import java.util.ArrayList;
import java.util.Collection;

import Utility.Random.RandomProvider;

/**
 * Created by nismrg on 2016-08-02.
 */
public class SteelBrickParticleEngine implements IParticleEngine<PaintParticleLine>
{

    //region Instance

    //region Variables

    //region Private

    private ArrayList<PaintParticleLine> m_CacheBank;

    //endregion

    //endregion

    //region Constructors

    private SteelBrickParticleEngine()
    {
    }

    //endregion

    //region Methods

    //region Public

    // Start the singleton build a cache of objects.
    public void InitializeSingleTon()
    {

        this.m_CacheBank = new ArrayList<>();

        this.replenishInstances(this.STARTING_SIZE);
    }

    public ArrayList<PaintParticleLine> pullParticles(int numberOfObjects)
    {

        ArrayList<PaintParticleLine> result = new ArrayList<>();

        if(this.m_CacheBank.size() < numberOfObjects)
        {
            int missingValues = numberOfObjects - this.m_CacheBank.size();

            for(int i = 0; i < missingValues; i+= this.ON_EMPTY_REFILL_NUMBER)
            {
                this.replenishInstances(this.ON_EMPTY_REFILL_NUMBER);
            }
        }

        for(int i = 0; i < numberOfObjects; i++)
        {
            result.add(this.m_CacheBank.get(0));
            this.m_CacheBank.remove(0);
        }

        return result;

    }

    public PaintParticleLine pullSingleParticle()
    {

        PaintParticleLine result = null;

        if(this.m_CacheBank.size() < 1)
        {
            this.replenishInstances(this.ON_EMPTY_REFILL_NUMBER);
        }

        result = this.m_CacheBank.get(0);
        this.m_CacheBank.remove(0);

        return result;

    }

    // External push values

    public void pushBackParticles(Collection<PaintParticleLine> particles)
    {

        for(PaintParticleLine paintParticleLine : particles)
        {
            paintParticleLine.getmMotionSecond().scaleToLength(1f);
            paintParticleLine.mMotionSecond3d.scaleToLength(1f);
            //paintParticleLine.getmMotionSecond3d().scaleToLength(1f);
            paintParticleLine.setmMillisecondsToDeath(this.getRandomLifeTime());
            paintParticleLine.updateSpeed(this.getSpeedSlightRandomized());
        }

        this.m_CacheBank.addAll(particles);
    }

    //endregion

    //endregion

    //region Private

    private void replenishInstances(int numberOfElements)
    {
        PaintParticleLine paintParticleLine;
        Vector3 startingLocation = new Vector3(0f,0f, 0f);
        Vector3 motionDirection = new Vector3(0f,-1f, 0f);

        for(int i = 0; i < numberOfElements; i++)
        {

            paintParticleLine =
                new PaintParticleLine(startingLocation,
                                      motionDirection,
                                      this.getSpeedSlightRandomized(),
                                      Color.BLACK,
                                      this.getRandomizedLength(),
                                      this.getRandomizedWidth(),
                                      this.getRandomizeRotationSpeedDegreesSecond(),
                                      this.getRandomizeRotation(),
                                      this.getRandomLifeTime());

            this.m_CacheBank.add(paintParticleLine);

        }
    }

    private int getRandomLifeTime()
    {
        RandomProvider randomProvider = RandomProvider.getInstance();

        // No need for rounding. Not to accurate value over all.
        int lifetime = (int)((randomProvider.nextGaussian() * SteelBrickParticleEngine.LIFETIME_IN_MILLISECONDS_STANDARD_DEVIATION) + SteelBrickParticleEngine.AVERAGE_PARTICLE_LIFETIME_IN_MILLISECONDS);

        return lifetime;

    }

    private float getRandomizedLength()
    {

        RandomProvider randomProvider = RandomProvider.getInstance();

        float length =
            Math.abs((float)(randomProvider.nextGaussian()) * SteelBrickParticleEngine.LENGTH_NORMAL_DISTRIBUTION_IN_UNITS) +
            SteelBrickParticleEngine.NORMAL_LENGTH_IN_UNITS;

        return  length;

    }

    // No need to make this. User will not see startup differences in rotation any way
    private Vector3 getRandomizeRotation()
    {

        /*
        Random r = RandomProvider.getInstance().getM_Random();

        Vector3 rotation =
            new Vector3(0f, // In a 2d plane we don't start with depth rotations.
                        0f, // In a 2d plane we don't start with depth rotations.
                        (float)((r.nextGaussian() * SteelBrickParticleEngine.PARTICLE_ROTATION_STANDARD_DEVIATION_IN_DEGREES_SECOND.getmZ()) +
                        SteelBrickParticleEngine.PARTICLE_ROTATION_AVERAGES_IN_DEGREES_SECOND.getmZ()));

        */

        return new Vector3(0f, 0f, 0f);

    }

    private Vector3 getRandomizeRotationSpeedDegreesSecond()
    {

        RandomProvider randomProvider = RandomProvider.getInstance();

        float x = (float)((randomProvider.nextGaussian() * SteelBrickParticleEngine.PARTICLE_ROTATION_STANDARD_DEVIATION_IN_DEGREES_SECOND.getmX()) +
                  SteelBrickParticleEngine.PARTICLE_ROTATION_AVERAGES_IN_DEGREES_SECOND.getmX());
        float y = (float)((randomProvider.nextGaussian() * SteelBrickParticleEngine.PARTICLE_ROTATION_STANDARD_DEVIATION_IN_DEGREES_SECOND.getmY()) +
                  SteelBrickParticleEngine.PARTICLE_ROTATION_AVERAGES_IN_DEGREES_SECOND.getmY())  ;
        float z = (float)((randomProvider.nextGaussian() * SteelBrickParticleEngine.PARTICLE_ROTATION_STANDARD_DEVIATION_IN_DEGREES_SECOND.getmZ()) +
                  SteelBrickParticleEngine.PARTICLE_ROTATION_AVERAGES_IN_DEGREES_SECOND.getmZ());

        Vector3 rotation = new Vector3(x,
                                       y,
                                       z);

        return  rotation;

    }

    public static float getSpeedSlightRandomized()
    {
        float result = SteelBrickParticleEngine.INITIAL_SPEED_UNIT_SECOND;

        RandomProvider randomProvider = RandomProvider.getInstance();

        result += randomProvider.nextGaussian() * SteelBrickParticleEngine.SPEED_STANDARD_DEVIATION_UNIT_SECOND;

        return result;

    }

    public static int getRandomizedWidth()
    {
        RandomProvider randomProvider = RandomProvider.getInstance();

        //                                                                                                                                            Performance
        float width = (float)((Math.abs(randomProvider.nextGaussian()) * (SteelBrickParticleEngine.LINE_WIDTH_STANDARD_DEVIATION_ONLY_FOR_POSITIVE_USE * 0.5f)) +
                      SteelBrickParticleEngine.AVERAGE_LINE_WIDTH);

        int intWidth = Math.round(width);

        return intWidth;

    }

    //endregion

    //endregion

    //endregion

    //region Static

    //region Constants

    //region Private

    // Speed
    private static final float INITIAL_SPEED_UNIT_SECOND = 700f;
    private static final float SPEED_STANDARD_DEVIATION_UNIT_SECOND = 300f;

    // Length
    private static final float NORMAL_LENGTH_IN_UNITS = 4;
    private static final float LENGTH_NORMAL_DISTRIBUTION_IN_UNITS = 4;

    // Bulk values
    private static final int STARTING_SIZE = 15000;
    private static final int ON_EMPTY_REFILL_NUMBER = 2000;

    // Life time
    private static final int AVERAGE_PARTICLE_LIFETIME_IN_MILLISECONDS = 1000;
    private static final int LIFETIME_IN_MILLISECONDS_STANDARD_DEVIATION = 100;

    // Line width
    private static final int AVERAGE_LINE_WIDTH = 3;
    //     This can only represent deviation in the right hand way. E.a the distributed value must be pulled in absolut value
    //     |r| over r
    private static final float LINE_WIDTH_STANDARD_DEVIATION_ONLY_FOR_POSITIVE_USE = 2f;

    //endregion

    //endregion

    //region Variables

    //region Public

    private static Vector3 PARTICLE_ROTATION_AVERAGES_IN_DEGREES_SECOND =
        new Vector3(0f,
                    0f,
                    0f);

    private static Vector3 PARTICLE_ROTATION_STANDARD_DEVIATION_IN_DEGREES_SECOND =
        new Vector3(1080f,
                    // I think it looks more natural if this rotation actually is absent.
                    1080f,
                    1080f);

    //endregion

    //region Private

    private static SteelBrickParticleEngine ourInstance = new SteelBrickParticleEngine();

    //endregion

    //endregion

    //region Encapsulation

    //region Public

    public static SteelBrickParticleEngine getInstance()
    {
        return ourInstance;
    }

    //endregion

    //endregion

    //endregion

}
