package com.example.nismrg.breakoutsample.DeviceFunctions;

/**
 * Created by nismrg on 2016-12-20.
 */
public class VibrationEnums
{

    //region Static

    //region Variables

    //region Public

    public static final int LENGTH_INFO = 75;
    public static final int LENGTH_SMALL = 20;
    public static final int LENGTH_SMALLEST_POSSIBLE = 10;

    //endregion

    //endregion

    //endregion

}
